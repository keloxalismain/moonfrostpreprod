using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using UnityEditor.SceneManagement;

namespace Oxalis.TechArt
{
    public class GEN_IconGenerator : EditorWindow
    {
        public static ItemDefinition ItemForIcon;
        private const string GeneratorFolder = "Assets/Editor/IconGenerator/";
        private const string IconFolder = "Assets/UI/Icons/";

        private string _holderName;
        private SkinnedMeshRenderer _holderWearable;
        private MeshRenderer _holderItem;
        private SkinnedMeshRenderer _holderChibiMesh;

        private GameObject _holderCamera;
        private GameObject _holderBaseChibi;
        private Material _holderMaterial;

        private bool _isWearable = false;
        private ItemDefinition _itemDefinition;
        private WearableDefinition _wearableDefinition;

        private static GameObject _baseChibiPB;
        private static RenderTexture _renderTexture;

        [MenuItem("Moonfrost/Generic Icon Generator")]
        public static void Init()
        {
            EditorWindow windowWindow = GetWindow(typeof(GEN_IconGenerator));
            windowWindow.Show();
            ItemForIcon = null;
            _baseChibiPB = AssetDatabase.LoadMainAssetAtPath(GeneratorFolder + "GEN_ICN_Chibi_01_PB.prefab") as GameObject;
            _renderTexture = AssetDatabase.LoadMainAssetAtPath(GeneratorFolder + "GEN_IconGenerator_RT.asset") as RenderTexture;
        }

        private void OnGUI()
        {
            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Tip of the day:");
            EditorGUILayout.HelpBox("You can edit pose, angle, materials or even lighting before dropping the mic!", MessageType.Info);
            EditorGUILayout.Space();

            GUILayout.Label("1. Add A Item Definition Config");
            ItemForIcon = EditorGUILayout.ObjectField("", ItemForIcon, typeof(ItemDefinition), true) as ItemDefinition;
            EditorGUILayout.Space();

            if (ItemForIcon != null)
            {
                AssignItem(ItemForIcon);
            }

            GUILayout.Label("2. Preview Icon");
            if (GUILayout.Button("Iconic really!"))
            {
                SetCostumeScene();
            } // If icon button pressed

            GUILayout.Label("3. Save Render");
            if (GUILayout.Button("Mic Drop"))
            {
                CamCapture();
            } // Mic drop button pressed
            
            GUILayout.Label("4. Assign and Exit");
            if (GUILayout.Button("Leave Stage"))
            {
                SetItemSprite();
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

                this.Close();
            } // Leave stage button pressed

        } // OnGui

        public void AssignItem(ItemDefinition m_itemForIcon)
        {
            if (m_itemForIcon is WearableDefinition)
            {
                _wearableDefinition = m_itemForIcon as WearableDefinition;
                _isWearable = true;
            }
            else
            {
                _itemDefinition = m_itemForIcon;
                _isWearable = false;
            }
            _holderName = m_itemForIcon.name.Replace("SO", "EICN.png");
        }

        public void SetCostumeScene()
        {
            EditorSceneManager.OpenScene(GeneratorFolder + "GEN_IconGenerator_SCN.unity", OpenSceneMode.Single);
            _holderCamera = GameObject.Find("Main Camera");

            AssetSettingsPresetter.shouldProcess = true;
            Menu.SetChecked("Moonfrost/Asset Presetter", AssetSettingsPresetter.shouldProcess);

            if (_isWearable)
            {
                _holderBaseChibi = GameObject.Instantiate(_baseChibiPB);

                _holderChibiMesh = _holderBaseChibi.GetComponentInChildren<SkinnedMeshRenderer>();

                _holderBaseChibi.transform.localPosition = Vector3.zero;
                _holderBaseChibi.transform.localRotation = Quaternion.identity;
                _holderBaseChibi.transform.localScale = Vector3.one;

                _holderWearable = Instantiate<SkinnedMeshRenderer>(_wearableDefinition.chibiAsset);

                _holderWearable.transform.parent = _holderChibiMesh.transform;
                _holderWearable.transform.localPosition = Vector3.zero;
                _holderWearable.transform.localRotation = Quaternion.Euler(30f, 45f, 0); // Quaternion.identity;
                _holderWearable.transform.localScale = Vector3.one;

                _holderWearable.bones = _holderChibiMesh.bones;
                _holderWearable.rootBone = _holderChibiMesh.rootBone;

                _holderMaterial = AssetDatabase.LoadAssetAtPath<Material>(GeneratorFolder + "GEN_VertMask_C_Gen_MAT.mat");
                _holderWearable.sharedMaterial = Instantiate(_holderMaterial);
                _holderWearable.sharedMaterial.SetColor("_BaseColor", _wearableDefinition.defaultColorConfig.PrimaryColor);
                _holderWearable.sharedMaterial.SetColor("_SecondaryColor", _wearableDefinition.defaultColorConfig.SecondaryColor);
            }
            else
            {
                var m_holderItemPB = Instantiate(_itemDefinition.inWorldAsset);
                m_holderItemPB.transform.localPosition = Vector3.zero;
                m_holderItemPB.transform.localRotation = Quaternion.Euler(30f, 45f, 0);// Quaternion.identity;
                m_holderItemPB.transform.localScale = Vector3.one;
                _holderItem = m_holderItemPB.GetComponent<MeshRenderer>();
            }
            CamFocus();
        }

        private void CamFocus()
        {
            if (_isWearable)
            {
                _holderCamera.transform.SetPositionAndRotation(_holderWearable.bounds.center + new Vector3(0, 0, _holderWearable.bounds.size.z + 0.1f), Quaternion.Euler(0f, -180f, 0f));
                _holderChibiMesh.enabled = false;
            }
            else
            {
                _holderCamera.transform.SetPositionAndRotation(_holderItem.bounds.center + new Vector3(0, 0, _holderItem.bounds.size.z + 0.1f), Quaternion.Euler(0f, -180f, 0f));
            }
        }

        // "Capture" camera's Render Texture and save.
        public void CamCapture()
        {
            var m_camera = _holderCamera.GetComponent<Camera>();
            m_camera.targetTexture = _renderTexture;
            // The Render Texture in RenderTexture.active is the one that will be read by ReadPixels.
            RenderTexture currentRT = RenderTexture.active;
            RenderTexture.active = m_camera.targetTexture;

            // Render the camera's view.
            m_camera.Render();

            // Make a new texture and read the active Render Texture into it.
            Texture2D image = new Texture2D(m_camera.targetTexture.width, m_camera.targetTexture.height);
            image.ReadPixels(new Rect(0, 0, m_camera.targetTexture.width, m_camera.targetTexture.height), 0, 0);
            image.Apply();

            // Replace the original active Render Texture.
            RenderTexture.active = currentRT;

            var Bytes = image.EncodeToPNG();
            DestroyImmediate(image, true);
            string m_path = "";
            if (_isWearable)
            {
                DestroyImmediate(_holderWearable, true);
                DestroyImmediate(_holderBaseChibi, true);
                m_path = EditorUtility.OpenFolderPanel("Icon Folder Location", IconFolder, "Wearables");
            }
            else
            {
                DestroyImmediate(_holderItem, true);
                m_path = EditorUtility.OpenFolderPanel("Icon Folder Location", IconFolder, "Items");
            }

            File.WriteAllBytes(m_path + "/" + _holderName, Bytes);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        } //CamCapture

        public void SetItemSprite()
        {
            string m_assetType;

            if (_isWearable) m_assetType = "Wearables/";
            else m_assetType = "Items/";

            Sprite m_renderedIcon = AssetDatabase.LoadAssetAtPath<Sprite>(IconFolder + m_assetType + _holderName);

            ItemForIcon.itemIcon = m_renderedIcon;
            EditorUtility.SetDirty(ItemForIcon);
        }
    }
}
