﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Linq;

namespace Oxalis.TechArt
{
    /// <summary>
    /// Validates art assets by comparing against preset files listed in a projects scriptable object template
    /// can be compared against or applied through here
    /// </summary>

    public class AssetValidator
    {
        public enum ValidationMessageType
        {
            NamingConventionError,
            ModelError,
            TextureError,
            AudioError,
            ConformSuccess,
            ValidationSuccess
        }

        public struct ValidationMessageInfo
        {
            public readonly ValidationMessageType MessageType;
            public readonly string MessageString;
            public readonly string AssetPath;

            public ValidationMessageInfo(ValidationMessageType validationType, string messageString, string assetPath)
            {
                MessageType = validationType;
                MessageString = messageString;
                AssetPath = assetPath;
            }
        }

        public int AssetsToValidate
        {
            get
            {
                return _assetsToValidate.Count;
            }
        }

        public int AssetsValidated
        {
            get
            {
                return _assetsValidated;
            }
        }

        public List<ValidationMessageInfo> ValidationMessageList
        {
            get
            {
                return _validationMessageList;
            }
        }

        private readonly int _batchSize = 30;

        private readonly string _assetVaildatorSoPath = "Assets/Editor/AssetValidator/GEN_MF_AssetValidator_SO.asset";

        private AssetValidatorSO _assetValidatorSo;

        private bool _conformSelected = false;

        private List<ValidationMessageInfo> _validationMessageList = new List<ValidationMessageInfo>();

        private List<string> _assetsToValidate = new List<string>();

        private int _assetsValidated;

        public void FindAllAssetsToValidate()
        {
            PreValidationSetup();
            _conformSelected = false;

            if (_assetValidatorSo != null)
            {
                if (_assetValidatorSo._searchPackages && _assetValidatorSo._packagesFolder.Length > 1)
                {
                    if (_assetValidatorSo._packagesSubFolders.Count != 0)
                    {
                        foreach (var subFolder in _assetValidatorSo._packagesSubFolders)
                        {
                            foreach (var path in Directory.GetFiles(_assetValidatorSo._packagesFolder + subFolder, "*", SearchOption.AllDirectories))
                            {
                                _assetsToValidate.Add(path);
                            }
                        }
                    }
                    else
                    {
                        foreach (var path in Directory.GetFiles(_assetValidatorSo._packagesFolder, "*", SearchOption.AllDirectories))
                        {
                            _assetsToValidate.Add(path);
                        }
                    }
                }

                if (_assetValidatorSo._searchAssets)
                {
                    if (_assetValidatorSo._assetsSubFolders.Count != 0)
                    {
                        foreach (var subFolder in _assetValidatorSo._assetsSubFolders)
                        {
                            foreach (var path in Directory.GetFiles("Assets/" + subFolder, "*", SearchOption.AllDirectories))
                            {
                                _assetsToValidate.Add(path);
                            }
                        }
                    }
                    else
                    {
                        foreach (var path in Directory.GetFiles("Assets", "*", SearchOption.AllDirectories))
                        {
                            _assetsToValidate.Add(path);
                        }
                    }
                }
            }
        }

        public void FindSelectedAssetsToValidate()
        {
            PreValidationSetup();

            if (_assetValidatorSo != null)
            {
                if (Selection.assetGUIDs.Length > 0 && _assetValidatorSo != null)
                {
                    foreach (var selectedObject in Selection.objects)
                    {
                        var selectedAssetPath = AssetDatabase.GetAssetPath(selectedObject);
                        if (Directory.Exists(selectedAssetPath))
                        {
                            foreach (var path in Directory.GetFiles(selectedAssetPath, "*", SearchOption.AllDirectories))
                            {
                                _assetsToValidate.Add(path);
                            }
                        }
                        else if (File.Exists(selectedAssetPath))
                        {
                            _assetsToValidate.Add(selectedAssetPath);
                        }
                    }
                }
            }
        }

        public void ValidateAssets()
        {
            for (int i = _assetsValidated; i < _assetsToValidate.Count && i < _assetsValidated + _batchSize; i++)
            {
                ValidateAsset(_assetsToValidate[i]);
            }

            if (_assetsValidated + _batchSize >= _assetsToValidate.Count)
            {
                _assetsValidated = _assetsToValidate.Count;

                if (_validationMessageList.Count == 0)
                {
                    LogValidationSuccess();
                }
                _conformSelected = false;
            }
            else
            {
                _assetsValidated += _batchSize;
            }
        }

        public void ConformSelected()
        {
            _conformSelected = true;
            FindSelectedAssetsToValidate();
        }

        public int GetErrorCount(ValidationMessageType messageType)
        {
            int m_errorCount = 0;
            foreach (var m_error in _validationMessageList)
            {
                if (m_error.MessageType == messageType)
                {
                    m_errorCount++;
                }
            }

            return m_errorCount;
        }

        public void ValidateAsset(string m_path)
        {
            if (m_path.ToLower().EndsWith("fbx") || m_path.ToLower().EndsWith("obj"))
            {
                ValidateModel(m_path, false);
            }
            else if (m_path.ToLower().EndsWith("png") || m_path.ToLower().EndsWith("tga"))
            {
                ValidateTexture(m_path);
            }
            else if (m_path.ToLower().EndsWith("ogg") || m_path.ToLower().EndsWith("mp3") || m_path.ToLower().EndsWith("wav"))
            {
                ValidateAudio(m_path, false);
            }
        }

        public int ValidateModel(string path, bool m_fromImporter)
        {
            if(_assetValidatorSo == null)
                _assetValidatorSo = AssetDatabase.LoadMainAssetAtPath(_assetVaildatorSoPath) as AssetValidatorSO;

            var assetName = Path.GetFileNameWithoutExtension(path);
            if (_assetValidatorSo.CharacterModelPresets.prefix.Length > 1 && assetName.StartsWith(_assetValidatorSo.CharacterModelPresets.prefix))
            {
                if (!m_fromImporter)
                {
                    ValidateAssetFromAffixPresetPair(_assetValidatorSo.CharacterModelPresets, path, assetName);
                }
                return (int)AssetValidatorSO.AssetDiscipline.Character;
            }
            else if (_assetValidatorSo.AvatarModelPresets.prefix.Length > 1 && assetName.StartsWith(_assetValidatorSo.AvatarModelPresets.prefix))
            {
                if (!m_fromImporter)
                {
                    ValidateAssetFromAffixPresetPair(_assetValidatorSo.AvatarModelPresets, path, assetName);
                }
                return (int)AssetValidatorSO.AssetDiscipline.Avatar;
            }
            else if (_assetValidatorSo.PetModelPresets.prefix.Length > 1 && assetName.StartsWith(_assetValidatorSo.PetModelPresets.prefix))
            {
                if (!m_fromImporter)
                {
                    ValidateAssetFromAffixPresetPair(_assetValidatorSo.PetModelPresets, path, assetName);
                }
                return (int)AssetValidatorSO.AssetDiscipline.Pet;
            }
            else if (!string.IsNullOrEmpty(_assetValidatorSo.EnvironmentModelPresets.prefix) && assetName.StartsWith(_assetValidatorSo.EnvironmentModelPresets.prefix))
            {
                if (!m_fromImporter)
                {
                    ValidateAssetFromAffixPresetPair(_assetValidatorSo.EnvironmentModelPresets, path, assetName);
                }
                return (int)AssetValidatorSO.AssetDiscipline.Environment;

            }
            else if (!string.IsNullOrEmpty(_assetValidatorSo.buildingModelPresets.prefix) && assetName.StartsWith(_assetValidatorSo.buildingModelPresets.prefix))
            {
                if (!m_fromImporter)
                {
                    ValidateAssetFromAffixPresetPair(_assetValidatorSo.buildingModelPresets, path, assetName);
                }
                return (int)AssetValidatorSO.AssetDiscipline.Building;
            }
            else if (!string.IsNullOrEmpty(_assetValidatorSo.ItemsModelPresets.prefix) && assetName.StartsWith(_assetValidatorSo.ItemsModelPresets.prefix))
            {
                if (!m_fromImporter)
                {
                    ValidateAssetFromAffixPresetPair(_assetValidatorSo.ItemsModelPresets, path, assetName);
                }
                return (int)AssetValidatorSO.AssetDiscipline.Items;

            }
            else if (!string.IsNullOrEmpty(_assetValidatorSo.VfxModelPresets.prefix) && assetName.StartsWith(_assetValidatorSo.VfxModelPresets.prefix))
            {
                if (!m_fromImporter)
                {
                    ValidateAssetFromAffixPresetPair(_assetValidatorSo.VfxModelPresets, path, assetName);
                }
                return (int)AssetValidatorSO.AssetDiscipline.VFX;

            }
            else
            {
                if (!m_fromImporter)
                {
                    LogNamingError(assetName, path);
                }
                return (int)AssetValidatorSO.AssetDiscipline.Error;

            }
        }

        private void ValidateTexture(string path)
        {
            if (_assetValidatorSo == null)
                _assetValidatorSo = AssetDatabase.LoadMainAssetAtPath(_assetVaildatorSoPath) as AssetValidatorSO;

            var assetName = Path.GetFileNameWithoutExtension(path);
            if (_assetValidatorSo.CharacterTexturePresets.prefix.Length > 1 && assetName.StartsWith(_assetValidatorSo.CharacterTexturePresets.prefix))
            {
                ValidateAssetFromAffixPresetPair(_assetValidatorSo.CharacterTexturePresets, path, assetName);
            }
            else if (_assetValidatorSo.AvaTexturePresets.prefix.Length > 1 && assetName.StartsWith(_assetValidatorSo.AvaTexturePresets.prefix))
            {
                ValidateAssetFromAffixPresetPair(_assetValidatorSo.AvaTexturePresets, path, assetName);
            }
            else if (_assetValidatorSo.PetTexturePresets.prefix.Length > 1 && assetName.StartsWith(_assetValidatorSo.PetTexturePresets.prefix))
            {
                ValidateAssetFromAffixPresetPair(_assetValidatorSo.PetTexturePresets, path, assetName);
            }
            else if (_assetValidatorSo.EnvironmentTexturePresets.prefix.Length > 1 && assetName.StartsWith(_assetValidatorSo.EnvironmentTexturePresets.prefix))
            {
                ValidateAssetFromAffixPresetPair(_assetValidatorSo.EnvironmentTexturePresets, path, assetName);
            }
            else if (_assetValidatorSo.BuildingTexturePresets.prefix.Length > 1 && assetName.StartsWith(_assetValidatorSo.BuildingTexturePresets.prefix))
            {
                ValidateAssetFromAffixPresetPair(_assetValidatorSo.BuildingTexturePresets, path, assetName);
            }
            else if (_assetValidatorSo.ItemTexturePresets.prefix.Length > 1 && assetName.StartsWith(_assetValidatorSo.ItemTexturePresets.prefix))
            {
                ValidateAssetFromAffixPresetPair(_assetValidatorSo.ItemTexturePresets, path, assetName);
            }
            else if (_assetValidatorSo.VfxTexturePresets.prefix.Length > 1 && assetName.StartsWith(_assetValidatorSo.VfxTexturePresets.prefix))
            {
                ValidateAssetFromAffixPresetPair(_assetValidatorSo.VfxTexturePresets, path, assetName);
            }
            else if (_assetValidatorSo.UiTexturePresets.prefix.Length > 1 && assetName.StartsWith(_assetValidatorSo.UiTexturePresets.prefix))
            {
                ValidateAssetFromAffixPresetPair(_assetValidatorSo.UiTexturePresets, path, assetName);
            }
            else
            {
                LogNamingError(assetName, path);
            }
        }

        public int ValidateAudio(string path, bool m_fromImporter)
        {
            if (_assetValidatorSo == null)
                _assetValidatorSo = AssetDatabase.LoadMainAssetAtPath(_assetVaildatorSoPath) as AssetValidatorSO;

            var assetName = Path.GetFileNameWithoutExtension(path);
            if (_assetValidatorSo.AudioClipPresets.prefix.Length > 1 && assetName.StartsWith(_assetValidatorSo.AudioClipPresets.prefix))
            {
                if (_assetValidatorSo.AudioClipPresets.affixPresetPairs.Count > 1 && assetName.EndsWith(_assetValidatorSo.AudioClipPresets.affixPresetPairs[0].affix))
                {
                    if (!m_fromImporter)
                    {
                        ValidateAssetFromAffixPresetPair(_assetValidatorSo.AudioClipPresets, path, assetName);
                    }
                    return 0; // Loop
                }
                else if (_assetValidatorSo.AudioClipPresets.affixPresetPairs.Count > 1 && assetName.EndsWith(_assetValidatorSo.AudioClipPresets.affixPresetPairs[1].affix))
                {
                    if (!m_fromImporter)
                    {
                        ValidateAssetFromAffixPresetPair(_assetValidatorSo.AudioClipPresets, path, assetName);
                    }
                    return 1; // Musis
                }
                else if (_assetValidatorSo.AudioClipPresets.affixPresetPairs.Count > 1 && assetName.EndsWith(_assetValidatorSo.AudioClipPresets.affixPresetPairs[2].affix))
                {
                    if (!m_fromImporter)
                    {
                        ValidateAssetFromAffixPresetPair(_assetValidatorSo.AudioClipPresets, path, assetName);
                    }
                    return 2; // Loop
                }
                else return 9; //error
            }
            else
            {
                if (!m_fromImporter)
                {
                    LogNamingError(assetName, path);
                }
                return 9; //error
            }
        }

        private void ValidateAssetFromAffixPresetPair(AssetValidatorSO.AssetValidationSettings assetSettings, string assetPath, string assetName)
        {
            bool validAssetName = false;

            var orderedPresetPairs = assetSettings.affixPresetPairs.OrderByDescending(x => x.affix.Length);

            foreach (var suffixPresetPair in orderedPresetPairs)
            {
                if (!string.IsNullOrEmpty(suffixPresetPair.affix))
                {
                    switch (suffixPresetPair.affixType)
                    {
                        case AssetValidatorSO.AffixType.Prefix:
                            validAssetName = assetName.StartsWith(suffixPresetPair.affix);
                            break;
                        case AssetValidatorSO.AffixType.Suffix:
                            validAssetName = assetName.EndsWith(suffixPresetPair.affix);
                            break;
                    }

                    if (validAssetName)
                    {
                        AssetImporter assetImporter = AssetImporter.GetAtPath(assetPath);
                        if (suffixPresetPair.preset != null && !suffixPresetPair.preset.DataEquals(assetImporter))
                        {

                            if (_conformSelected)
                            {
                                string conformMessageString = assetName + " has been conformed to preset: " + suffixPresetPair.preset.name;
                                suffixPresetPair.preset.ApplyTo(assetImporter);
                                assetImporter.SaveAndReimport();
                                LogMessage(ValidationMessageType.ConformSuccess, conformMessageString, assetPath);
                            }
                            else
                            {
                                string errorString = assetName + " does not conform to preset: " + suffixPresetPair.preset.name;
                                switch (assetSettings.assetType)
                                {
                                    case (AssetValidatorSO.AssetType.Model):
                                        LogMessage(ValidationMessageType.ModelError, errorString, assetPath);
                                        break;
                                    case (AssetValidatorSO.AssetType.Texture):
                                        LogMessage(ValidationMessageType.TextureError, errorString, assetPath);
                                        break;
                                    case (AssetValidatorSO.AssetType.Audio):
                                        LogMessage(ValidationMessageType.AudioError, errorString, assetPath);
                                        break;
                                }
                            }
                        }
                        break;
                    }
                }
            }

            if (!validAssetName)
            {
                if (!string.IsNullOrEmpty(assetSettings.wildcard))
                {
                    if (assetPath.Contains(assetSettings.wildcard))
                    {
                        OnWildCardPresent(assetSettings, assetPath, assetName);
                    }
                    else
                        LogNamingError(assetName, assetPath);
                }
                else
                    LogNamingError(assetName, assetPath);
            }
        }

        private void OnWildCardPresent(AssetValidatorSO.AssetValidationSettings assetSettings, string assetPath, string assetName)
        {
            switch (assetSettings.assetType)
            {
                case AssetValidatorSO.AssetType.Model:
                    if (assetSettings.assetDiscipline == AssetValidatorSO.AssetDiscipline.Character)
                    {
                        OnCharacterModelWildcardPresent(assetPath, assetName);
                    }
                    else if (assetSettings.assetDiscipline == AssetValidatorSO.AssetDiscipline.Avatar)
                    {
                        OnAvatarModelWildcardPresent(assetPath, assetName);
                    }
                    else if (assetSettings.assetDiscipline == AssetValidatorSO.AssetDiscipline.Environment)
                    {
                        OnEnvModelWildcardPresent(assetPath, assetName);
                    }
                    else if (assetSettings.assetDiscipline == AssetValidatorSO.AssetDiscipline.Building)
                    {
                        OnBldModelWildcardPresent(assetPath, assetName);
                    }
                    else if (assetSettings.assetDiscipline == AssetValidatorSO.AssetDiscipline.Items)
                    {
                        OnItmModelWildcardPresent(assetPath, assetName);
                    }
                    else if (assetSettings.assetDiscipline == AssetValidatorSO.AssetDiscipline.VFX)
                    {
                        OnVfxModelWildcardPresent(assetPath, assetName);
                    }
                    else if (assetSettings.assetDiscipline == AssetValidatorSO.AssetDiscipline.Ui)
                    {
                        OnUiTexturelWildcardPresent(assetPath, assetName);
                    }
                    else if (assetSettings.assetDiscipline == AssetValidatorSO.AssetDiscipline.Sound)
                    {
                        OnAudioCliplWildcardPresent(assetPath, assetName);
                    }
                    break;
            }
        }

        private void OnCharacterModelWildcardPresent(string assetPath, string assetName)
        {
            var importer = AssetImporter.GetAtPath(assetPath) as ModelImporter;
            if (importer.animationType != _assetValidatorSo.animationType)
            {
                string errorString = assetName + " doesn't conform to wildcard animation type expectations";
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);

                if (_conformSelected)
                {
                    importer.animationType = _assetValidatorSo.animationType;
                }
            }

            if (importer.avatarSetup != _assetValidatorSo.avatarSetup)
            {
                string errorString = assetName + " doesn't conform to wildcard avatar setup expectations";
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);

                if (_conformSelected)
                {
                    importer.avatarSetup = _assetValidatorSo.avatarSetup;
                }
            }

            if (importer.skinWeights != _assetValidatorSo.skinWeights)
            {
                string errorString = assetName + " doesn't conform to wildcard skin weights settings expectations";
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);

                if (_conformSelected)
                {
                    importer.skinWeights = _assetValidatorSo.skinWeights;
                }
            }

            if (importer.materialImportMode != _assetValidatorSo.materialImportMode)
            {
                string errorString = assetName + " doesn't conform to wildcard material import options expectations";
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);

                if (_conformSelected)
                {
                    importer.materialImportMode = _assetValidatorSo.materialImportMode;
                }
            }

            if (importer.animationCompression != _assetValidatorSo.animationCompression)
            {
                string errorString = assetName + " doesn't conform to wildcard animation compression options expectations";
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);

                if (_conformSelected)
                {
                    importer.animationCompression = _assetValidatorSo.animationCompression;
                }
            }

            if (importer.importAnimation != _assetValidatorSo.importAnimation)
            {
                string errorString = assetName + " doesn't conform to wildcard animation import expectations";
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);

                if (_conformSelected)
                {
                    importer.importAnimation = _assetValidatorSo.importAnimation;
                }
            }

            if (importer.resampleCurves != _assetValidatorSo.resampleCurves)
            {
                string errorString = assetName + " doesn't conform to wildcard curves sampling expectations";
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);

                if (_conformSelected)
                {
                    importer.resampleCurves = _assetValidatorSo.resampleCurves;
                }
            }

            if (_conformSelected)
            {
                importer.SaveAndReimport();
            }
        }

        private void OnAvatarModelWildcardPresent(string assetPath, string assetName)
        {
            var importer = AssetImporter.GetAtPath(assetPath) as ModelImporter;
            if (importer.animationType != _assetValidatorSo.animationType)
            {
                string errorString = assetName + " doesn't conform to wildcard animation type expectations";
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);

                if (_conformSelected)
                {
                    importer.animationType = _assetValidatorSo.animationType;
                }
            }

            if (importer.avatarSetup != _assetValidatorSo.avatarSetup)
            {
                string errorString = assetName + " doesn't conform to wildcard avatar setup expectations";
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);

                if (_conformSelected)
                {
                    importer.avatarSetup = _assetValidatorSo.avatarSetup;
                }
            }

            if (importer.skinWeights != _assetValidatorSo.skinWeights)
            {
                string errorString = assetName + " doesn't conform to wildcard skin weights settings expectations";
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);

                if (_conformSelected)
                {
                    importer.skinWeights = _assetValidatorSo.skinWeights;
                }
            }

            if (importer.materialImportMode != _assetValidatorSo.materialImportMode)
            {
                string errorString = assetName + " doesn't conform to wildcard material import options expectations";
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);

                if (_conformSelected)
                {
                    importer.materialImportMode = _assetValidatorSo.materialImportMode;
                }
            }

            if (importer.animationCompression != _assetValidatorSo.animationCompression)
            {
                string errorString = assetName + " doesn't conform to wildcard animation compression options expectations";
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);

                if (_conformSelected)
                {
                    importer.animationCompression = _assetValidatorSo.animationCompression;
                }
            }

            if (importer.importAnimation != _assetValidatorSo.importAnimation)
            {
                string errorString = assetName + " doesn't conform to wildcard animation import expectations";
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);

                if (_conformSelected)
                {
                    importer.importAnimation = _assetValidatorSo.importAnimation;
                }
            }

            if (importer.resampleCurves != _assetValidatorSo.resampleCurves)
            {
                string errorString = assetName + " doesn't conform to wildcard curves sampling expectations";
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);

                if (_conformSelected)
                {
                    importer.resampleCurves = _assetValidatorSo.resampleCurves;
                }
            }

            if (_conformSelected)
            {
                importer.SaveAndReimport();
            }
        }


        private void OnEnvModelWildcardPresent(string assetPath, string assetName)
        {
            if (_assetValidatorSo.envModelPresetWildcard != null && !_assetValidatorSo.envModelPresetWildcard.DataEquals(AssetImporter.GetAtPath(assetPath)))
            {
                string errorString = assetName + "dosen't conform to " + _assetValidatorSo.envModelPresetWildcard.name;
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);


                if (_conformSelected)
                {
                    _assetValidatorSo.envModelPresetWildcard.ApplyTo(AssetImporter.GetAtPath(assetPath));
                }
            }
        }

        private void OnBldModelWildcardPresent(string assetPath, string assetName)
        {
            if (_assetValidatorSo.bldModelPresetWildcard != null && !_assetValidatorSo.bldModelPresetWildcard.DataEquals(AssetImporter.GetAtPath(assetPath)))
            {
                string errorString = assetName + "dosen't conform to " + _assetValidatorSo.bldModelPresetWildcard.name;
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);


                if (_conformSelected)
                {
                    _assetValidatorSo.bldModelPresetWildcard.ApplyTo(AssetImporter.GetAtPath(assetPath));
                }
            }
        }

        private void OnItmModelWildcardPresent(string assetPath, string assetName)
        {
            if (_assetValidatorSo.itmModelPresetWildcard != null && !_assetValidatorSo.itmModelPresetWildcard.DataEquals(AssetImporter.GetAtPath(assetPath)))
            {
                string errorString = assetName + "dosen't conform to " + _assetValidatorSo.itmModelPresetWildcard.name;
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);


                if (_conformSelected)
                {
                    _assetValidatorSo.itmModelPresetWildcard.ApplyTo(AssetImporter.GetAtPath(assetPath));
                }
            }
        }

        private void OnVfxModelWildcardPresent(string assetPath, string assetName)
        {
            if (_assetValidatorSo.vfxModelPresetWildcard != null && !_assetValidatorSo.vfxModelPresetWildcard.DataEquals(AssetImporter.GetAtPath(assetPath)))
            {
                string errorString = assetName + "dosen't conform to " + _assetValidatorSo.vfxModelPresetWildcard.name;
                LogMessage(ValidationMessageType.ModelError, errorString, assetPath);


                if (_conformSelected)
                {
                    _assetValidatorSo.vfxModelPresetWildcard.ApplyTo(AssetImporter.GetAtPath(assetPath));
                }
            }
        }

        private void OnUiTexturelWildcardPresent(string assetPath, string assetName)
        {
            if (_assetValidatorSo.uiTexturePresetWildcard != null && !_assetValidatorSo.uiTexturePresetWildcard.DataEquals(AssetImporter.GetAtPath(assetPath)))
            {
                string errorString = assetName + "dosen't conform to " + _assetValidatorSo.uiTexturePresetWildcard.name;
                LogMessage(ValidationMessageType.TextureError, errorString, assetPath);


                if (_conformSelected)
                {
                    _assetValidatorSo.uiTexturePresetWildcard.ApplyTo(AssetImporter.GetAtPath(assetPath));
                }
            }
        }

        private void OnAudioCliplWildcardPresent(string assetPath, string assetName)
        {
            if (_assetValidatorSo.audioClipPresetWildcard != null && !_assetValidatorSo.audioClipPresetWildcard.DataEquals(AssetImporter.GetAtPath(assetPath)))
            {
                string errorString = assetName + "dosen't conform to " + _assetValidatorSo.audioClipPresetWildcard.name;
                LogMessage(ValidationMessageType.AudioError, errorString, assetPath);


                if (_conformSelected)
                {
                    _assetValidatorSo.audioClipPresetWildcard.ApplyTo(AssetImporter.GetAtPath(assetPath));
                }
            }
        }

        private void LogNamingError(string assetPath, string assetName)
        {
            string errorString = assetName + " doesn't conform to naming convention";
            ValidationMessageInfo namingValidationMessage = new ValidationMessageInfo(ValidationMessageType.NamingConventionError, errorString, assetPath);
            _validationMessageList.Add(namingValidationMessage);
        }

        private void LogValidationSuccess()
        {
            string validationString = "No invalid assets detected!";
            ValidationMessageInfo validationSuccessMessage = new ValidationMessageInfo(ValidationMessageType.ValidationSuccess, validationString, "");
            _validationMessageList.Add(validationSuccessMessage);
        }

        private void LogMessage(ValidationMessageType messageType, string message, string assetPath)
        {
            ValidationMessageInfo validationMessage = new ValidationMessageInfo(messageType, message, assetPath);
            _validationMessageList.Add(validationMessage);
        }

        private void PreValidationSetup()
        {
            _assetValidatorSo = AssetDatabase.LoadMainAssetAtPath(_assetVaildatorSoPath) as AssetValidatorSO;
            _validationMessageList.Clear();
            _assetsToValidate.Clear();
            _assetsValidated = 0;
        }
    }
}
