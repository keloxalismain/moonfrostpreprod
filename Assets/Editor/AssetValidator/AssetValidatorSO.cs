﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Presets;

namespace Oxalis.TechArt
{
    /// <summary>
    /// Template of the projects art assets settings are saved as presets
    /// and can be tested against or applied through here
    /// </summary>
    [CreateAssetMenu(fileName = "GEN_MF_AssetValidator_SO.asset", menuName = "Preproduction/AssetValidation")]

    public class AssetValidatorSO : ScriptableObject
    {
        public enum AssetType
        {
            Model,
            Texture,
            Audio
        }

        public enum AssetDiscipline
        {
            Character,
            Avatar,
            Pet,
            Environment,
            Ui,
            Building,
            Items,
            VFX,
            Sound,
            Error
        }

        public enum AffixType
        {
            Prefix,
            Suffix
        }

        [Serializable]
        public struct AffixPresetPair
        {
            public string affix;
            public AffixType affixType;
            public Preset preset;
        }

        [Serializable]
        public class AssetValidationSettings
        {
            public AssetType assetType;
            public AssetDiscipline assetDiscipline;

            public string prefix;
            public List<AffixPresetPair> affixPresetPairs = new List<AffixPresetPair>();

            public string wildcard;
        }

        [Space, Header("Search Options"), Space]

        public bool _searchPackages = false;
        public string _packagesFolder = "Packages/";
        public List<string> _packagesSubFolders;
        [Space]

        public bool _searchAssets = false;
        public List<string> _assetsSubFolders;
        [Space]


        [Space, Header("Character FBXs"), Space]
        public AssetValidationSettings CharacterModelPresets;
        [Space]

        public ModelImporterAnimationType animationType = ModelImporterAnimationType.Generic;
        public ModelImporterAvatarSetup avatarSetup = ModelImporterAvatarSetup.CopyFromOther;
        public Avatar SourceAvatar;
        public ModelImporterSkinWeights skinWeights = ModelImporterSkinWeights.Standard;
        public ModelImporterMaterialImportMode materialImportMode = ModelImporterMaterialImportMode.None;
        public ModelImporterAnimationCompression animationCompression = ModelImporterAnimationCompression.KeyframeReduction;
        public bool importAnimation = true;
        public bool resampleCurves = false;

        [Header("Character Textures")]
        public AssetValidationSettings CharacterTexturePresets;


        [Space, Header("Avatar FBXs"), Space]
        public AssetValidationSettings AvatarModelPresets;
        [Space]

        public ModelImporterAnimationType AvaAnimationType = ModelImporterAnimationType.Generic;
        public ModelImporterAvatarSetup AvaAvatarSetup = ModelImporterAvatarSetup.CopyFromOther;
        public Avatar AvaSourceAvatar;
        public ModelImporterSkinWeights AvaSkinWeights = ModelImporterSkinWeights.Standard;
        public ModelImporterMaterialImportMode AvaMaterialImportMode = ModelImporterMaterialImportMode.None;
        public ModelImporterAnimationCompression AvaAnimationCompression = ModelImporterAnimationCompression.KeyframeReduction;
        public bool AvaImportAnimation = true;
        public bool AvaResampleCurves = true;

        [Header("Avatar Textures")]
        public AssetValidationSettings AvaTexturePresets;


        [Space, Header("Pet FBXs"), Space]
        public AssetValidationSettings PetModelPresets;
        [Space]

        public ModelImporterAnimationType petAnimationType = ModelImporterAnimationType.Generic;
        public ModelImporterAvatarSetup petAvatarSetup = ModelImporterAvatarSetup.CopyFromOther;
        public Avatar petSourceAvatar;
        public ModelImporterSkinWeights petSkinWeights = ModelImporterSkinWeights.Standard;
        public ModelImporterMaterialImportMode petMaterialImportMode = ModelImporterMaterialImportMode.None;
        public ModelImporterAnimationCompression petAnimationCompression = ModelImporterAnimationCompression.KeyframeReduction;
        public bool petImportAnimation = true;
        public bool petResampleCurves = false;

        [Header("Pet Textures")]
        public AssetValidationSettings PetTexturePresets;


        [Space, Header("Environment FBXs"), Space]
        public AssetValidationSettings EnvironmentModelPresets;
        public Preset envModelPresetWildcard;
        [Header("Environment Textures")]
        public AssetValidationSettings EnvironmentTexturePresets;


        [Space, Header("Building FBXs"), Space]
        public AssetValidationSettings buildingModelPresets;
        public Preset bldModelPresetWildcard;
        [Header("Building Textures")]
        public AssetValidationSettings BuildingTexturePresets;


        [Space, Header("Item FBXs"), Space]
        public AssetValidationSettings ItemsModelPresets;
        public Preset itmModelPresetWildcard;
        [Header("Item Textures")]
        public AssetValidationSettings ItemTexturePresets;


        [Space, Header("VFX FBXs"), Space]
        public AssetValidationSettings VfxModelPresets;
        public Preset vfxModelPresetWildcard;
        [Header("VFX Textures")]
        public AssetValidationSettings VfxTexturePresets;


        [Space, Header("UI Textures")]
        public AssetValidationSettings UiTexturePresets;
        public Preset uiTexturePresetWildcard;


        [Space, Header("Audio clips")]
        public AssetValidationSettings AudioClipPresets;
        public Preset audioClipPresetWildcard;
    }
}