﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

namespace Oxalis.TechArt
{
    /// <summary>
    /// Defaults false and resets on purpose because Postprocessors slow down an initial set up of a project
    /// and also a reimport all for everyone, but the benifit is only there for those few people importing assets 
    /// Switching to making use of the settings of the validator rather than coded values here
    /// </summary>
    [InitializeOnLoad]
    public class AssetSettingsPresetter : AssetPostprocessor
    {
        public Avatar Avatar;
        public const string AudioAssetPath = "Assets/Audio/";
        public const string AvatarAssetPath = "Assets/Avatar/";
        public const string BuildingsAssetPath = "Assets/Buildings/";
        public const string CharacterAssetPath = "Assets/Character/";
        public const string EnvironmentAssetPath = "Assets/Environment/";
        public const string ItemsAssetPath = "Assets/Items/";
        public const string PetsAssetPath = "Assets/Pets/";
        public const string UiAssetPath = "Assets/UI/";
        public const string VfxAssetPath = "Assets/Vfx/";
        public const string IconPath = "Assets/UI/Icons/";
        //public const string AnimationPath = "Assets/Art/Animation/"; harder to sort, maybe not worth doing
        public const string _assetVaildatorSoPath = "Assets/Editor/AssetValidator/GEN_MF_AssetValidator_SO.asset";
        private AssetValidatorSO _assetValidatorSo;

        public static bool shouldProcess = false;

        [MenuItem("Moonfrost/Asset Presetter Toggle")]
        private static void ToggleShouldRunPresetter()
        {
            shouldProcess = !shouldProcess;
            Menu.SetChecked("Moonfrost/Asset Presetter Toggle", shouldProcess);
        }

        private void ValidatorInit()
        {
            _assetValidatorSo = AssetDatabase.LoadMainAssetAtPath(_assetVaildatorSoPath) as AssetValidatorSO;
        }


        private void OnPreprocessTexture()
        {
            if (_assetValidatorSo == null)
            {
                ValidatorInit();
            }
            // Only run on initial import, Allows artist overwrites to be persistant when iterating an asset in external software, or to delete and discard
            // files, therefore reimporting, without overwriting thier original settings
            if (assetImporter.importSettingsMissing && shouldProcess)
            {
                ProcessTexture(assetPath);
            }
        }

        private void ProcessTexture(string m_assetPath)
        {
            var m_fileName = Path.GetFileNameWithoutExtension(m_assetPath);
            // Get a reference to the assetImporter which is contained in the class we've inherited from AssetPostProcessor		
            TextureImporter m_asset = assetImporter as TextureImporter;
            if (m_asset != null)
            {
                // Character Textures
                if (m_assetPath.StartsWith(CharacterAssetPath))
                {
                    if (m_fileName.EndsWith("_AM"))
                    {
                        _assetValidatorSo.CharacterTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_ICN"))
                    {
                        _assetValidatorSo.CharacterTexturePresets.affixPresetPairs[1].preset.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_MSK"))
                    {
                        _assetValidatorSo.CharacterTexturePresets.affixPresetPairs[2].preset.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_EICN"))
                    {
                        _assetValidatorSo.CharacterTexturePresets.affixPresetPairs[3].preset.ApplyTo(m_asset);
                    }
                    else
                    {
                        _assetValidatorSo.CharacterTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                    }
                }

                // Avatar Textures
                if (m_assetPath.StartsWith(AvatarAssetPath))
                {
                    if (m_fileName.EndsWith("_AM"))
                    {
                        _assetValidatorSo.AvaTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_MSK"))
                    {
                        _assetValidatorSo.AvaTexturePresets.affixPresetPairs[1].preset.ApplyTo(m_asset);
                    }
                    else
                    {
                        _assetValidatorSo.AvaTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                    }
                }

                // Pet Textures
                if (m_assetPath.StartsWith(PetsAssetPath))
                {
                    if (m_fileName.EndsWith("_AM"))
                    {
                        _assetValidatorSo.PetTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_MSK"))
                    {
                        _assetValidatorSo.PetTexturePresets.affixPresetPairs[1].preset.ApplyTo(m_asset);
                    }
                    else
                    {
                        _assetValidatorSo.PetTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                    }
                }

                // Environment Textures
                if (m_assetPath.StartsWith(EnvironmentAssetPath))
                {
                    if (m_fileName.EndsWith("_AM"))
                    {
                        _assetValidatorSo.EnvironmentTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_NM"))
                    {
                        _assetValidatorSo.EnvironmentTexturePresets.affixPresetPairs[1].preset.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_ICN"))
                    {
                        _assetValidatorSo.EnvironmentTexturePresets.affixPresetPairs[2].preset.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_MSK"))
                    {
                        _assetValidatorSo.EnvironmentTexturePresets.affixPresetPairs[3].preset.ApplyTo(m_asset);
                    }
                    else
                    {
                        _assetValidatorSo.EnvironmentTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                    }
                }

                // Building Textures
                if (m_assetPath.StartsWith(BuildingsAssetPath))
                {
                    if (m_fileName.EndsWith("_AM"))
                    {
                        _assetValidatorSo.BuildingTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_NM"))
                    {
                        _assetValidatorSo.BuildingTexturePresets.affixPresetPairs[1].preset.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_ICN"))
                    {
                        _assetValidatorSo.BuildingTexturePresets.affixPresetPairs[2].preset.ApplyTo(m_asset);
                    }
                    else
                    {
                        _assetValidatorSo.BuildingTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                    }
                }

                // Items Textures
                if (m_assetPath.StartsWith(ItemsAssetPath))
                {
                    if (m_fileName.EndsWith("_AM"))
                    {
                        _assetValidatorSo.ItemTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_NM"))
                    {
                        _assetValidatorSo.ItemTexturePresets.affixPresetPairs[1].preset.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_ICN"))
                    {
                        _assetValidatorSo.ItemTexturePresets.affixPresetPairs[2].preset.ApplyTo(m_asset);
                    }
                    else
                    {
                        _assetValidatorSo.ItemTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                    }
                }

                // UI and Icons
                if (m_assetPath.StartsWith(UiAssetPath))
                {
                    if (m_assetPath.EndsWith(IconPath)) // Engine rendered
                    {
                        _assetValidatorSo.UiTexturePresets.affixPresetPairs[2].preset.ApplyTo(m_asset);
                    }
                    else
                    {
                        if (m_fileName.EndsWith("_AM"))
                        {
                            _assetValidatorSo.UiTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                        }
                        else if (m_fileName.EndsWith("_ICN"))
                        {
                            _assetValidatorSo.UiTexturePresets.affixPresetPairs[1].preset.ApplyTo(m_asset);
                        }
                        else if (m_fileName.EndsWith("_EICN"))
                        {
                            _assetValidatorSo.UiTexturePresets.affixPresetPairs[2].preset.ApplyTo(m_asset);
                        }
                        else
                        {
                            _assetValidatorSo.UiTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                        }
                    }
                }

                // VFX Textures
                if (m_assetPath.StartsWith(VfxAssetPath))
                {
                    if (m_fileName.EndsWith("_AM"))
                    {
                        _assetValidatorSo.VfxTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_NM"))
                    {
                        _assetValidatorSo.VfxTexturePresets.affixPresetPairs[1].preset.ApplyTo(m_asset);
                    }
                    else
                    {
                        _assetValidatorSo.VfxTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                    }
                }

                m_asset.SaveAndReimport();
            }
        }


        private void OnPreprocessModel()
        {
            if (_assetValidatorSo == null)
            {
                ValidatorInit();
            }
            // Only run on initial import, Allows artist overwrites to be persistant when iterating an asset in external software, or to delete and discard
            // files, therefore reimporting, without overwriting thier original settings
            if (assetImporter.importSettingsMissing && shouldProcess)
            {
                ProcessModel(assetPath);
            }
        }

        private void ProcessModel(string m_assetPath)
        {
            var m_fileName = Path.GetFileNameWithoutExtension(m_assetPath);
            // Get a reference to the assetImporter which is contained in the class we've inherited from AssetPostProcessor		
            ModelImporter m_asset = assetImporter as ModelImporter;

            if (m_asset != null)
            {
                // Character models
                if (m_assetPath.StartsWith(CharacterAssetPath))
                {
                    if (!string.IsNullOrEmpty(_assetValidatorSo.CharacterModelPresets.wildcard) && m_assetPath.Contains(_assetValidatorSo.CharacterModelPresets.wildcard)) // inc @
                    {
                        m_asset.animationType = _assetValidatorSo.animationType;
                        m_asset.avatarSetup = _assetValidatorSo.avatarSetup;
                        m_asset.sourceAvatar = _assetValidatorSo.SourceAvatar;
                        m_asset.skinWeights = _assetValidatorSo.skinWeights;
                        m_asset.materialImportMode = _assetValidatorSo.materialImportMode;
                        m_asset.animationCompression = _assetValidatorSo.animationCompression;
                        m_asset.importAnimation = _assetValidatorSo.importAnimation;
                        m_asset.resampleCurves = _assetValidatorSo.resampleCurves;
                        //m_asset.clipAnimations = m_asset.defaultClipAnimations;
                        m_asset.SaveAndReimport();
                    }
                    else if (m_fileName.EndsWith("_MO"))
                    {
                        _assetValidatorSo.CharacterModelPresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                        m_asset.SaveAndReimport();
                    }
                    else
                    {
                        _assetValidatorSo.CharacterModelPresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                        m_asset.SaveAndReimport();
                    }
                }

                // Avatar models
                if (m_assetPath.StartsWith(AvatarAssetPath))
                {
                    if (!string.IsNullOrEmpty(_assetValidatorSo.AvatarModelPresets.wildcard) && m_assetPath.Contains(_assetValidatorSo.AvatarModelPresets.wildcard)) // inc @
                    {
                        m_asset.animationType = _assetValidatorSo.AvaAnimationType;
                        m_asset.avatarSetup = _assetValidatorSo.AvaAvatarSetup;
                        m_asset.sourceAvatar = _assetValidatorSo.AvaSourceAvatar;
                        m_asset.skinWeights = _assetValidatorSo.AvaSkinWeights;
                        m_asset.materialImportMode = _assetValidatorSo.AvaMaterialImportMode;
                        m_asset.animationCompression = _assetValidatorSo.AvaAnimationCompression;
                        m_asset.importAnimation = _assetValidatorSo.AvaImportAnimation;
                        m_asset.resampleCurves = _assetValidatorSo.AvaResampleCurves;
                        //m_asset.clipAnimations = m_asset.defaultClipAnimations;
                        m_asset.SaveAndReimport();
                    }
                    else if (m_fileName.EndsWith("_MO"))
                    {
                        _assetValidatorSo.CharacterModelPresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                        m_asset.SaveAndReimport();
                    }
                    else
                    {
                        _assetValidatorSo.CharacterModelPresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                        m_asset.SaveAndReimport();
                    }
                }

                // Pet models
                if (m_assetPath.StartsWith(PetsAssetPath))
                {
                    if (string.IsNullOrEmpty(_assetValidatorSo.PetModelPresets.wildcard) && m_assetPath.Contains(_assetValidatorSo.PetModelPresets.wildcard)) // inc @
                    {
                        m_asset.animationType = _assetValidatorSo.petAnimationType;
                        m_asset.avatarSetup = _assetValidatorSo.petAvatarSetup;
                        m_asset.sourceAvatar = _assetValidatorSo.petSourceAvatar;
                        m_asset.skinWeights = _assetValidatorSo.petSkinWeights;
                        m_asset.materialImportMode = _assetValidatorSo.petMaterialImportMode;
                        m_asset.animationCompression = _assetValidatorSo.petAnimationCompression;
                        m_asset.importAnimation = _assetValidatorSo.petImportAnimation;
                        m_asset.resampleCurves = _assetValidatorSo.petResampleCurves;
                        //m_asset.clipAnimations = m_asset.defaultClipAnimations;
                        m_asset.SaveAndReimport();
                    }
                    else if (m_fileName.EndsWith("_MO"))
                    {
                        _assetValidatorSo.PetModelPresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                        m_asset.SaveAndReimport();
                    }
                    else
                    {
                        _assetValidatorSo.PetModelPresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                        m_asset.SaveAndReimport();
                    }
                }

                // Environment models
                if (m_assetPath.StartsWith(EnvironmentAssetPath))
                {
                    if (!string.IsNullOrEmpty(_assetValidatorSo.EnvironmentModelPresets.wildcard) && m_assetPath.Contains(_assetValidatorSo.EnvironmentModelPresets.wildcard))
                    {
                        _assetValidatorSo.envModelPresetWildcard.ApplyTo(m_asset);
                    }
                    else if (m_fileName.StartsWith("ENV_SRC_"))
                    {
                        _assetValidatorSo.EnvironmentModelPresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                        m_asset.SaveAndReimport();
                    }
                    else if (m_fileName.EndsWith("_MO"))
                    {
                        _assetValidatorSo.EnvironmentModelPresets.affixPresetPairs[1].preset.ApplyTo(m_asset);
                        m_asset.SaveAndReimport();
                    }
                    else
                    {
                        _assetValidatorSo.EnvironmentModelPresets.affixPresetPairs[1].preset.ApplyTo(m_asset);
                        m_asset.SaveAndReimport();
                    }
                }

                // Building models
                if (m_assetPath.StartsWith(BuildingsAssetPath))
                {
                    if (!string.IsNullOrEmpty(_assetValidatorSo.buildingModelPresets.wildcard) && m_assetPath.Contains(_assetValidatorSo.buildingModelPresets.wildcard)) // inc @
                    {
                        _assetValidatorSo.bldModelPresetWildcard.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_MO"))
                    {
                        _assetValidatorSo.buildingModelPresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                        m_asset.SaveAndReimport();
                    }
                    else
                    {
                        _assetValidatorSo.buildingModelPresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                        m_asset.SaveAndReimport();
                    }
                }

                // Item models
                if (m_assetPath.StartsWith(ItemsAssetPath))
                {
                    if (!string.IsNullOrEmpty(_assetValidatorSo.ItemsModelPresets.wildcard) && m_assetPath.Contains(_assetValidatorSo.ItemsModelPresets.wildcard)) // inc @
                    {
                        _assetValidatorSo.itmModelPresetWildcard.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_MO"))
                    {
                        _assetValidatorSo.ItemsModelPresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                        m_asset.SaveAndReimport();
                    }
                    else
                    {
                        _assetValidatorSo.ItemsModelPresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                        m_asset.SaveAndReimport();
                    }
                }

                // VFX models
                if (m_assetPath.StartsWith(VfxAssetPath))
                {
                    if (!string.IsNullOrEmpty(_assetValidatorSo.VfxModelPresets.wildcard) && m_assetPath.Contains(_assetValidatorSo.VfxModelPresets.wildcard)) // inc @
                    {
                        _assetValidatorSo.vfxModelPresetWildcard.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_MO"))
                    {
                        _assetValidatorSo.VfxModelPresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                        m_asset.SaveAndReimport();
                    }
                    else
                    {
                        _assetValidatorSo.VfxModelPresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                        m_asset.SaveAndReimport();
                    }
                }

                m_asset.SaveAndReimport();
            }
        }

        private void OnPreprocessAudio()
        {
            if (_assetValidatorSo == null)
            {
                ValidatorInit();
            }
            // Only run on initial import, Allows artist overwrites to be persistant when iterating an asset in external software, or to delete and discard
            // files, therefore reimporting, without overwriting thier original settings
            if (assetImporter.importSettingsMissing && shouldProcess)
            {
                ProcessAudio(assetPath);
            }
        }

        private void ProcessAudio(string m_assetPath)
        {
            var m_fileName = Path.GetFileNameWithoutExtension(m_assetPath);
            // Get a reference to the assetImporter which is contained in the class we've inherited from AssetPostProcessor		
            AudioImporter m_asset = assetImporter as AudioImporter;
            if (m_asset != null)
            {
                // Audio files
                if (m_assetPath.StartsWith(AudioAssetPath))
                {
                    if (m_fileName.EndsWith("_LOOP"))
                    {
                        _assetValidatorSo.AudioClipPresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_MUS"))
                    {
                        _assetValidatorSo.AudioClipPresets.affixPresetPairs[1].preset.ApplyTo(m_asset);
                    }
                    else if (m_fileName.EndsWith("_SFX"))
                    {
                        _assetValidatorSo.AudioClipPresets.affixPresetPairs[2].preset.ApplyTo(m_asset);
                    }
                    else
                    {
                        _assetValidatorSo.ItemTexturePresets.affixPresetPairs[0].preset.ApplyTo(m_asset);
                    }
                }

                m_asset.SaveAndReimport();
            }
        }
    }
}
