﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Oxalis.TechArt
{
	public class AssetValidatorWindow : EditorWindow
	{
		private AssetValidator _assetValidator = new AssetValidator();

		private static ScriptableObject _config;

		private static Editor _editor;
		
		private Vector2 _configPanelScrollPosition = new Vector2();

		private Vector2 _messagePanelScrollPosition = new Vector2();

		private int _marginSize = 4;

		private float _messagePanelYoffset;

		private float _progressBarYOffset;

		private GUIStyle _totalButtonStyle;
		
		private GUIStyle _logMessageButtonStyle;
		
		private static readonly Rect DefaultSize = new Rect(400, 400, 600, 800);
		
		private bool _showNamingErrors = false;
		private bool _showTextureErrors = false;
		private bool _showModelErrors = false;
		private bool _showAudioErrors = false;
		private bool _showConformInfo = false;

		private List<AssetValidator.ValidationMessageInfo> _filteredList = new List<AssetValidator.ValidationMessageInfo>();

		private bool _isCurrentlyValidating = false;
		
		public void OnEnable()
		{
			SetupStyles();
		}

		[MenuItem("Moonfrost/Asset Validator")]
		public static void ShowWindow()
		{
			_config = AssetDatabase.LoadMainAssetAtPath("Assets/Editor/AssetValidator/MF_AssetValidatorSO.asset") as ScriptableObject;
			EditorWindow win = GetWindowWithRect<AssetValidatorWindow>(DefaultSize, false, "Asset Validator");
		}

		private void SetupStyles()
		{
			_totalButtonStyle = new GUIStyle(EditorStyles.toolbarButton);
			_totalButtonStyle.alignment = TextAnchor.MiddleLeft;
			_totalButtonStyle.margin = new RectOffset(_marginSize, _marginSize, 0, 0);
			_totalButtonStyle.onActive.background = Texture2D.whiteTexture;
			_totalButtonStyle.onNormal.background = Texture2D.normalTexture;

			_logMessageButtonStyle = new GUIStyle(EditorStyles.toolbarButton);
			_logMessageButtonStyle.alignment = TextAnchor.MiddleLeft;
			_logMessageButtonStyle.richText = true;
			_logMessageButtonStyle.margin = new RectOffset(0, 0, 0, 0);
			_logMessageButtonStyle.border = new RectOffset(0, 0, 0, 0);
			_logMessageButtonStyle.overflow = new RectOffset(0, 0, 0, 0);
		}

		private void OnGUI()
		{
			DrawButtons();

			DrawProgressBar();
			
			DrawConfigPanel();

			if (_isCurrentlyValidating)
			{
				ValidateBatch();
			}
			
			DrawMessageTotals();
			
			DrawMessagePanel();
		}
		
		private void DrawButtons()
		{
			GUILayout.BeginHorizontal();

			if (GUILayout.Button("Validate All"))
			{
				_assetValidator.FindAllAssetsToValidate();
				_isCurrentlyValidating = true;
				Repaint();
			}

			if (GUILayout.Button("Validate Selected"))
			{
				_assetValidator.FindSelectedAssetsToValidate();
				_isCurrentlyValidating = true;
				Repaint();
			}

			if (GUILayout.Button("Conform Selected"))
			{
				_assetValidator.ConformSelected();
				_isCurrentlyValidating = true;
				Repaint();
			}
		
			GUILayout.EndHorizontal();
		}

		private void DrawMessageTotals()
		{
			GUILayout.Space(_marginSize * 2);
			
			GUIContent errorIcon = EditorGUIUtility.IconContent("console.erroricon");
			GUIContent infoIcon = EditorGUIUtility.IconContent("console.infoicon");
			
			GUILayout.BeginVertical();

			float buttonWidth = 300.0f;
			
			int textureErrorTotal = _assetValidator.GetErrorCount(AssetValidator.ValidationMessageType.TextureError);
			GUIContent texturesTotalContent = new GUIContent($"Texture Total Errors: {textureErrorTotal}", errorIcon.image );
			_showTextureErrors = GUILayout.Toggle(_showTextureErrors, texturesTotalContent, _totalButtonStyle, GUILayout.Width(buttonWidth));
				
			int modelErrorCount = _assetValidator.GetErrorCount(AssetValidator.ValidationMessageType.ModelError);
			GUIContent modelsTotalContent = new GUIContent($"Model Total Errors: {modelErrorCount}", errorIcon.image);
			_showModelErrors = GUILayout.Toggle(_showModelErrors, modelsTotalContent, _totalButtonStyle, GUILayout.Width(buttonWidth));

			int audioErrorCount = _assetValidator.GetErrorCount(AssetValidator.ValidationMessageType.AudioError);
			GUIContent audioTotalContent = new GUIContent($"Audio Total Errors: {audioErrorCount}", errorIcon.image);
			_showAudioErrors = GUILayout.Toggle(_showAudioErrors, audioTotalContent, _totalButtonStyle, GUILayout.Width(buttonWidth));

			int namingErrorCount = _assetValidator.GetErrorCount(AssetValidator.ValidationMessageType.NamingConventionError);
			GUIContent nameTotalContent = new GUIContent($"Name Convention Total Errors: {namingErrorCount}", errorIcon.image);
			_showNamingErrors = GUILayout.Toggle(_showNamingErrors, nameTotalContent, _totalButtonStyle, GUILayout.Width(buttonWidth));

			int assetsConformedCount = _assetValidator.GetErrorCount(AssetValidator.ValidationMessageType.ConformSuccess);
			GUIContent conformTotalContent = new GUIContent($"Conformed Assets Total: {assetsConformedCount}", infoIcon.image);
			_showConformInfo = GUILayout.Toggle(_showConformInfo, conformTotalContent, _totalButtonStyle, GUILayout.Width(buttonWidth));
			
			ReCalculateFilteredErrors();
				
			GUILayout.EndVertical();
		}

		private void DrawConfigPanel()
		{
			GUILayout.Label("Validator Settings", EditorStyles.largeLabel);
			GUILayout.BeginVertical(EditorStyles.helpBox);
			_configPanelScrollPosition = GUILayout.BeginScrollView(_configPanelScrollPosition, GUILayout.MaxHeight(300.0f));
			if (_config != null)
			{
				Editor editor = Editor.CreateEditor( _config );
				editor.DrawDefaultInspector();
			}
			GUILayout.EndScrollView();
			GUILayout.EndVertical();
		}

		private void DrawMessagePanel()
		{
			if (Event.current.type == EventType.Repaint)
			{
				Rect lastRect = GUILayoutUtility.GetLastRect();
				_messagePanelYoffset = lastRect.yMax;
			}
			
			float scrollViewHeight = DefaultSize.height - _messagePanelYoffset - _marginSize;
			GUILayout.BeginArea(new Rect(_marginSize, _messagePanelYoffset,DefaultSize.width - (_marginSize * 2),scrollViewHeight), EditorStyles.helpBox);
			
			_messagePanelScrollPosition = GUILayout.BeginScrollView(_messagePanelScrollPosition, GUILayout.ExpandHeight(true));
			const int messageItemHeight = 18;

			int viewCount = (int) (scrollViewHeight / messageItemHeight);

			int firstIndex = (int)(_messagePanelScrollPosition.y / messageItemHeight);
			int firstIndexLimit = Mathf.Max(0, _filteredList.Count - viewCount);
			firstIndex = Mathf.Clamp(firstIndex, 0, firstIndexLimit);
			
			int lastIndex = firstIndex + viewCount;
			lastIndex = Mathf.Clamp(lastIndex, 0, _filteredList.Count);
			
			GUILayout.Space(firstIndex * messageItemHeight);
			
			for (int i = firstIndex; i < lastIndex; i++)
			{
				if (GUILayout.Button(_filteredList[i].MessageString, _logMessageButtonStyle, GUILayout.Height(messageItemHeight)))
				{
					Selection.activeObject = AssetDatabase.LoadMainAssetAtPath(_filteredList[i].AssetPath);
				}
			}
			
			GUILayout.Space((_filteredList.Count - lastIndex) * messageItemHeight);
			GUILayout.EndScrollView();
			
			GUILayout.EndArea();
		}
		
		private void DrawProgressBar()
		{
			if (Event.current.type == EventType.Repaint)
			{
				Rect rect = GUILayoutUtility.GetLastRect();
				_progressBarYOffset = rect.height;
			}

			string progressBarLabel = $"{_assetValidator.AssetsValidated} / {_assetValidator.AssetsToValidate} Assets Processed";
			float progressBarHeight = 20.0f;
            
			EditorGUI.ProgressBar(new Rect(_marginSize, _progressBarYOffset + _marginSize, DefaultSize.width - (_marginSize * 2), progressBarHeight), (float)_assetValidator.AssetsValidated / _assetValidator.AssetsToValidate, progressBarLabel);
			GUILayout.Space(progressBarHeight + (_marginSize * 2));
		}
		
		private void ReCalculateFilteredErrors()
		{
			_filteredList.Clear();
			
			foreach (var validationMessage in _assetValidator.ValidationMessageList)
			{
				if (validationMessage.MessageType == AssetValidator.ValidationMessageType.TextureError && _showTextureErrors)
				{
					_filteredList.Add(validationMessage);
				}
				else if (validationMessage.MessageType == AssetValidator.ValidationMessageType.ModelError && _showModelErrors)
				{
					_filteredList.Add(validationMessage);
				}
				else if (validationMessage.MessageType == AssetValidator.ValidationMessageType.AudioError && _showAudioErrors)
				{
					_filteredList.Add(validationMessage);
				}
				else if (validationMessage.MessageType == AssetValidator.ValidationMessageType.NamingConventionError && _showNamingErrors)
				{
					_filteredList.Add(validationMessage);
				}
				else if (validationMessage.MessageType == AssetValidator.ValidationMessageType.ConformSuccess && _showConformInfo)
				{
					_filteredList.Add(validationMessage);
				}
				else if (validationMessage.MessageType == AssetValidator.ValidationMessageType.ValidationSuccess)
				{
					_filteredList.Add(validationMessage);
				}
			}
		}
		
		private void ValidateBatch()
		{
			_assetValidator.ValidateAssets();
			Repaint();
			if (_assetValidator.AssetsValidated >= _assetValidator.AssetsToValidate)
			{
				_isCurrentlyValidating = false;
			}
		}
		
	}
}


