using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine.AI;

namespace Oxalis.TechArt
{
    public class MultiModelImporter : Editor
    {
        public const string _defaultSvnPath = "D:/MoonfrostSvn/MoonfrostArtRepo";
        //private const string _assetVaildatorSoPath = "Assets/Editor/AssetValidator/GEN_MF_AssetValidator_SO.asset";
        private const string _pixelShaderPath = "Preproduction/CHA_CustomisationNoPixel_SHD";
        private const string _avatarShaderPath = "Preproduction/CHA_CustomisationNoPixel_SHD";
        private const string _characterMasterPbPath = "Assets/Editor/MultiImporter/MasterPrefab_Presets/CHA_Master_01_PB.prefab";
        private const string _avatarMasterPbPath = "Assets/Editor/MultiImporter/MasterPrefab_Presets/AVA_Master_01_PB.prefab";
        private const string _buildingMasterPbPath = "Assets/Editor/MultiImporter/MasterPrefab_Presets/BLD_Master_01_PB.prefab";
        private const string _environmentMasterPbPath = "Assets/Editor/MultiImporter/MasterPrefab_Presets/ENV_Master_01_PB.prefab";
        private const string _itemMasterPbPath = "Assets/Editor/MultiImporter/MasterPrefab_Presets/ITM_Master_01_PB.prefab";
        private const string _petMasterCtrlPath = "Assets/Editor/MultiImporter/MasterPrefab_Presets/PET_Master_01_CTRL.controller";

        public static void ImportModel()
        {
            var m_sourceModelPath = EditorUtility.OpenFilePanel("Select source model export", _defaultSvnPath, "fbx");
            if (!string.IsNullOrEmpty(m_sourceModelPath))
            {
                var m_modelName = Path.GetFileName(m_sourceModelPath);
                AssetValidator m_assetValidator = new();
                int m_assetDiscipline = m_assetValidator.ValidateModel(m_sourceModelPath, true);

                if (m_assetDiscipline != 9) //9 is an error code, So checking if valid
                {
                    AssetSettingsPresetter.shouldProcess = true;
                    Menu.SetChecked("Moonfrost/Asset Presetter", AssetSettingsPresetter.shouldProcess);
                }

                AssetValidatorSO _assetValidatorSo = AssetDatabase.LoadMainAssetAtPath(AssetSettingsPresetter._assetVaildatorSoPath) as AssetValidatorSO;
                var m_cleanName = Regex.Replace(m_modelName, "_MO.FBX", "", RegexOptions.IgnoreCase);

                switch (m_assetDiscipline)
                {
                    default:
                        break;

                    case 0: // Character
                        if (!string.IsNullOrEmpty(_assetValidatorSo.CharacterModelPresets.wildcard) && m_modelName.Contains(_assetValidatorSo.CharacterModelPresets.wildcard))
                        {
                            Debug.Log((AssetValidatorSO.AssetDiscipline)m_assetDiscipline + " anim");
                            // Copy source model
                            var m_modelPath = Directory.CreateDirectory(AssetSettingsPresetter.CharacterAssetPath + "/Animation");
                            FileUtil.ReplaceFile(m_sourceModelPath, m_modelPath + "/" + m_modelName);
                        }
                        else
                        {
                            //Copy source model
                            var m_modelPath = Directory.CreateDirectory(AssetSettingsPresetter.CharacterAssetPath + "/Models");
                            FileUtil.ReplaceFile(m_sourceModelPath, m_modelPath + "/" + m_modelName);
                            //Create material
                            Material m_material = new Material(Shader.Find(_pixelShaderPath));
                            AssetDatabase.CreateAsset(m_material, AssetSettingsPresetter.CharacterAssetPath + "/Materials/" + m_cleanName + "_MAT.mat");
                            AssetDatabase.Refresh();
                            //Create prefab variant                            
                            GameObject m_masterPb = (GameObject)AssetDatabase.LoadMainAssetAtPath(_characterMasterPbPath);
                            GameObject m_instanceMaster = (GameObject)PrefabUtility.InstantiatePrefab(m_masterPb);
                            var m_instanceMesh = (GameObject)AssetDatabase.LoadMainAssetAtPath($"{AssetSettingsPresetter.CharacterAssetPath}/Models/{m_modelName}");
                            m_instanceMesh = (GameObject)PrefabUtility.InstantiatePrefab(m_instanceMesh);

                            Directory.CreateDirectory(AssetSettingsPresetter.CharacterAssetPath + "/Data");
                            Directory.CreateDirectory(AssetSettingsPresetter.CharacterAssetPath + "/Prefabs");

                            // check if data object exists by trying to load it
                            WearableDefinition m_wearableDefinition = null;

                            m_wearableDefinition = AssetDatabase.LoadMainAssetAtPath($"{AssetSettingsPresetter.CharacterAssetPath}/Data/{m_cleanName}_SO.asset") as WearableDefinition;

                            if (m_wearableDefinition == null) // if wasn't previously created by character import
                            {
                                m_wearableDefinition = ScriptableObject.CreateInstance<WearableDefinition>();
                                AssetDatabase.CreateAsset(m_wearableDefinition, $"{AssetSettingsPresetter.CharacterAssetPath}/Data/{m_cleanName}_SO.asset");
                            }

                            //Edit the instanced master prefab
                            m_instanceMaster.GetComponent<SkinnedMeshRenderer>().sharedMesh = m_instanceMesh.GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh;
                            m_instanceMaster.GetComponent<SkinnedMeshRenderer>().material = m_material;
                            var m_variantPB = PrefabUtility.SaveAsPrefabAsset(m_instanceMaster, $"{AssetSettingsPresetter.CharacterAssetPath}/Prefabs/{m_cleanName}_PB.prefab");
                            m_wearableDefinition.chibiAsset = m_variantPB.GetComponent<SkinnedMeshRenderer>();

                            //Clean instances from scene
                            DestroyImmediate(m_instanceMesh);
                            DestroyImmediate(m_instanceMaster);
                            EditorUtility.SetDirty(m_wearableDefinition);
                        }
                        break;

                    case 1: // Avatar
                        if (!string.IsNullOrEmpty(_assetValidatorSo.AvatarModelPresets.wildcard) && m_modelName.Contains(_assetValidatorSo.AvatarModelPresets.wildcard))
                        {
                            // Copy source model
                            var m_modelPath = Directory.CreateDirectory(AssetSettingsPresetter.AvatarAssetPath + "/Animation");
                            FileUtil.ReplaceFile(m_sourceModelPath, m_modelPath + "/" + m_modelName);
                        }
                        else
                        {
                            //Copy source model
                            var m_modelPath = Directory.CreateDirectory(AssetSettingsPresetter.AvatarAssetPath + "/Models");
                            FileUtil.ReplaceFile(m_sourceModelPath, m_modelPath + "/" + m_modelName);
                            AssetDatabase.Refresh();

                            //Create material
                            Material m_material = new Material(Shader.Find(_avatarShaderPath));
                            AssetDatabase.CreateAsset(m_material, AssetSettingsPresetter.AvatarAssetPath + "/Materials/" + m_cleanName + "_MAT.mat");
                            AssetDatabase.Refresh();
                            //Create prefab variant                            
                            GameObject m_masterPb = (GameObject)AssetDatabase.LoadMainAssetAtPath(_avatarMasterPbPath);
                            GameObject m_instanceMaster = (GameObject)PrefabUtility.InstantiatePrefab(m_masterPb);
                            var m_instanceMesh = (GameObject)AssetDatabase.LoadMainAssetAtPath($"{AssetSettingsPresetter.AvatarAssetPath}/Models/{m_modelName}");
                            m_instanceMesh = (GameObject)PrefabUtility.InstantiatePrefab(m_instanceMesh);

                            //Create directories
                            Directory.CreateDirectory(AssetSettingsPresetter.CharacterAssetPath + "/Data"); // Same data path and object shared between avatar and characters
                            Directory.CreateDirectory(AssetSettingsPresetter.AvatarAssetPath + "/Prefabs");

                            // check if data object exists by trying to load it
                            WearableDefinition m_wearableDefinition = null;
                            string m_nameFromOtherType = Regex.Replace(m_modelName, "AVA_", "CHA_", RegexOptions.IgnoreCase);
                            m_nameFromOtherType = Regex.Replace(m_nameFromOtherType, "_MO.FBX", "", RegexOptions.IgnoreCase);

                            m_wearableDefinition = AssetDatabase.LoadMainAssetAtPath($"{AssetSettingsPresetter.CharacterAssetPath}/Data/{m_nameFromOtherType}_SO.asset") as WearableDefinition;

                            if (m_wearableDefinition == null) // if wasn't previously created by character import
                            {
                                m_wearableDefinition = ScriptableObject.CreateInstance<WearableDefinition>();
                                AssetDatabase.CreateAsset(m_wearableDefinition, $"{AssetSettingsPresetter.CharacterAssetPath}/Data/{m_nameFromOtherType}_SO.asset");
                            }

                            //Edit the instanced master prefab
                            m_instanceMaster.GetComponent<SkinnedMeshRenderer>().sharedMesh = m_instanceMesh.GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh;
                            m_instanceMaster.GetComponent<SkinnedMeshRenderer>().material = m_material;
                            var m_variantPB = PrefabUtility.SaveAsPrefabAsset(m_instanceMaster, $"{AssetSettingsPresetter.AvatarAssetPath}/Prefabs/{m_cleanName}_PB.prefab");
                            m_wearableDefinition.avatarAsset = m_variantPB.GetComponent<SkinnedMeshRenderer>();

                            //Clean instances from scene
                            DestroyImmediate(m_instanceMesh);
                            DestroyImmediate(m_instanceMaster);
                            EditorUtility.SetDirty(m_wearableDefinition);
                        }

                        break;

                    case 2: // Pet
                        if (!string.IsNullOrEmpty(_assetValidatorSo.PetModelPresets.wildcard) && m_modelName.Contains(_assetValidatorSo.PetModelPresets.wildcard))
                        {
                            Debug.Log((AssetValidatorSO.AssetDiscipline)m_assetDiscipline + " anim");
                            // Copy source model
                            var m_modelPath = Directory.CreateDirectory(AssetSettingsPresetter.PetsAssetPath + "/Animation");
                            FileUtil.ReplaceFile(m_sourceModelPath, m_modelPath + "/" + m_modelName);
                        }
                        else
                        {
                            //Copy source model
                            var m_modelPath = Directory.CreateDirectory(AssetSettingsPresetter.PetsAssetPath + "/Models");
                            FileUtil.ReplaceFile(m_sourceModelPath, m_modelPath + "/" + m_modelName);

                            // Copy texture source
                            var m_sourceTexturePath = EditorUtility.OpenFilePanel("Select source albedo texture", _defaultSvnPath, "png");
                            var m_textureName = Path.GetFileName(m_sourceTexturePath);
                            FileUtil.ReplaceFile(m_sourceTexturePath, AssetSettingsPresetter.PetsAssetPath + "/Textures/" + m_textureName);
                            AssetDatabase.Refresh();

                            //Create material
                            Material m_material = new Material(Shader.Find(_pixelShaderPath));
                            m_material.SetTexture("_Albedo", (Texture)AssetDatabase.LoadMainAssetAtPath(AssetSettingsPresetter.PetsAssetPath + "/Textures/" + m_textureName));
                            AssetDatabase.Refresh();

                            AssetDatabase.CreateAsset(m_material, AssetSettingsPresetter.PetsAssetPath + "/Materials/" + m_cleanName + "_MAT.mat");
                            AssetDatabase.Refresh();

                            // Need model hierachy here, so have to make the prefab from model rather than master variant
                            var m_instanceMesh = (GameObject)AssetDatabase.LoadMainAssetAtPath($"{AssetSettingsPresetter.PetsAssetPath}/Models/{m_modelName}");
                            m_instanceMesh = Instantiate(m_instanceMesh);

                            var m_parentGameObject = new GameObject();
                            m_instanceMesh.transform.parent = m_parentGameObject.transform;
                            m_parentGameObject.name = m_instanceMesh.name;
                            m_instanceMesh.name = "Puppet";


                            var m_capsuleCollider = m_instanceMesh.AddComponent<CapsuleCollider>();
                            m_capsuleCollider.center = new Vector3(0f, 0.5f, 0f);
                            m_capsuleCollider.height = 1.5f;
                            m_capsuleCollider.direction = 2; // 2 = Z axis

                            var m_animator = m_instanceMesh.AddComponent<Animator>();
                            AnimatorOverrideController m_overrideController = new();
                            m_overrideController.runtimeAnimatorController = (RuntimeAnimatorController)AssetDatabase.LoadMainAssetAtPath(_petMasterCtrlPath);
                            AssetDatabase.CreateAsset(m_overrideController, AssetSettingsPresetter.PetsAssetPath + "/Models/" + m_cleanName + "_CTRL.overrideController");
                            m_animator.runtimeAnimatorController = m_overrideController;

                            //pet script required
                            m_parentGameObject.AddComponent<SphereCollider>().isTrigger = true;

                            var m_navAgent = m_parentGameObject.AddComponent<NavMeshAgent>();
                            m_navAgent.baseOffset = 0.22f;
                            m_navAgent.stoppingDistance = 0;
                            m_navAgent.radius = 0.3f;
                            m_navAgent.height = 0.5f;

                            var m_charController = m_parentGameObject.AddComponent<CharacterController>();
                            m_charController.height = 0.8f;
                            //....

                            m_instanceMesh.layer = 7; // Layer human character 

                            m_instanceMesh.GetComponentInChildren<SkinnedMeshRenderer>().material = m_material;

                            Directory.CreateDirectory(AssetSettingsPresetter.PetsAssetPath + "/Prefabs");
                            PrefabUtility.SaveAsPrefabAsset(m_parentGameObject, $"{AssetSettingsPresetter.PetsAssetPath}/Prefabs/{m_cleanName}_PB.prefab");

                            //Clean instances from scene
                            DestroyImmediate(m_parentGameObject);
                        }
                        break;

                    case 3: // Environment
                        if (!string.IsNullOrEmpty(_assetValidatorSo.EnvironmentModelPresets.wildcard) && m_modelName.Contains(_assetValidatorSo.EnvironmentModelPresets.wildcard))
                        {
                            Debug.Log((AssetValidatorSO.AssetDiscipline)m_assetDiscipline + " anim");
                            // Copy source model
                            var m_modelPath = Directory.CreateDirectory(AssetSettingsPresetter.EnvironmentAssetPath + "/Animation");
                            FileUtil.ReplaceFile(m_sourceModelPath, m_modelPath + "/" + m_modelName);
                        }
                        else
                        {
                            //Copy source model
                            var m_modelPath = Directory.CreateDirectory(AssetSettingsPresetter.EnvironmentAssetPath + "/Models");
                            FileUtil.ReplaceFile(m_sourceModelPath, m_modelPath + "/" + m_modelName);

                            // Copy texture source
                            var m_sourceTexturePath = EditorUtility.OpenFilePanel("Select source albedo texture", _defaultSvnPath, "png");
                            var m_textureName = Path.GetFileName(m_sourceTexturePath);
                            FileUtil.ReplaceFile(m_sourceTexturePath, AssetSettingsPresetter.EnvironmentAssetPath + "/Textures/" + m_textureName);
                            AssetDatabase.Refresh();

                            //Create material
                            Material m_material = new Material(Shader.Find(_pixelShaderPath));
                            m_material.SetTexture("_Albedo", (Texture)AssetDatabase.LoadMainAssetAtPath(AssetSettingsPresetter.EnvironmentAssetPath + "/Textures/" + m_textureName));
                            AssetDatabase.Refresh();

                            AssetDatabase.CreateAsset(m_material, AssetSettingsPresetter.EnvironmentAssetPath + "/Materials/" + m_cleanName + "_MAT.mat");
                            AssetDatabase.Refresh();



                            //Create prefab variant                            
                            GameObject m_masterPb = (GameObject)AssetDatabase.LoadMainAssetAtPath(_environmentMasterPbPath);
                            GameObject m_instanceMaster = (GameObject)PrefabUtility.InstantiatePrefab(m_masterPb);
                            var m_instanceMesh = (GameObject)AssetDatabase.LoadMainAssetAtPath($"{AssetSettingsPresetter.EnvironmentAssetPath}/Models/{m_modelName}");
                            var m_puppetTransform = GameObject.Find("Puppet").transform;
                            m_instanceMesh = (GameObject)PrefabUtility.InstantiatePrefab(m_instanceMesh, m_puppetTransform);
                            m_instanceMesh.transform.localPosition = Vector3.zero;

                            var m_instanceShadowMesh = (GameObject)AssetDatabase.LoadMainAssetAtPath($"{AssetSettingsPresetter.EnvironmentAssetPath}/Models/{m_modelName}");
                            m_instanceShadowMesh = (GameObject)PrefabUtility.InstantiatePrefab(m_instanceShadowMesh, m_instanceMesh.transform);
                            m_instanceShadowMesh.transform.localPosition = Vector3.zero;
                            m_instanceShadowMesh.name = "ShadowMesh";

                            Directory.CreateDirectory(AssetSettingsPresetter.EnvironmentAssetPath + "/Prefabs");

                            //Edit the instanced master prefab                            
                            var m_instanceMeshRenderer = m_instanceMesh.GetComponent<MeshRenderer>();
                            m_instanceMeshRenderer.material = m_material;
                            m_instanceMeshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

                            var m_instanceShadowMeshRenderer = m_instanceShadowMesh.GetComponent<MeshRenderer>();
                            m_instanceShadowMeshRenderer.material = m_material;
                            m_instanceShadowMeshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;

                            var m_collider = m_puppetTransform.GetComponentInChildren<BoxCollider>();
                            m_collider.size = m_instanceMeshRenderer.localBounds.extents + m_instanceMeshRenderer.localBounds.extents;
                            m_collider.center = new Vector3(0f, m_instanceMeshRenderer.localBounds.extents.y, 0f);
                            var m_navMeshObstacle = m_puppetTransform.GetComponentInChildren<NavMeshObstacle>();
                            m_navMeshObstacle.size = m_collider.size;
                            m_navMeshObstacle.center = m_collider.center;

                            //m_puppetTransform.gameObject.AddComponent<BasePuppet>().AllMeshRenderers[0] = m_instanceMeshRenderer;
                            //m_instanceMaster.AddComponent<StaticItem>().Puppet = m_puppetTransform.gameObject.GetComponent<BasePuppet>();
                            var m_variantPB = PrefabUtility.SaveAsPrefabAsset(m_instanceMaster, $"{AssetSettingsPresetter.EnvironmentAssetPath}/Prefabs/{m_cleanName}_PB.prefab");

                            //Clean instances from scene
                            DestroyImmediate(m_instanceMesh);
                            DestroyImmediate(m_instanceMaster);
                        }
                        break;

                    case 4: // Ui
                        Debug.LogWarning("UI models end use case not defined - SKIPPING");
                        break;

                    case 5: // Building
                        if (!string.IsNullOrEmpty(_assetValidatorSo.buildingModelPresets.wildcard) && m_modelName.Contains(_assetValidatorSo.buildingModelPresets.wildcard))
                        {
                            Debug.Log((AssetValidatorSO.AssetDiscipline)m_assetDiscipline + " anim");
                            // Copy source model
                            var m_modelPath = Directory.CreateDirectory(AssetSettingsPresetter.BuildingsAssetPath + "/Animation");
                            FileUtil.ReplaceFile(m_sourceModelPath, m_modelPath + "/" + m_modelName);
                        }
                        else
                        {
                            //Copy source model
                            var m_modelPath = Directory.CreateDirectory(AssetSettingsPresetter.BuildingsAssetPath + "/Models");
                            FileUtil.ReplaceFile(m_sourceModelPath, m_modelPath + "/" + m_modelName);

                            // Copy texture source
                            var m_sourceTexturePath = EditorUtility.OpenFilePanel("Select source albedo texture", _defaultSvnPath, "png");
                            var m_textureName = Path.GetFileName(m_sourceTexturePath);

                            Directory.CreateDirectory(AssetSettingsPresetter.BuildingsAssetPath + "/Textures");
                            FileUtil.ReplaceFile(m_sourceTexturePath, AssetSettingsPresetter.BuildingsAssetPath + "/Textures/" + m_textureName);
                            AssetDatabase.Refresh();

                            //Create material
                            Material m_material = new Material(Shader.Find(_pixelShaderPath));
                            m_material.SetTexture("_Albedo", (Texture)AssetDatabase.LoadMainAssetAtPath(AssetSettingsPresetter.BuildingsAssetPath + "/Textures/" + m_textureName));

                            Directory.CreateDirectory(AssetSettingsPresetter.BuildingsAssetPath + "/Materials");
                            AssetDatabase.CreateAsset(m_material, AssetSettingsPresetter.BuildingsAssetPath + "/Materials/" + m_cleanName + "_MAT.mat");
                            AssetDatabase.Refresh();

                            //Create prefab variant                            
                            GameObject m_masterPb = (GameObject)AssetDatabase.LoadMainAssetAtPath(_buildingMasterPbPath);
                            GameObject m_instanceMaster = (GameObject)PrefabUtility.InstantiatePrefab(m_masterPb);
                            var m_instanceMesh = (GameObject)AssetDatabase.LoadMainAssetAtPath($"{AssetSettingsPresetter.BuildingsAssetPath}/Models/{m_modelName}");
                            var m_puppetTransform = GameObject.Find("Puppet").transform;
                            m_instanceMesh = (GameObject)PrefabUtility.InstantiatePrefab(m_instanceMesh, m_puppetTransform);
                            m_instanceMesh.transform.localPosition = Vector3.zero;

                            var m_instanceShadowMesh = (GameObject)AssetDatabase.LoadMainAssetAtPath($"{AssetSettingsPresetter.BuildingsAssetPath}/Models/{m_modelName}");
                            m_instanceShadowMesh = (GameObject)PrefabUtility.InstantiatePrefab(m_instanceShadowMesh, m_instanceMesh.transform);
                            m_instanceShadowMesh.transform.localPosition = Vector3.zero;
                            m_instanceShadowMesh.name = "ShadowMesh";

                            Directory.CreateDirectory(AssetSettingsPresetter.BuildingsAssetPath + "/Prefabs");

                            //Edit the instanced master prefab                            
                            var m_instanceMeshRenderer = m_instanceMesh.GetComponent<MeshRenderer>();
                            m_instanceMeshRenderer.material = m_material;
                            m_instanceMeshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

                            var m_instanceShadowMeshRenderer = m_instanceShadowMesh.GetComponent<MeshRenderer>();
                            m_instanceShadowMeshRenderer.material = m_material;
                            m_instanceShadowMeshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;

                            var m_collider = m_puppetTransform.GetComponentInChildren<BoxCollider>();
                            m_collider.size = m_instanceMeshRenderer.localBounds.extents + m_instanceMeshRenderer.localBounds.extents;
                            m_collider.center = new Vector3 (0f, m_instanceMeshRenderer.localBounds.extents.y, 0f);
                            var m_navMeshObstacle = m_puppetTransform.GetComponentInChildren<NavMeshObstacle>();
                            m_navMeshObstacle.size = m_collider.size;
                            m_navMeshObstacle.center = m_collider.center;

                            //m_puppetTransform.gameObject.AddComponent<BasePuppet>().AllMeshRenderers[0] = m_instanceMeshRenderer;
                            //m_instanceMaster.AddComponent<StaticItem>().Puppet = m_puppetTransform.gameObject.GetComponent<BasePuppet>();
                            var m_variantPB = PrefabUtility.SaveAsPrefabAsset(m_instanceMaster, $"{AssetSettingsPresetter.BuildingsAssetPath}/Prefabs/{m_cleanName}_PB.prefab");

                            //Clean instances from scene
                            DestroyImmediate(m_instanceMesh);
                            DestroyImmediate(m_instanceMaster);
                        }
                        break;

                    case 6: // Items
                        if (!string.IsNullOrEmpty(_assetValidatorSo.ItemsModelPresets.wildcard) && m_modelName.Contains(_assetValidatorSo.ItemsModelPresets.wildcard))
                        {
                            Debug.Log((AssetValidatorSO.AssetDiscipline)m_assetDiscipline + " anim");
                            // Copy source model
                            var m_modelPath = Directory.CreateDirectory(AssetSettingsPresetter.ItemsAssetPath + "/Animation");
                            FileUtil.ReplaceFile(m_sourceModelPath, m_modelPath + "/" + m_modelName);
                        }
                        else
                        {
                            //Copy source model
                            var m_modelPath = Directory.CreateDirectory(AssetSettingsPresetter.ItemsAssetPath + "/Models");
                            FileUtil.ReplaceFile(m_sourceModelPath, m_modelPath + "/" + m_modelName);

                            // Copy texture source
                            var m_sourceTexturePath = EditorUtility.OpenFilePanel("Select source albedo texture", _defaultSvnPath, "png");
                            var m_textureName = Path.GetFileName(m_sourceTexturePath);
                            FileUtil.ReplaceFile(m_sourceTexturePath, AssetSettingsPresetter.ItemsAssetPath + "/Textures/" + m_textureName);
                            AssetDatabase.Refresh();

                            //Create material
                            Material m_material = new Material(Shader.Find(_pixelShaderPath));
                            m_material.SetTexture("_Albedo", (Texture)AssetDatabase.LoadMainAssetAtPath(AssetSettingsPresetter.ItemsAssetPath + "/Textures/" + m_textureName));

                            AssetDatabase.CreateAsset(m_material, AssetSettingsPresetter.ItemsAssetPath + "/Materials/" + m_cleanName + "_MAT.mat");
                            AssetDatabase.Refresh();



                            //Create prefab variant                            
                            GameObject m_masterPb = (GameObject)AssetDatabase.LoadMainAssetAtPath(_itemMasterPbPath);
                            GameObject m_instanceMaster = (GameObject)PrefabUtility.InstantiatePrefab(m_masterPb);
                            var m_instanceMesh = (GameObject)AssetDatabase.LoadMainAssetAtPath($"{AssetSettingsPresetter.ItemsAssetPath}/Models/{m_modelName}");
                            var m_puppetTransform = GameObject.Find("Puppet").transform;
                            m_instanceMesh = (GameObject)PrefabUtility.InstantiatePrefab(m_instanceMesh, m_puppetTransform);
                            m_instanceMesh.transform.localPosition = Vector3.zero;

                            var m_instanceShadowMesh = (GameObject)AssetDatabase.LoadMainAssetAtPath($"{AssetSettingsPresetter.ItemsAssetPath}/Models/{m_modelName}");
                            m_instanceShadowMesh = (GameObject)PrefabUtility.InstantiatePrefab(m_instanceShadowMesh, m_instanceMesh.transform);
                            m_instanceShadowMesh.transform.localPosition = Vector3.zero;
                            m_instanceShadowMesh.name = "ShadowMesh";

                            Directory.CreateDirectory(AssetSettingsPresetter.ItemsAssetPath + "/Prefabs");

                            //Edit the instanced master prefab                            
                            var m_instanceMeshRenderer = m_instanceMesh.GetComponent<MeshRenderer>();
                            m_instanceMeshRenderer.material = m_material;
                            m_instanceMeshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

                            var m_instanceShadowMeshRenderer = m_instanceShadowMesh.GetComponent<MeshRenderer>();
                            m_instanceShadowMeshRenderer.material = m_material;
                            m_instanceShadowMeshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;

                            var m_collider = m_puppetTransform.GetComponentInChildren<BoxCollider>();
                            m_collider.size = m_instanceMeshRenderer.localBounds.extents + m_instanceMeshRenderer.localBounds.extents;
                            m_collider.center = new Vector3(0f, m_instanceMeshRenderer.localBounds.extents.y, 0f);
                            var m_navMeshObstacle = m_puppetTransform.GetComponentInChildren<NavMeshObstacle>();
                            m_navMeshObstacle.size = m_collider.size;
                            m_navMeshObstacle.center = m_collider.center;

                            //m_puppetTransform.gameObject.AddComponent<BasePuppet>().AllMeshRenderers[0] = m_instanceMeshRenderer;
                            //m_instanceMaster.AddComponent<StaticItem>().Puppet = m_puppetTransform.gameObject.GetComponent<BasePuppet>();
                            var m_variantPB = PrefabUtility.SaveAsPrefabAsset(m_instanceMaster, $"{AssetSettingsPresetter.ItemsAssetPath}/Prefabs/{m_cleanName}_PB.prefab");

                            //Clean instances from scene
                            DestroyImmediate(m_instanceMesh);
                            DestroyImmediate(m_instanceMaster);
                        }
                        break;

                    case 7: // Vfx
                        Debug.LogWarning("VFX models end use case not defined - SKIPPING");
                        break;

                    case 8: // Sound
                        Debug.LogWarning("Audio models end use case not defined - SKIPPING");
                        break;

                    case 9: // Error
                        Debug.LogWarning("Unknown prefix - End use case not defined - SKIPPING");
                        break;
                }

                AssetDatabase.Refresh();
            }
        }
    }
}

