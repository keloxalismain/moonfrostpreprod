using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Text.RegularExpressions;

namespace Oxalis.TechArt
{
    public class MultiAudioImporter : MonoBehaviour
    {
        public const string _defaultSvnPath = "D:/MoonfrostSvn/MoonfrostArtRepo";

        private const string _audioLoopPath = "Assets/Audio/Looping/";
        private const string _audioMusicPath = "Assets/Audio/Music/";
        private const string _audioSfxPath = "Assets/Audio/SFX/";

        private static readonly string[] _fileFormats = new string[] { "Audio files", "ogg,mp3,wav", "All files", "*" };

        public static void ImportAudio()
        {
            var m_sourceAudioPath = EditorUtility.OpenFilePanelWithFilters("Select source sound export", _defaultSvnPath, _fileFormats);
            if (!string.IsNullOrEmpty(m_sourceAudioPath))
            {
                var m_audioClipName = Path.GetFileName(m_sourceAudioPath);
                AssetValidator m_assetValidator = new();
                int m_audioClipType = m_assetValidator.ValidateAudio(m_sourceAudioPath, true);

                if (m_audioClipType != 9) //9 is an error code, So checking if valid
                {
                    AssetSettingsPresetter.shouldProcess = true;
                    Menu.SetChecked("Moonfrost/Asset Presetter", AssetSettingsPresetter.shouldProcess);
                }

                switch (m_audioClipType)
                {
                    default:
                        break;

                    case 0: // Looping
                        Directory.CreateDirectory(_audioLoopPath);
                        FileUtil.ReplaceFile(m_sourceAudioPath, _audioLoopPath + m_audioClipName);
                        break;

                    case 1: // Music
                        Directory.CreateDirectory(_audioMusicPath);
                        FileUtil.ReplaceFile(m_sourceAudioPath, _audioMusicPath + m_audioClipName);
                        break;

                    case 2: // Sfx
                        Directory.CreateDirectory(_audioSfxPath);
                        FileUtil.ReplaceFile(m_sourceAudioPath, _audioSfxPath + m_audioClipName);
                        break;
                }

                AssetDatabase.Refresh();
            }
        }
    }
}
