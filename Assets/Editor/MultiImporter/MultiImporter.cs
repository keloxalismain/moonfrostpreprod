using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Oxalis.TechArt
{
    public class MultiImporter : EditorWindow
    {
        [MenuItem("Moonfrost/MultiImporter")]
        static void Init()
        {
            EditorWindow m_window = GetWindow(typeof(MultiImporter));

            Texture m_icon = AssetDatabase.LoadAssetAtPath<Texture>("Assets/_temp/koala-face-clipart_temp.png");
            GUIContent m_titleContent = new GUIContent("MultiImporter", m_icon);
            m_window.titleContent = m_titleContent;
            m_window.minSize = new Vector2(400, 300);

            m_window.Show();
        }

        void OnGUI()
        {
            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Tip of the day:");
            EditorGUILayout.HelpBox("The model name convention should be in the format of `AREA_MyModel_MO`", MessageType.Info);
            EditorGUILayout.Space();

            if (GUILayout.Button("Import Model"))
            {
                Oxalis.TechArt.AssetSettingsPresetter.shouldProcess = true;
                Menu.SetChecked("Moonfrost/Asset Presetter Toggle", Oxalis.TechArt.AssetSettingsPresetter.shouldProcess);
                MultiModelImporter.ImportModel();
            }
            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Tip of the day:");
            EditorGUILayout.HelpBox("The image name convention should be in the format of `AREA_MyImage_TYPE`", MessageType.Info);
            EditorGUILayout.Space();

            if (GUILayout.Button("Import Image"))
            {
                Oxalis.TechArt.AssetSettingsPresetter.shouldProcess = true;
                Menu.SetChecked("Moonfrost/Asset Presetter Toggle", Oxalis.TechArt.AssetSettingsPresetter.shouldProcess);
                //CharacterModelImporter.ImportCharacter();
            }
            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Tip of the day:");
            EditorGUILayout.HelpBox("The sound name convention should be in the format of `AUD_MySound_TYPE`", MessageType.Info);
            EditorGUILayout.Space();

            if (GUILayout.Button("Import Audio"))
            {
                Oxalis.TechArt.AssetSettingsPresetter.shouldProcess = true;
                Menu.SetChecked("Moonfrost/Asset Presetter Toggle", Oxalis.TechArt.AssetSettingsPresetter.shouldProcess);
                MultiAudioImporter.ImportAudio();
            }
            EditorGUILayout.Space();
        }

    }
}
