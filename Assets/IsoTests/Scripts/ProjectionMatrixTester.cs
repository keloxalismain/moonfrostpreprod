using UnityEngine;

[RequireComponent(typeof(Camera))]
public class ProjectionMatrixTester : MonoBehaviour
{
    Camera camera;
    [SerializeField] Matrix4x4 matrix;

    void Start()
    {
        camera = GetComponent<Camera>();
        //InitializeMatrix();
    }

    void Update()
    {
        camera.projectionMatrix = matrix;
    }

    void InitializeMatrix()
    {
        matrix.SetRow(0, new Vector4(0.09f, 0, 0, 0));
        matrix.SetRow(1, new Vector4(0, 0.05f, -0.02f, 0));
        matrix.SetRow(2, new Vector4(0, 0, -0.02f, -1));
        matrix.SetRow(3, new Vector4(0, 0, 0, 1));
    }
}