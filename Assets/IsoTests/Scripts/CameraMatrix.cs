using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMatrix : MonoBehaviour
{
    Camera cam;
    Matrix4x4 camMatrix;
    int CameraPosition;
    
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        camMatrix = Matrix4x4.TRS(cam.transform.position, Quaternion.Euler(cam.transform.eulerAngles), cam.transform.localScale);
      
        CameraPosition = Shader.PropertyToID("_CamPos");
        Shader.SetGlobalVector(CameraPosition, cam.transform.position);

        Debug.Log(camMatrix);
        Debug.Log(cam.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
    }
}
