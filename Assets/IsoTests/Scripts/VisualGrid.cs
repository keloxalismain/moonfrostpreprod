using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class VisualGrid : MonoBehaviour
{
    // When added to an object, draws colored rays from the
    // transform position.
    //public int lineCount = 100;
    //public float radius = 3.0f;
    public int GridSize = 10;

    static Material lineMaterial;
    static void CreateLineMaterial()
    {
        if (!lineMaterial)
        {
            // Unity has a built-in shader that is useful for drawing
            // simple colored things.
            Shader shader = Shader.Find("Hidden/Internal-Colored");
            lineMaterial = new Material(shader);
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            // Turn on alpha blending
            lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            // Turn backface culling off
            lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            // Turn off depth writes
            lineMaterial.SetInt("_ZWrite", 0);
        }
    }

    // Will be called after all regular rendering is done
    public void OnRenderObject()
    {
        CreateLineMaterial();
        // Apply the line material
        lineMaterial.SetPass(0);

        GL.PushMatrix();
        // Set transformation matrix for drawing to
        // match our transform
        GL.MultMatrix(transform.localToWorldMatrix);

        // Draw lines
        GL.Begin(GL.LINES);
        GL.Color(new Color(1, 1, 1, 0.25f));
        for (int i = -GridSize; i < GridSize + 1; ++i)
        {
            GL.Vertex3(i, 0, -GridSize);
            GL.Vertex3(i, 0, GridSize);
        }
        for (int i = -GridSize; i < GridSize + 1; ++i)
        {
            GL.Vertex3(-GridSize, 0, i);
            GL.Vertex3(GridSize, 0, i);
        }
        GL.End();
        GL.PopMatrix();
    }
}
