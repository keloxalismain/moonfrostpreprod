using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    #region Singleton
    public static PlayerManager instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    public GameObject Player;
    [SerializeField] private GameObject _nameTagParent;
    [SerializeField] private TMPro.TMP_Text _nameTagText;
    private string _playerName;
    private Vector3 _offset = new Vector3(0f, 1f, 0f);
    private Transform _playerVisual;

    // Start is called before the first frame update
    void Start()
    {
        _nameTagParent.SetActive(true);
        _playerName = PlayerProfileManager.instance.PlayerProfileData.FirstName + " " + PlayerProfileManager.instance.PlayerProfileData.Surname;
        _nameTagText.text = _playerName;
        _playerVisual = PlayerSparkController.instance.VisualsTransform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 m_namePos = Camera.main.WorldToScreenPoint(_playerVisual.position + _offset);
        _nameTagParent.transform.position = m_namePos * UI_SceneManager.instance.UpscaleRatio;
    }
}
