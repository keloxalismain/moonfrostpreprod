using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WearablesManager : MonoBehaviour
{
    #region Singleton
    public static WearablesManager instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    public WearableDefinition[] CurrentWearables;
    private SkinnedMeshRenderer[] _currentMeshes;

    public WearableDefinition[] StarterWearables;

    private PlayerInventoryManager _playerInventoryManager;

    [SerializeField] private Transform _baseMeshParent;

    private SkinnedMeshRenderer _baseMesh;
    private MeshRenderer _eyesMesh;
    private PlayerProfileManager _playerProfileManager;
    private PlayerProfileData _playerProfileData;
    private DefinitionsMaster _definitionsMaster;

    public delegate void OnWearablesChanged(WearableDefinition newWearable, WearableDefinition alreadyEquippedWearable);
    public OnWearablesChanged onWearablesChanged;

    private void Start()
    {
        _playerInventoryManager = PlayerInventoryManager.instance;
        _playerProfileManager = PlayerProfileManager.instance;

        int m_totalWearableSlots = System.Enum.GetNames(typeof(WearableSlots)).Length;
        CurrentWearables = new WearableDefinition[m_totalWearableSlots];
        _currentMeshes = new SkinnedMeshRenderer[m_totalWearableSlots];

        _playerProfileData = PlayerProfileManager.instance.PlayerProfileData;
        _definitionsMaster = PlayerProfileManager.instance.DefinitionsMaster;

        var m_baseMesh = GameObject.Instantiate(_definitionsMaster.ChibiBaseMeshes[0]);
        PlayerSparkController.instance.VisualsTransform = m_baseMesh.transform;
        _baseMesh = m_baseMesh.GetComponentInChildren<SkinnedMeshRenderer>();
        m_baseMesh.transform.parent = _baseMeshParent;
        m_baseMesh.transform.localPosition = Vector3.zero;
        m_baseMesh.transform.localRotation = Quaternion.identity;
        m_baseMesh.transform.localScale = Vector3.one;

        _baseMesh.SetBlendShapeWeight(_playerProfileData.BodyBlend.ID, _playerProfileData.BodyBlendIntensity);

        _baseMesh.material.SetColor("_ProfileColor", _playerProfileData.SkinColor.PrimaryColor);

        var m_eyesMesh = GameObject.Instantiate(_definitionsMaster.ChibiEyeMeshes[_playerProfileData.EyeTypeID]);
        _eyesMesh = m_eyesMesh.GetComponentInChildren<MeshRenderer>();

        m_eyesMesh.transform.parent = m_baseMesh.transform.Find(n: "joint_Head");
        m_eyesMesh.transform.localPosition = Vector3.zero;
        m_eyesMesh.transform.localRotation = Quaternion.identity;
        m_eyesMesh.transform.localScale = Vector3.one;

        _eyesMesh.material.SetColor("_BaseColor", _playerProfileData.EyeColor.PrimaryColor);

        EquipStarterWearables();
    }

    public void EquipWearable(WearableDefinition m_newWearable)
    {
        int m_wearableSlotIndex = (int)m_newWearable.WearableSlot;

        foreach (var m_maskRegion in m_newWearable.MaskRegions)
        {
            if((int)m_maskRegion == m_wearableSlotIndex)
            {
                UI_PlayerEquipMenu.instance.uI_PlayerEquipSlots[(int)m_maskRegion].AddEquipToSlot(m_newWearable, Color.white);
            }
            else UI_PlayerEquipMenu.instance.uI_PlayerEquipSlots[(int)m_maskRegion].AddEquipToSlot(m_newWearable, Color.grey);
        }

        CurrentWearables[m_wearableSlotIndex] = m_newWearable;
        SkinnedMeshRenderer m_newMesh = Instantiate<SkinnedMeshRenderer>(m_newWearable.chibiAsset);
        m_newMesh.material.SetColor("_ProfileColor", _playerProfileData.HairColor.PrimaryColor);
        m_newMesh.material.SetColor("_BaseColor", m_newWearable.defaultColorConfig.PrimaryColor);
        m_newMesh.material.SetColor("_SecondaryColor", m_newWearable.defaultColorConfig.SecondaryColor);

        m_newMesh.transform.parent = _baseMesh.transform;
        m_newMesh.transform.localPosition = Vector3.zero;
        m_newMesh.transform.localRotation = Quaternion.identity;
        m_newMesh.transform.localScale = Vector3.one;

        m_newMesh.bones = _baseMesh.bones;
        m_newMesh.rootBone = _baseMesh.rootBone;

        if (m_newMesh.sharedMesh.blendShapeCount >= 1)
        {
            m_newMesh.SetBlendShapeWeight(_playerProfileData.BodyBlend.ID, _playerProfileData.BodyBlendIntensity);
        }

        _currentMeshes[m_wearableSlotIndex] = m_newMesh;

        if (onWearablesChanged != null)
        {
            onWearablesChanged.Invoke(m_newWearable, null);
        }

        //CheckStarterWearables();
    }

    public WearableDefinition UnequipWearable(int m_wearableSlotIndex)
    {
        if (CurrentWearables[m_wearableSlotIndex] != null)
        {
            if (_currentMeshes[m_wearableSlotIndex] != null)
            {
                Destroy(_currentMeshes[m_wearableSlotIndex].gameObject);
            }

            WearableDefinition m_alreadyEquippedWearable = CurrentWearables[m_wearableSlotIndex];
            _playerInventoryManager.AddToPlayerInventory(m_alreadyEquippedWearable);

            CurrentWearables[m_wearableSlotIndex] = null;

            if (onWearablesChanged != null)
            {
                onWearablesChanged.Invoke(null, m_alreadyEquippedWearable);
            }

            foreach (var m_maskRegion in m_alreadyEquippedWearable.MaskRegions)
            {
                UI_PlayerEquipMenu.instance.uI_PlayerEquipSlots[(int)m_maskRegion].ClearEquipSlot();
            }

            StartCoroutine(CheckStarterWearables());
            return m_alreadyEquippedWearable;
        }
        
        return null;
    }


    private IEnumerator CheckStarterWearables()
    {
        yield return new WaitForEndOfFrame();

        foreach (var m_starterWearable in StarterWearables)
        {
            if (m_starterWearable != null)
            {
                if (CurrentWearables[(int)m_starterWearable.WearableSlot] == null)
                    EquipWearable(m_starterWearable);
            }
        }
    }

    private void EquipStarterWearables()
    {
        if (_playerProfileManager != null)
        {
            StarterWearables = _playerProfileManager.PlayerProfileData.StarterWearables;
        }

        foreach (WearableDefinition m_starterWearable in StarterWearables) //back up til player profile version
        {
            EquipWearable(m_starterWearable);
        }
    }
}
