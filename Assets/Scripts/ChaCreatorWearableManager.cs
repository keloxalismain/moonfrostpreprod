using UnityEngine;

/// <summary>
/// Similar to wearables manager but without inventory interaction and designed to update avatar,
/// as well as Chibi, may combine at the end, if needed, or use this as basis for wardrobe functionality
/// </summary>
/// 

public class ChaCreatorWearableManager : MonoBehaviour
{
    #region Singleton
    public static ChaCreatorWearableManager instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    [HideInInspector] public Animator AvatarCharCtrl;
    [HideInInspector] public Animator ChibiCharCtrl;

    [HideInInspector] public SkinnedMeshRenderer AvatarCharHairMesh;
    [HideInInspector] public SkinnedMeshRenderer ChibiCharHairMesh;

    [HideInInspector] public SkinnedMeshRenderer AvaBaseMesh;
    [HideInInspector] public SkinnedMeshRenderer ChibiBaseMesh;

    [HideInInspector] public MeshRenderer AvaEyesMesh;
    [HideInInspector] public MeshRenderer ChibiEyesMesh;

    [SerializeField] private Transform _avatarCharCreator;
    [SerializeField] private Transform _chibiCharCreator;

    private PlayerProfileManager _playerProfileManager;
    private PlayerProfileData _playerProfileData;
    private DefinitionsMaster _definitionsMaster;

    private WearableDefinition[] _currentWearables;
    private SkinnedMeshRenderer[] _currentMeshes;
    //private WearableDefinition[] PlayerProfileStarterWearables;

    public delegate void OnWearablesChanged(WearableDefinition newWearable, WearableDefinition alreadyEquippedWearable);
    public OnWearablesChanged onWearablesChanged;
    public Vector3 PrevLookTarget;

    // Start is called before the first frame update
    void Start()
    {
        _playerProfileManager = PlayerProfileManager.instance;

        int m_totalWearableSlots = System.Enum.GetNames(typeof(WearableSlots)).Length;
        _currentWearables = new WearableDefinition[m_totalWearableSlots];
        _currentMeshes = new SkinnedMeshRenderer[m_totalWearableSlots];

        _playerProfileData = PlayerProfileManager.instance.PlayerProfileData;
        _definitionsMaster = PlayerProfileManager.instance.DefinitionsMaster;

        InitCharacter();
    }

    public void EquipWearable(WearableDefinition m_newWearable)
    {
        int m_wearableSlotIndex = (int)m_newWearable.WearableSlot;
        WearableDefinition m_alreadyEquippedWearable = UnequipWearable(m_wearableSlotIndex);

        if (onWearablesChanged != null)
        {
            onWearablesChanged.Invoke(m_newWearable, m_alreadyEquippedWearable);
        }

        _currentWearables[m_wearableSlotIndex] = m_newWearable;
        if (m_newWearable.avatarAsset != null)
        {
            SkinnedMeshRenderer m_newAvatarMesh = Instantiate<SkinnedMeshRenderer>(m_newWearable.avatarAsset);
            m_newAvatarMesh.material.SetColor("_ProfileColor", _playerProfileData.HairColor.PrimaryColor);
            m_newAvatarMesh.material.SetColor("_BaseColor", m_newWearable.defaultColorConfig.PrimaryColor);
            m_newAvatarMesh.material.SetColor("_SecondaryColor", m_newWearable.defaultColorConfig.SecondaryColor);

            if (m_newWearable.WearableSlot == WearableSlots.HairHat) AvatarCharHairMesh = m_newAvatarMesh;

            m_newAvatarMesh.transform.parent = AvaBaseMesh.transform;
            m_newAvatarMesh.transform.localPosition = Vector3.zero;
            m_newAvatarMesh.transform.localRotation = Quaternion.identity;
            m_newAvatarMesh.transform.localScale = Vector3.one;

            m_newAvatarMesh.bones = AvaBaseMesh.bones;
            m_newAvatarMesh.rootBone = AvaBaseMesh.rootBone;
        }

        SkinnedMeshRenderer m_newChibiMesh = Instantiate<SkinnedMeshRenderer>(m_newWearable.chibiAsset);
        m_newChibiMesh.material.SetColor("_ProfileColor", _playerProfileData.HairColor.PrimaryColor);
        m_newChibiMesh.material.SetColor("_BaseColor", m_newWearable.defaultColorConfig.PrimaryColor);
        m_newChibiMesh.material.SetColor("_SecondaryColor", m_newWearable.defaultColorConfig.SecondaryColor);

        if (m_newWearable.WearableSlot == WearableSlots.HairHat) ChibiCharHairMesh = m_newChibiMesh;

        m_newChibiMesh.transform.parent = ChibiBaseMesh.transform;
        m_newChibiMesh.transform.localPosition = Vector3.zero;
        m_newChibiMesh.transform.localRotation = Quaternion.identity;
        m_newChibiMesh.transform.localScale = Vector3.one;

        m_newChibiMesh.bones = ChibiBaseMesh.bones;
        m_newChibiMesh.rootBone = ChibiBaseMesh.rootBone;

        if (m_newChibiMesh.sharedMesh.blendShapeCount >= 1)
        {
            m_newChibiMesh.SetBlendShapeWeight(_playerProfileData.BodyBlend.ID, _playerProfileData.BodyBlendIntensity);
        }
       
        _currentMeshes[m_wearableSlotIndex] = m_newChibiMesh;
    }

    public WearableDefinition UnequipWearable(int m_wearableSlotIndex)
    {
        if (_currentWearables[m_wearableSlotIndex] != null)
        {
            if (_currentMeshes[m_wearableSlotIndex] != null)
            {
                Destroy(_currentMeshes[m_wearableSlotIndex].gameObject);
            }

            WearableDefinition m_alreadyEquippedWearable = _currentWearables[m_wearableSlotIndex];
            _currentWearables[m_wearableSlotIndex] = null;

            if (onWearablesChanged != null)
            {
                onWearablesChanged.Invoke(null, m_alreadyEquippedWearable);
            }
            return m_alreadyEquippedWearable;
        }
        return null;
    }

    public void InitCharacter()
    {
        if (_avatarCharCreator.childCount >= 1)
            DestroyImmediate(_avatarCharCreator.GetChild(0).gameObject, true);

        if (_chibiCharCreator.childCount >= 1)
            DestroyImmediate(_chibiCharCreator.GetChild(0).gameObject, true);

        var m_avatarBaseMesh = GameObject.Instantiate(_definitionsMaster.AvatarBaseMeshes[(int)_playerProfileData.Gender]);
        var m_chibiBaseMesh = GameObject.Instantiate(_definitionsMaster.ChibiBaseMeshes[(int)_playerProfileData.Gender]);
        GameObject.FindGameObjectWithTag("AimTarget").transform.position = PrevLookTarget;

        AvaBaseMesh = m_avatarBaseMesh.GetComponentInChildren<SkinnedMeshRenderer>();
        ChibiBaseMesh = m_chibiBaseMesh.GetComponentInChildren<SkinnedMeshRenderer>();

        m_avatarBaseMesh.transform.parent = _avatarCharCreator;
        m_avatarBaseMesh.transform.localPosition = Vector3.zero;
        m_avatarBaseMesh.transform.localRotation = Quaternion.identity;
        m_avatarBaseMesh.transform.localScale = Vector3.one;
        //GameObject.FindGameObjectWithTag("AimTarget").transform.position = PrevLookTarget;

        m_chibiBaseMesh.transform.parent = _chibiCharCreator;
        m_chibiBaseMesh.transform.localPosition = Vector3.zero;
        m_chibiBaseMesh.transform.localRotation = Quaternion.identity;
        m_chibiBaseMesh.transform.localScale = Vector3.one;

        AvaBaseMesh.material.SetColor("_ProfileColor", _playerProfileData.SkinColor.PrimaryColor);
        AvaBaseMesh.material.SetColor("_BaseColor", _playerProfileData.EyeColor.PrimaryColor);
        AvaBaseMesh.material.SetColor("_SecondaryColor", _playerProfileData.EyeColor.SecondaryColor);

        ChibiBaseMesh.SetBlendShapeWeight(_playerProfileData.BodyBlend.ID, _playerProfileData.BodyBlendIntensity);
        ChibiBaseMesh.material.SetColor("_ProfileColor", _playerProfileData.SkinColor.PrimaryColor);

        AvatarCharCtrl = m_chibiBaseMesh.GetComponentInChildren<Animator>(); //change to avatar
        ChibiCharCtrl = m_chibiBaseMesh.GetComponentInChildren<Animator>();

        EquipCreatorWearables();
    }

    public void EquipCreatorWearables()
    {
        foreach (WearableDefinition m_starterWearable in _playerProfileManager.PlayerProfileData.StarterWearables)
        {
            EquipWearable(m_starterWearable);
        }

        var m_eyesMesh = GameObject.Instantiate(_definitionsMaster.ChibiEyeMeshes[_playerProfileData.EyeTypeID]);
        ChibiEyesMesh = m_eyesMesh.GetComponentInChildren<MeshRenderer>();

        var m_eyeJoint = _chibiCharCreator.transform.GetChild(0).Find(n: "joint_Head");

        if (m_eyeJoint.childCount >= 1)
            DestroyImmediate(m_eyeJoint.GetChild(0).gameObject, true);

        m_eyesMesh.transform.parent = m_eyeJoint;
        m_eyesMesh.transform.localPosition = Vector3.zero;
        m_eyesMesh.transform.localRotation = Quaternion.identity;
        m_eyesMesh.transform.localScale = Vector3.one;

        ChibiEyesMesh.material.SetColor("_BaseColor", _playerProfileData.EyeColor.PrimaryColor);
    }
}
