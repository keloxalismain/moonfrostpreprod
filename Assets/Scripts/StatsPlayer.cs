using UnityEngine;

public class StatsPlayer : StatsCharacter
{
    private Animator _animator;
    private CharacterCombat _combat;

    private void Awake()
    {
        WearablesManager.instance.onWearablesChanged += OnWearablesChanged;
        _animator = GetComponentInChildren<Animator>();
        _combat = GetComponent<CharacterCombat>();
    }

    private void OnWearablesChanged(WearableDefinition m_newItem, WearableDefinition m_oldItem)
    {
        if (m_newItem != null)
        {
            Health.AddModifier(m_newItem.health);
            Attack.AddModifier(m_newItem.attack);
            Defence.AddModifier(m_newItem.defence);

            Range.AddModifier(m_newItem.range);
            Speed.AddModifier(m_newItem.speed);
            Luck.AddModifier(m_newItem.luck);
            Charm.AddModifier(m_newItem.charm);
        }

        if (m_oldItem != null)
        {
            Health.RemoveModifier(m_oldItem.health);
            Attack.RemoveModifier(m_oldItem.attack);
            Defence.RemoveModifier(m_oldItem.defence);

            Range.RemoveModifier(m_oldItem.range);
            Speed.RemoveModifier(m_oldItem.speed);
            Luck.RemoveModifier(m_oldItem.luck);
            Charm.RemoveModifier(m_oldItem.charm);
        }

        UI_PlayerEquipMenu.instance.EquipMenuStats.text = "Health " + Health.GetValue() + " Attack " + Attack.GetValue() + " Defence " + Defence.GetValue() + " Range " + Range.GetValue() + " Speed " + Speed.GetValue() + " Luck " + Luck.GetValue() + " Charm " + Charm.GetValue();
    }

    public override void Die()
    {
        base.Die();
        _animator.SetTrigger("m_death");
    }

    public override void WasHit()
    {
        base.WasHit();
      if (!_combat.IsAttacking) _animator.SetTrigger("m_damage");
        CameraShake.instance.ShakeBurst(1f, .75f);
    }
}
