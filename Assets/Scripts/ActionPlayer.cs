using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionPlayer : StateMachineBehaviour
{  
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        PlayerPropObjects.instance.ActivatePropObject(animator.GetInteger("m_actionId"));
        animator.SetInteger("m_actionId", 0);
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        PlayerPropObjects.instance.DeactivateAllPropObjects();
        PlayerSparkController.instance.Focus.InteractAnimComplete();
    }
}
