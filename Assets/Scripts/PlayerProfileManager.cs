using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerProfileManager : MonoBehaviour
{
    #region Singleton
    public static PlayerProfileManager instance;
    public DefinitionsMaster DefinitionsMaster;
    public PlayerProfileData PlayerProfileData;

    private void Awake()
    {
        instance = this;
        SceneManager.LoadSceneAsync("CharacterCreator", LoadSceneMode.Additive);
    }
    #endregion

}
