using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CHA_BodyBlend_00_SO", menuName = "Preproduction/Customisation Configs/Body Config")]
public class BodyBlendDefinition : ScriptableObject
{
    public string BlendName = "New Name";
    public int ID;
    [Space]
    public Sprite BlendIcon = null;
}
