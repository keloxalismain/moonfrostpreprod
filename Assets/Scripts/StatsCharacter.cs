using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsCharacter : MonoBehaviour
{
    public Stats Health;
    public int CurrentHealth { get; private set; }

    public Stats Attack;
    public Stats Defence;

    public Stats Range;
    public Stats Speed;
    public Stats Luck;
    public Stats Charm;

    public event System.Action<int, int> OnHealthChanged;

    private void Awake()
    {
        CurrentHealth = Health.GetValue();
    }

    public void TakeDamage(int m_damage)
    {
        m_damage -= Defence.GetValue();
        m_damage = Mathf.Clamp(m_damage, 0, int.MaxValue);

        CurrentHealth -= m_damage;

        if(OnHealthChanged != null) OnHealthChanged(Health.GetValue(), CurrentHealth);

        if (CurrentHealth <= 0)
        {
            Die();
        }
        else
        {
            WasHit();
        }
    }

    public virtual void WasHit()
    {

    }

    public virtual void Die()
    {

    }

}
