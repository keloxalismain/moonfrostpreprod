using EasyBuildSystem.Features.Scripts.Core.Base.Builder;
using EasyBuildSystem.Features.Scripts.Core.Base.Manager;
using EasyBuildSystem.Features.Scripts.Core.Base.Piece;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UI_BLD_Menu_OutdoorFurn : MonoBehaviour
{
    public Button ButtonTemplate;
    public Transform Container;
    public List<PieceBehaviour> OutdoorFurn_Pieces;

    private void Start()
    {
       

        for (int i = 0; i < OutdoorFurn_Pieces.Count; i++)
        {
            if (OutdoorFurn_Pieces[i] == null) continue;

            GameObject Button = (GameObject)Instantiate(ButtonTemplate.gameObject, Container);
            Button.SetActive(true);

           

            int Index = i;
            Button.GetComponent<Button>().onClick.AddListener(() =>
            {
                BuildManager.Instance.Pieces = OutdoorFurn_Pieces;
                BuilderBehaviour.Instance.ChangeMode(EasyBuildSystem.Features.Scripts.Core.Base.Builder.Enums.BuildMode.None);
                BuilderBehaviour.Instance.SelectPrefab(BuildManager.Instance.Pieces[Index]);
                BuilderBehaviour.Instance.ChangeMode(EasyBuildSystem.Features.Scripts.Core.Base.Builder.Enums.BuildMode.Placement);
            });

            Button.transform.GetChild(0).GetComponent<Image>().sprite = OutdoorFurn_Pieces[i].Icon;
            Button.transform.GetChild(0).GetComponent<Image>().preserveAspect = true;

            Button.transform.GetChild(1).GetComponent<Text>().text = OutdoorFurn_Pieces[i].Name;
        }
    }
}
