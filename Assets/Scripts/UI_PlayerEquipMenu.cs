using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_PlayerEquipMenu : MonoBehaviour
{
    #region Singleton
    public static UI_PlayerEquipMenu instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public UI_PlayerEquipSlot[] uI_PlayerEquipSlots;
    public TMPro.TMP_Text EquipMenuStats;
}
