using UnityEngine;

[CreateAssetMenu(fileName = "CHA_DuoColor_00_SO", menuName = "Preproduction/Color Configs/Color Duo Config")]
public class ColorDuoDefinition : ScriptableObject
{
    public string ColorName = "New Name";

    [ColorUsage(false, true)]
    public Color PrimaryColor = Color.magenta;

    [Space]
    public Color SecondaryColor = Color.cyan;

    [Space]
    public Sprite DisplayIcon = null;
}
