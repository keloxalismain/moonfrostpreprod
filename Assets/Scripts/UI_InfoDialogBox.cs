using UnityEngine;
using UnityEngine.UI;

public class UI_InfoDialogBox : MonoBehaviour
{
    public GameObject InfoDialogBox;
    public TMPro.TMP_Text ItemName;
    public TMPro.TMP_Text ItemDescription;
    public Image ItemIcon;
    private GameObject _tempAvatarItem;

    public void OnCloseDialogBoxButton()
    {
        Destroy(_tempAvatarItem);
        InfoDialogBox.SetActive(false);
    }

    public void PopulateInfoDialog(ItemDefinition m_item)
    {
        ItemName.text = m_item.itemName;
        ItemDescription.text = m_item.itemDescription;
        ItemIcon.sprite = m_item.itemIcon;
        _tempAvatarItem = Instantiate(m_item.inWorldAsset, RenderTextureManager.instance.SpawnPointForRenderTexture);
        InfoDialogBox.SetActive(true);
    }
}
