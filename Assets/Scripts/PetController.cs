using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PetController : MonoBehaviour
{
    #region Singleton

    public static PetController instance;
    private void Awake()
    {
        instance = this;
    }

    #endregion

    public float LookRadius = 10f;
    public bool IsPerformingAction = false;

    private PlayerManager _playerManager;

    [SerializeField] private float _wanderRadius = 3f;
    [SerializeField] private Transform _petRestLocation;
    [SerializeField] private float _restWaitTime = 10f;

    private float _jumpDuration = 1f;
    private float _agentSpeed;

    private Transform _player;
    public NavMeshAgent _agent;
    private Animator _animator;
    private Vector3 _target;

    public bool IsDistracted = false;
    private bool _isResting = false;
    public bool ShouldRest = false;

    private float _patrolTimeRemaining = 5f;
    public float ExcitableWaitTimeRemaining = 0f;

    private bool _moveAcrossNavMeshesStarted = false;

    private bool m_isAirborne = false;
    private bool m_isBiDirectional = false;

    // Start is called before the first frame update
    private void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        _player = PlayerManager.instance.Player.transform;
        _animator = GetComponent<Animator>();
        _agentSpeed = _agent.speed;
        _playerManager = PlayerManager.instance;
    }

    private void LateUpdate()
    {
        if (_agent.isOnOffMeshLink && !_moveAcrossNavMeshesStarted)
        {
            StartCoroutine(MoveAcrossNavMeshLink());
            _moveAcrossNavMeshesStarted = true;
        }

        if (_agent.isOnOffMeshLink && m_isAirborne == false)
        {
            m_isBiDirectional = _agent.currentOffMeshLinkData.offMeshLink.biDirectional;
            m_isAirborne = true;

            _animator.SetBool("m_isBiDirectional", m_isBiDirectional);
            _animator.SetBool("m_isAirborne", m_isAirborne);
        }
        else if (_agent.isOnNavMesh && m_isAirborne == true)
        {
            m_isAirborne = false;
            m_isBiDirectional = false;
            _animator.SetBool("m_isAirborne", m_isAirborne);
            _animator.SetBool("m_isBiDirectional", m_isBiDirectional);
        }

        _target = _player.position;

        if (_agent.destination != null) _target = _agent.destination;

        float m_distance = Vector3.Distance(_target, transform.position);
        float m_agentSpeed;
        _patrolTimeRemaining -= Time.deltaTime;
        ExcitableWaitTimeRemaining -= Time.deltaTime;

        _agent.speed = _agentSpeed;
        m_agentSpeed = _agent.velocity.magnitude / _agent.speed;

        if (!IsPerformingAction)
        {
            
            if (ShouldRest)
            {
                if (_isResting)
                {
                    _agent.SetDestination(_petRestLocation.position);
                    _restWaitTime = 10f;
                }

                else
                {
                    _restWaitTime -= Time.deltaTime;
                    if (_restWaitTime <= 0f)
                        _isResting = true;
                }
            }

            else if (m_distance <= LookRadius)
            {
                _isResting = false;

                if (!IsDistracted) _agent.SetDestination(_playerManager.Player.transform.position);



                if (m_distance <= _agent.stoppingDistance)
                {
                    if (!IsDistracted)
                    {
                        if (ExcitableWaitTimeRemaining <= 0f)
                        {
                            //bark near player?
                            _animator.SetTrigger("Exciting");
                            Debug.Log("bark bark");
                        }
                        //else _agent.SetDestination(_playerManager.Player.transform.position);
                    }

                    else
                    {

                        if (ExcitableWaitTimeRemaining <= 0f)
                        {
                            _animator.SetTrigger("Celebration");
                            Debug.Log("bark at thing");
                        }
                    }

                    FaceTarget();
                }
            }
            else
            {
                if (IsDistracted) _agent.SetDestination(_target);
                else if (_patrolTimeRemaining <= 0f)
                {
                    StartCoroutine(Patrolling());
                }
            }
        }
        else
        {
            _agent.speed = 0;
            m_agentSpeed = 0f;
        }
        _animator.SetFloat("m_agentSpeed", m_agentSpeed, .1f, Time.deltaTime);
    }

    IEnumerator MoveAcrossNavMeshLink()
    {
        OffMeshLinkData data = _agent.currentOffMeshLinkData;
        _agent.updateRotation = false;

        Vector3 startPos = _agent.transform.position;
        Vector3 endPos = data.endPos + Vector3.up * _agent.baseOffset;

        Vector3 m_lookAt;
        if (Vector3.Distance(transform.position, data.startPos) > Vector3.Distance(transform.position, data.endPos)) m_lookAt = data.startPos;
        else m_lookAt = data.endPos;

        FaceTarget();
        float t = 0.0f;
        float tStep = 1.0f / _jumpDuration;

        //_isAirborne = true;
        Vector3 m_destinationHolder = _agent.destination;
        while (t < 1.0f)
        {
            this.transform.position = Vector3.Lerp(startPos, endPos, t);
            _agent.destination = this.transform.position;
            t += tStep * Time.deltaTime;
            yield return null;
        }

        this.transform.position = endPos;
        _agent.updateRotation = true;

        _agent.CompleteOffMeshLink();
        _moveAcrossNavMeshesStarted = false;
        _agent.destination = m_destinationHolder;
    }

    IEnumerator Patrolling()
    {
        _agent.SetDestination(new Vector3(_agent.transform.position.x + Random.Range(-_wanderRadius, _wanderRadius), _agent.transform.position.y, _agent.transform.position.z + Random.Range(-_wanderRadius, _wanderRadius)));
        _patrolTimeRemaining = 5f;
        yield return new WaitForSeconds(5f);
    }

    private void FaceTarget()
    {
        Vector3 m_direction = (_target - this.transform.position).normalized;
        Quaternion m_lookRotation = Quaternion.LookRotation(new Vector3(m_direction.x, 0, m_direction.z));
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, m_lookRotation, Time.deltaTime * 2f);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, LookRadius);
    }

    public void CallPet()
    {
        IsDistracted = false;
        _agent.SetDestination(_playerManager.Player.transform.position);
    }
}
