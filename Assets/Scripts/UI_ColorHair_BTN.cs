using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ColorHair_BTN : UI_Color_BTN
{
    public override void ConfigButton()
    {
        base.ConfigButton();
        GetComponent<Image>().color = PlayerProfileManager.instance.DefinitionsMaster.HairColors[_colorListID].PrimaryColor;
        GetComponentInChildren<TMPro.TMP_Text>().text = PlayerProfileManager.instance.DefinitionsMaster.HairColors[_colorListID].ColorName;
    }

    public override void SetColor()
    {
        base.SetColor();
        PlayerProfileManager.instance.PlayerProfileData.HairColor = PlayerProfileManager.instance.DefinitionsMaster.HairColors[_colorListID];        
        ChaCreatorWearableManager.instance.InitCharacter();
    }
}
