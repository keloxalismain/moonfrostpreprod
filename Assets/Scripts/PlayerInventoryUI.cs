using UnityEngine;

public class PlayerInventoryUI : MonoBehaviour
{
    private PlayerInventoryManager _playerInventory;
    [SerializeField] private Transform _playerInventoryParent;
    private UI_PlayerInventorySlot[] _playerInventorySlots;

    // Start is called before the first frame update
    void Start()
    {
        _playerInventory = PlayerInventoryManager.instance;
        _playerInventory.onInventoryChangedCallback += UpdateUI;

        _playerInventorySlots = _playerInventoryParent.GetComponentsInChildren<UI_PlayerInventorySlot>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void UpdateUI()
    {
        for (int i = 0; i < _playerInventorySlots.Length; i++)
        {
            if (i < _playerInventory.items.Count)
            {
                _playerInventorySlots[i].AddItemToSlot(_playerInventory.items[i]);
            }
            else
            {
                _playerInventorySlots[i].ClearItemSlot();
            }

        }
    }
}
