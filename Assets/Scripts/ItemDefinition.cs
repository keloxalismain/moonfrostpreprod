using UnityEngine;

[CreateAssetMenu(fileName = "ITM_NewItem_SO", menuName = "Preproduction/Inventory/Item Config")]
public class ItemDefinition : ScriptableObject
{
    public string itemName = "New Name";

    [TextArea (3, 5)]
    public string itemDescription = "New lore - Max 5 lines";

    [Space]
    public Sprite itemIcon = null;
    public GameObject inWorldAsset = null;

    public Rarity rarity;

    [Space]
    public int purchaseCost = 10;
    public int resalePrice = 5;

    [Space]
    public int stackableQuantity = 99;

    [Space]
    public bool isStarterItem = false;
    public bool isSellableItem = true;


    public virtual void Use()
    {
        // Not sure what generic behavious would be yet... open pop up description?
    }

    public void RemoveFromInventory()
    {
        //requires stackable support
        PlayerInventoryManager.instance.RemoveFromPlayerInventory(this);
    }

    public enum Rarity { Common, Uncommon, Rare, Epic, Legendary }
}
