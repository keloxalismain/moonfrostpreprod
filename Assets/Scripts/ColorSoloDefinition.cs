using UnityEngine;

[CreateAssetMenu(fileName = "CHA_SoloColor_00_SO", menuName = "Preproduction/Color Configs/Color Solo Config")]
public class ColorSoloDefinition : ScriptableObject
{
    public string ColorName = "New Name";

    [ Space]
    [ColorUsage(false, true)]
    public Color PrimaryColor = Color.magenta;

    [Space]
    public Sprite DisplayIcon = null;
}
