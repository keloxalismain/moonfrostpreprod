using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableEnemy : Interactable
{
    private PlayerManager _playerManager;
    private StatsCharacter _statsCharacter;

    public override void Interact()
    {
        _playerManager = PlayerManager.instance;
        _statsCharacter = GetComponent<StatsCharacter>();
        base.Interact();
        CharacterCombat m_characterCombat = _playerManager.Player.GetComponent<CharacterCombat>();
        if(m_characterCombat != null && _statsCharacter.CurrentHealth > 0)
        {
            m_characterCombat.Attack(_statsCharacter);
        }
    }
}
