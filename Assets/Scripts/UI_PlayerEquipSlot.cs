using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_PlayerEquipSlot : MonoBehaviour
{
    private WearableDefinition _equipItem;
    public Image ItemIcon;
    public Button DiscardButton;
    //public Button infoButton;

    public void AddEquipToSlot(WearableDefinition m_newItem, Color m_color)
    {
        _equipItem = m_newItem;
        ItemIcon.sprite = _equipItem.itemIcon;
        ItemIcon.enabled = true;
        GetComponentInChildren<Image>().color = m_color;
        //ItemIcon.color = m_color;
        DiscardButton.interactable = true;
        //infoButton.interactable = true;
    }

    public void ClearEquipSlot()
    {
        _equipItem = null;
        ItemIcon.sprite = null;
        ItemIcon.enabled = false;
        DiscardButton.interactable = false;
        //infoButton.interactable = false;
        GetComponentInChildren<Image>().color = Color.white;
    }

    public void OnUnequipButton()
    {
        WearablesManager.instance.UnequipWearable((int)_equipItem.WearableSlot);
    }

    public void OnUseEquipItem()
    {
        if (_equipItem != null)
        {
            _equipItem.Use();
        }
    }

    public void OnInfoEquipButton()
    {
        UI_SceneManager.instance.Canvas.GetComponent<UI_InfoDialogBox>().PopulateInfoDialog(_equipItem);
    }
}
