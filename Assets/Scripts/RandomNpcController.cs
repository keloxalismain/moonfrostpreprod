using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class RandomNpcController : MonoBehaviour
{
    public Transform VisualsTransform;
    public float DirectionChangeInterval = 5;

    private NavMeshAgent _navMeshAgent;
    private Animator _animatorController;
    
    private int _blendWeight;

    private SkinnedMeshRenderer _baseMesh;
    [SerializeField] private DefinitionsMaster _definitionsMaster; // get from scene instead

    private Vector3 _lastPos;
    private Vector3 _newPos;
    public float NpcTiltAngle = 0f;
    private Vector3 _npcDirection = Vector3.forward;

    private void Start()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _animatorController = GetComponentInChildren<Animator>();
        _baseMesh = GetComponentInChildren<SkinnedMeshRenderer>();
        _baseMesh.SetBlendShapeWeight(0, Random.Range(0, 101));
        _blendWeight = (int)_baseMesh.GetBlendShapeWeight(0);
        _baseMesh.material.SetColor("_ProfileColor", _definitionsMaster.SkinColors[Random.Range(0, _definitionsMaster.SkinColors.Count)].PrimaryColor);

        EquipWearable(_definitionsMaster.HairHatWearables[Random.Range(0, _definitionsMaster.HairHatWearables.Count)]);
        EquipWearable(_definitionsMaster.TorsoWearables[Random.Range(0, _definitionsMaster.TorsoWearables.Count)]);
        EquipWearable(_definitionsMaster.LegsWearables[Random.Range(0, _definitionsMaster.LegsWearables.Count)]);

        _navMeshAgent.speed = Random.Range(.65f, 1f);

        StartCoroutine(NewHeading());
        InvokeRepeating("DirectionEvaluate", 0.05f, .2f);
    }

    private void EquipWearable(WearableDefinition m_newWearable)
    {
        SkinnedMeshRenderer m_newMesh = Instantiate<SkinnedMeshRenderer>(m_newWearable.chibiAsset);
        m_newMesh.material.SetColor("_ProfileColor", _definitionsMaster.HairColors[Random.Range(0, _definitionsMaster.HairColors.Count)].PrimaryColor);
        int m_duoCol = Random.Range(0, _definitionsMaster.ColorConfigs.Count);
        m_newMesh.material.SetColor("_BaseColor", _definitionsMaster.ColorConfigs[m_duoCol].PrimaryColor);
        m_newMesh.material.SetColor("_SecondaryColor", _definitionsMaster.ColorConfigs[m_duoCol].SecondaryColor);

        m_newMesh.transform.parent = _baseMesh.transform;
        m_newMesh.transform.localPosition = Vector3.zero;
        m_newMesh.transform.localRotation = Quaternion.identity;
        m_newMesh.transform.localScale = Vector3.one;

        m_newMesh.bones = _baseMesh.bones;
        m_newMesh.rootBone = _baseMesh.rootBone;
        if (m_newMesh.sharedMesh.blendShapeCount >= 1)
        {
            m_newMesh.SetBlendShapeWeight(0, _blendWeight);
        }
    }

    private void FixedUpdate()
    {
        _navMeshAgent.Move(_npcDirection * _navMeshAgent.speed / 100);
        _animatorController.SetFloat("m_agentSpeed", Random.Range(9f, 11f));
        _newPos = this.transform.position;
    }

    private void DirectionEvaluate()
    {
        Vector3 m_direction = (_newPos - _lastPos).normalized;

        if (_lastPos != _newPos)
        {
            Pseudo2dRotation(m_direction);
        }
        _lastPos = _newPos;
    }

    /// <summary>
    /// Repeatedly calculates a new direction to move towards.
    /// Use this instead of MonoBehaviour.InvokeRepeating so that the interval can be changed at runtime.
    /// </summary>
    private IEnumerator NewHeading()
    {
        while (true)
        {
            NewHeadingRoutine();
            yield return new WaitForSeconds(DirectionChangeInterval);
        }
    }

    /// <summary>
    /// Calculates a new direction to move towards.
    /// </summary>
    private void NewHeadingRoutine()
    {
        _npcDirection = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f));
    }

    private void Pseudo2dRotation(Vector3 m_direction)
    {
        if (m_direction.x > 0)
        {
            if (m_direction.z > 0.2f)
            {
                VisualsTransform.localRotation = Quaternion.Euler(NpcTiltAngle / 2, 45, NpcTiltAngle / 2);
            }
            else if (m_direction.z < -0.2f)
            {
                VisualsTransform.localRotation = Quaternion.Euler(-NpcTiltAngle / 2, 135, NpcTiltAngle / 2);
            }
            else
            {
                VisualsTransform.localRotation = Quaternion.Euler(0, 90, NpcTiltAngle);
            }
        }
        else if (m_direction.x < 0)
        {
            if (m_direction.z > 0.2f)
            {
                VisualsTransform.localRotation = Quaternion.Euler(NpcTiltAngle / 2, -45, -NpcTiltAngle / 2);
            }
            else if (m_direction.z < -0.2f)
            {
                VisualsTransform.localRotation = Quaternion.Euler(-NpcTiltAngle / 2, -135, -NpcTiltAngle / 2);
            }
            else
            {
                VisualsTransform.localRotation = Quaternion.Euler(0, -90, -NpcTiltAngle);
            }
        }
        else if (m_direction.z > 0)
        {
            VisualsTransform.localRotation = Quaternion.Euler(NpcTiltAngle, 0, 0);
        }
        else
        {
            VisualsTransform.localRotation = Quaternion.Euler(-NpcTiltAngle, 180, 0);
        }
    }
}
