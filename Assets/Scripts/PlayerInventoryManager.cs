using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventoryManager : MonoBehaviour
{
    #region Singleton
    public static PlayerInventoryManager instance;
    private void Awake()
    {
        if(instance != null)
        {
            Debug.LogWarning("Multiple instances found", this);
            return;
        }

        instance = this;
    }
    #endregion

    public delegate void OnInventoryChanged();
    public OnInventoryChanged onInventoryChangedCallback;

    public int inventorySpace = 10;
    public List<ItemDefinition> items = new List<ItemDefinition>();
    public bool AddToPlayerInventory(ItemDefinition m_item)
    {
        if (!m_item.isStarterItem)
        {
            if (items.Count >= inventorySpace)
            {
                Debug.Log("Not emough room");
                return false;
            }
            items.Add(m_item);
            if(onInventoryChangedCallback != null) onInventoryChangedCallback.Invoke();
        }
        return true;
    }

    public void RemoveFromPlayerInventory(ItemDefinition m_item)
    {
        items.Remove(m_item);
        if (onInventoryChangedCallback != null) onInventoryChangedCallback.Invoke();
    }
}
