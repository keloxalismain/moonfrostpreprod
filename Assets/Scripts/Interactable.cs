using System.Collections;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public float InteractableRadius = 1f;
    public Transform InteractionPoint;

    [Space]
    public string ButtonText = "Interact";
    public Transform InteractableUiPoint;

    [Space]
    [SerializeField, Tooltip("0 = unused, 1 = FarmHoe, 2 = KneelingGather, 3 = Hammer, 4 pickaxe")] private int _animationID = 0;


    private bool _isFocus = false;
    private bool _hasInteracted = false;
    private Transform _playerVisualsTransform;
    private Material _interactableMaterial;
    private bool _hasInteractionPoint = false;
    private UI_InteractionPopup _interactionPopup;

    public virtual void Interact()
    {
        if (_hasInteractionPoint) PlayerSparkController.instance.ShouldLockRotation = true;

        else PlayerSparkController.instance.CouldInteract = true;

        PlayerSparkController.instance.PlayerAnimator.SetInteger("m_actionId", _animationID);
    }

    public virtual void InteractAnimComplete()
    {
        
    }

    public virtual void ArriveAtInteraction()
    {
        if (_interactableMaterial != null) _interactableMaterial.SetInteger("_ShouldOutline", 1);

        _interactionPopup.OpenInteractionPopup(this);
    }

    public virtual void LeaveInteraction()
    {
        if (_interactableMaterial != null) _interactableMaterial.SetInteger("_ShouldOutline", 0);

        _interactionPopup.CloseInteractionPopup();
    }

    private void Start()
    {
        CheckInteractionPoint();
        if (TryGetComponent<Renderer>(out Renderer m_renderer))
            _interactableMaterial = m_renderer.material;

        if(_interactableMaterial == null)
        {
            _interactableMaterial = GetComponentInChildren<Renderer>().material;
        }

        _interactionPopup = UI_SceneManager.instance.Canvas.GetComponent<UI_InteractionPopup>();
    }

    private void CheckInteractionPoint()
    {
        if (InteractionPoint == null)
        {
            InteractionPoint = this.transform;
            _hasInteractionPoint = false;
        }
        else if (InteractionPoint == this.transform)
        {
            _hasInteractionPoint = false;
        }
        else _hasInteractionPoint = true;
    }

    private void LateUpdate()
    {
        if (_isFocus)
        {
            if (!_hasInteracted)
            {
                float m_distance = Vector3.Distance(_playerVisualsTransform.position, InteractionPoint.position);
                if (m_distance <= InteractableRadius)
                {
                    Interact();
                    _hasInteracted = true;
                }
            }

            //else
                //InteractAnimComplete();
        }
    }

    public void OnFocused(Transform m_playerTransform)
    {
        _isFocus = true;
        _playerVisualsTransform = m_playerTransform;
        _hasInteracted = false;
    }

    public void OnDefocused()
    {
        _isFocus = false;
        _playerVisualsTransform = null;
        _hasInteracted = false;

        PlayerSparkController.instance.ShouldLockRotation = false;
        PlayerSparkController.instance.CouldInteract = false;
    }

    private void OnDrawGizmosSelected()
    {
        CheckInteractionPoint();
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(InteractionPoint.position, InteractableRadius);
        Gizmos.DrawSphere(InteractionPoint.position, 0.2f);

        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(InteractableUiPoint.position, 0.2f);
    }
}
