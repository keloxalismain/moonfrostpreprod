using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_EnemyHud : MonoBehaviour
{
    [SerializeField] private GameObject _enemyHud;
    private Image _healthBar;
    private float _visibleDelay = 5f;
    private float _lastVisible;

    [SerializeField] private Transform _enemyHudTransform;

    void Start()
    {
        if (_enemyHudTransform != null) {
            _enemyHud.SetActive(false);

            _healthBar = _enemyHud.transform.GetChild(0).GetComponent<Image>();
        }
        GetComponent<StatsCharacter>().OnHealthChanged += OnHealthChanged;
    }

    private void OnHealthChanged(int m_maxHealth, int m_currentHealth)
    {
        if (_enemyHudTransform != null)
        {
            _enemyHud.gameObject.SetActive(true);
            _lastVisible = Time.time;

            float m_healthPercent = (float)m_currentHealth / m_maxHealth;
            _healthBar.fillAmount = m_healthPercent;
        }
    }

    void LateUpdate()
    {
        if (_enemyHudTransform != null)
        {
            if (Time.time - _lastVisible > _visibleDelay) _enemyHud.SetActive(false);
        }
        if (_enemyHud.activeSelf)
        {
            Vector3 m_namePos = Camera.main.WorldToScreenPoint(_enemyHudTransform.position * UI_SceneManager.instance.UpscaleRatio);
            _enemyHud.transform.position = m_namePos;
        }
    }
}
