using UnityEngine;

[CreateAssetMenu(fileName = "NPC_NpcProfile_SO", menuName = "Preproduction/Profile Data/Npc Profile")]
public class NpcProfileData : CharacterProfileData
{
	public enum eDisposition
	{
		OptionA, OptionB, OptionC, OptionD, OptionE
	}

	[Space]
	public eDisposition Disposition;

	public enum eRelationshipPath
	{
		OptionA, OptionB, OptionC, OptionD, OptionE
	}

	[Space]
	public eRelationshipPath RelationshipPath;

	public int RelationshipLevel = 1;
	public int RelationshipExperience = 0;
}
