using UnityEngine;

[CreateAssetMenu(fileName = "GEN_PlayerProfile_SO", menuName = "Preproduction/Profile Data/Player Profile")]
public class PlayerProfileData : CharacterProfileData
{
    public string FarmName = "Farmington";

    [Space]
    public int PlayerLevel = 1;
    public int TotalExperience = 0;
}
