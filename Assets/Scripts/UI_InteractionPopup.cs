using UnityEngine;

public class UI_InteractionPopup : MonoBehaviour
{
    public GameObject InteractionPopup;
    public TMPro.TMP_Text InteractionText;
    private Interactable _interactable;

    public void OnInteractionPopupButton()
    {
        PlayerSparkController.instance.SetFocus(_interactable);
    }

    public void OpenInteractionPopup(Interactable m_interactable)
    {
        InteractionText.text = m_interactable.ButtonText;
        InteractionPopup.SetActive(true);
        _interactable = m_interactable;
    }

    private void Update()
    {
        if (InteractionPopup.activeSelf == true)
        {
            if (_interactable != null)
            {
                Vector3 m_namePos = Camera.main.WorldToScreenPoint(_interactable.InteractableUiPoint.position);
                InteractionPopup.transform.position = m_namePos * UI_SceneManager.instance.UpscaleRatio;
            }
            else
            {
                InteractionPopup.SetActive(false);
            }
            if (Input.GetButtonDown("Fire1")) OnInteractionPopupButton();
        }
    }

    public void CloseInteractionPopup()
    {
        InteractionPopup.SetActive(false);
    }
}