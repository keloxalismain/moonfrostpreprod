using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_JumpPopup : MonoBehaviour
{
    public GameObject JumpPopup;
    public TMPro.TMP_Text JumpBtnText;
    private OffMeshLinkController _offMeshLinkController;
    public void OpenInteractionPopup(OffMeshLinkController m_linkController)
    {
        _offMeshLinkController = m_linkController;
        JumpBtnText.text = m_linkController.ButtonText;
        JumpPopup.SetActive(true);
    }

    public void CloseInteractionPopup()
    {
        JumpPopup.SetActive(false);
    }

    public void OnJumpButton()
    {
        _offMeshLinkController.OnJumpButton();
    }

    private void Update()
    {
        if (JumpPopup.activeSelf == true)
        {
            if (_offMeshLinkController != null)
            {
                Vector3 m_namePos = Camera.main.WorldToScreenPoint(_offMeshLinkController.InteractableUiPoint.position);
                JumpPopup.transform.position = m_namePos * UI_SceneManager.instance.UpscaleRatio;
            }
            else
            {
                JumpPopup.SetActive(false);
            }
            if (Input.GetButtonDown("Fire2")) OnJumpButton();
        }
    }
}
