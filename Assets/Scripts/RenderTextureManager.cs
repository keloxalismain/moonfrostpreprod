using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderTextureManager : MonoBehaviour
{
    #region Singleton
    public static RenderTextureManager instance;
    private void Awake()
    {
        instance = this;
    }
    #endregion

    public Transform SpawnPointForRenderTexture;
}
