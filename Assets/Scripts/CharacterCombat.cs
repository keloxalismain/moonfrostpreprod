using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(StatsCharacter))]
public class CharacterCombat : MonoBehaviour
{
    private StatsCharacter _stats;
    private StatsCharacter _targetStats;
    private float _attackCooldown = 0f;
    private Animator _animator;
    private float _staggerFade = 0f;

    public float AttackSpeed = 0.25f;
    public float AttackDelay = 0.6f;
    public float Stagger = 0.25f;
    public bool IsAttacking = false;
    public bool IsPlayer = false;
    
    public List<Renderer> _staggerRenderers;

    public event System.Action OnAttack;

    private void Start()
    {
        _stats = GetComponent<StatsCharacter>();
        _animator = GetComponentInChildren<Animator>();
        GetComponent<StatsCharacter>().OnHealthChanged += OnHealthChanged;

        if(_staggerRenderers.Count < 1)
        {
            foreach (SkinnedMeshRenderer m_SMR in GetComponentsInChildren<SkinnedMeshRenderer>())
            {
                _staggerRenderers.Add(m_SMR);
            }
        }
    }

    private void Update()
    {
        _attackCooldown -= Time.deltaTime;
        _staggerFade -= Time.deltaTime;

        if (_staggerFade > 0)
        {
            foreach (var m_staggerRenderer in _staggerRenderers)
            {
                m_staggerRenderer.material.SetFloat("_HitStagger", _staggerFade);
            }
        }
    }

    private void OnHealthChanged(int m_maxHealth, int m_currentHealth)
    {
        _attackCooldown += Stagger;
        _staggerFade = 1;
    }

    public void Attack(StatsCharacter m_targetStats)
    {
        if (_attackCooldown <= 0f && _stats.CurrentHealth > 0)
        {           
            _animator.SetTrigger("m_attack");
            _targetStats = m_targetStats;
           if (OnAttack != null) OnAttack();
        }
    }

    public void AttackAction()
    {
        StartCoroutine(DelayDamage(_targetStats, AttackDelay));
        _attackCooldown = 1f / AttackSpeed;
    }

    IEnumerator DelayDamage (StatsCharacter m_stats, float m_attackDelay)
    {
        yield return new WaitForSeconds(m_attackDelay);

        if (IsPlayer) CameraShake.instance.ShakeBurst(.5f, .5f);
        m_stats.TakeDamage(_stats.Attack.GetValue());
    }
}
