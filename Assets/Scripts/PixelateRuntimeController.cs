using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

public class PixelateRuntimeController : MonoBehaviour
{
	[SerializeField] private UniversalRendererData rendererData = null;
	[SerializeField] private string featureName = null;
	[SerializeField] private float transitionPeriod = 1;
	[SerializeField] private Slider _slider;

	private bool transitioning;
	private float startTime;

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			StartTransition();
		}
		if (transitioning)
		{
			if (Time.timeSinceLevelLoad >= startTime + transitionPeriod)
			{
				EndTransition();
			}
			else
			{
				UpdateTransition();
			}
		}
	}

	private void OnDestroy()
	{
		ResetTransition();
	}

	bool TryGetFeature(out ScriptableRendererFeature feature)
	{
		feature = rendererData.rendererFeatures.Where((f) => f.name == featureName).FirstOrDefault();
		return feature != null;
	}

	public void SetPixelSize(){
		if (TryGetFeature(out var feature))
		{
			var pixelateFeature = feature as PixellateUrp;
			var material = pixelateFeature.settings.material;
			pixelateFeature.settings.Dencity = _slider.value;
			material.SetFloat("_Dencity", _slider.value);
			rendererData.SetDirty();
		}

}

	void StartTransition()
	{
		startTime = Time.timeSinceLevelLoad;
		transitioning = true;
	}

	private void UpdateTransition()
	{
		if(TryGetFeature(out var feature))
		{
			float pixelate = Mathf.Clamp01((Time.timeSinceLevelLoad - startTime) / transitionPeriod);
			var pixelateFeature = feature as PixellateUrp;
			var material = pixelateFeature.settings.material;
			pixelateFeature.settings.Dencity = pixelate;
			material.SetFloat("_Dencity", pixelate);
			rendererData.SetDirty();

		}
	}

	private void EndTransition()
	{
		if (TryGetFeature(out var feature))
		{
			feature.SetActive(false);
			rendererData.SetDirty();

			transitioning = false;
		}
	}

	private void ResetTransition()
	{
		if (TryGetFeature(out var feature))
		{
			feature.SetActive(true);
			rendererData.SetDirty();

			var pixelateFeature = feature as PixellateUrp;
			var material = pixelateFeature.settings.material;
			material.SetFloat("_Dencity", 0);

			transitioning = false;
		}
	}



}
