using UnityEngine;

public class CharacterFootsteps : MonoBehaviour
{
    [SerializeField] private GameObject _dampFootprint;
    [SerializeField] private GameObject _wetFootprint;
    [SerializeField] private Transform _leftFootTransform;
    [SerializeField] private Transform _rightFootTransform;

    public void LeftStep()
    {
        LeaveFootPrint(_leftFootTransform);
    }

    public void RightStep()
    {
       LeaveFootPrint(_rightFootTransform);
    }

    private void LeaveFootPrint(Transform m_footprintLocation)
    {
        switch (TimeOfDayManager.instance.MoistureLevel)
        {
            case TimeOfDayManager.eMoistureLevels.dry:
                // play dry foot print sound
                break;
            case TimeOfDayManager.eMoistureLevels.moist:
                Instantiate(_dampFootprint, m_footprintLocation.position, Quaternion.identity);
                // play moist foot print sound
                break;
            case TimeOfDayManager.eMoistureLevels.wet:
                Instantiate(_wetFootprint, m_footprintLocation.position, Quaternion.identity);
                break;
            default:
                break;
        }
    }

}
