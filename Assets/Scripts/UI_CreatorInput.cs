using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_CreatorInput : MonoBehaviour
{
    #region Singleton
    public static UI_CreatorInput instance;
    

    private void Awake()
    {
        instance = this;
    }
    #endregion

    private Vector3 _aimTargetPos;
    private DefinitionsMaster _definitonsMaster;
    private PlayerProfileData _playerProfileData;

    private void Start()
    {
        _definitonsMaster = PlayerProfileManager.instance.DefinitionsMaster;
        _playerProfileData = PlayerProfileManager.instance.PlayerProfileData;
    }

    public void OnInputPronoun(int m_playerPronoun)
    {
        _playerProfileData.Pronoun = (CharacterProfileData.eProNoun)m_playerPronoun;
        _aimTargetPos = new Vector3(-0.2f, 0, 0);
        ChaCreatorWearableManager.instance.PrevLookTarget = _aimTargetPos;
        LeanTween.move(GameObject.FindGameObjectWithTag("AimTarget"), _aimTargetPos, 1f);

    }

    public void OnInputFirstName(string m_playerName)
    {
        _playerProfileData.FirstName = m_playerName;

        _aimTargetPos = new Vector3(-0.1f, 0, 0);
        ChaCreatorWearableManager.instance.PrevLookTarget = _aimTargetPos;
        LeanTween.move(GameObject.FindGameObjectWithTag("AimTarget"), _aimTargetPos, 1f);

    }

    public void OnInputSurname(string m_playerSurname)
    {
        _playerProfileData.Surname = m_playerSurname;
        _aimTargetPos = new Vector3(-0.05f, 0, 0);
        ChaCreatorWearableManager.instance.PrevLookTarget = _aimTargetPos;
        LeanTween.move(GameObject.FindGameObjectWithTag("AimTarget"), _aimTargetPos, 1f);
    }

    public void OnInputFarmName(string m_farmName)
    {
        _playerProfileData.FarmName = m_farmName + " Farm";
        _aimTargetPos = new Vector3(-0.025f, 0, 0);
        ChaCreatorWearableManager.instance.PrevLookTarget = _aimTargetPos;
        LeanTween.move(GameObject.FindGameObjectWithTag("AimTarget"), _aimTargetPos, 1f);
        //ChaCreatorWearableManager.instance.PrevLookTarget = _aimTargetPos;
    }

    public void OnInputBodyType(int m_bodyType)
    {
        _playerProfileData.BodyBlend = _definitonsMaster.BodyBlendDefinitions[m_bodyType];
        if (m_bodyType == 0 || m_bodyType == 4) // if Curvy male or curvy female
            _playerProfileData.BodyBlendIntensity = 100;
        else _playerProfileData.BodyBlendIntensity = 0;

        _playerProfileData.Gender = (CharacterProfileData.eGender)m_bodyType;

        ChaCreatorWearableManager.instance.PrevLookTarget = _aimTargetPos;
        ChaCreatorWearableManager.instance.InitCharacter();

        _aimTargetPos = new Vector3(0.1f, 0, 0);
        LeanTween.move(GameObject.FindGameObjectWithTag("AimTarget"), _aimTargetPos, 1f);
        ChaCreatorWearableManager.instance.PrevLookTarget = _aimTargetPos;
    }

    public void OnInputEyeType(int m_eyeType)
    {
        _playerProfileData.EyeTypeID = m_eyeType;

        ChaCreatorWearableManager.instance.PrevLookTarget = _aimTargetPos;
        ChaCreatorWearableManager.instance.EquipCreatorWearables();

        _aimTargetPos = new Vector3(0.15f, 0, 0);
        LeanTween.move(GameObject.FindGameObjectWithTag("AimTarget"), _aimTargetPos, 1f);
        ChaCreatorWearableManager.instance.PrevLookTarget = _aimTargetPos;
    }

    public void OnInputHairType(int m_hairType)
    {
        for (int i = 0; i < _playerProfileData.StarterWearables.Length; i++)
        {
            if (_playerProfileData.StarterWearables[i].WearableSlot == WearableSlots.HairHat)
                _playerProfileData.StarterWearables[i] = _definitonsMaster.HairHatWearables[m_hairType];
        }

        ChaCreatorWearableManager.instance.PrevLookTarget = _aimTargetPos;
        ChaCreatorWearableManager.instance.InitCharacter();

        _aimTargetPos = new Vector3(0.25f, 0, 0);
        LeanTween.move(GameObject.FindGameObjectWithTag("AimTarget"), _aimTargetPos, 1f);
        ChaCreatorWearableManager.instance.PrevLookTarget = _aimTargetPos;
    }
}
