using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionExclusive : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var m_enemyController = animator.GetComponentInParent<EnemyController>();
        var m_playerController = animator.GetComponentInParent<PlayerSparkController>();
        var m_petController = animator.GetComponentInParent<PetController>();

        if (m_enemyController != null)
        {
            m_enemyController.IsPerformingAction = true;
        }

        if (m_playerController != null)
        {
            m_playerController.IsPerformingAction = true;
        }

        if (m_petController != null)
        {
            m_petController.IsPerformingAction = true;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var m_enemyController = animator.GetComponentInParent<EnemyController>();
        var m_playerController = animator.GetComponentInParent<PlayerSparkController>();
        var m_petController = animator.GetComponentInParent<PetController>();

        if (m_enemyController != null)
        {
            m_enemyController.IsPerformingAction = false;
        }

        if (m_playerController != null)
        {
            m_playerController.IsPerformingAction = false;
        }

        if (m_petController != null)
        {
            m_petController.IsPerformingAction = false;
        }
    }
}
