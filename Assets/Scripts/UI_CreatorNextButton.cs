using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI_CreatorNextButton : MonoBehaviour
{
    public void OnCreatorNextButton()
    {
        SceneManager.LoadSceneAsync("tileMapConversionTest", LoadSceneMode.Additive);
        SceneManager.UnloadSceneAsync("CharacterCreator");
    }
}
