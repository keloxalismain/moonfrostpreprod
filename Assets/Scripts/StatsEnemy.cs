using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsEnemy : StatsCharacter
{
    private Animator _animator;
    private CharacterCombat _combat;

    private void Start()
    {
        _animator = GetComponentInChildren<Animator>();
        _combat = GetComponent<CharacterCombat>();
    }
    public override void Die()
    {
        base.Die();
        _animator.SetTrigger("m_death");
        Invoke("CleanUp", 8f);
    }

    public override void WasHit()
    {
        base.WasHit();
        if(!_combat.IsAttacking) _animator.SetTrigger("m_damage");
    }

    private void CleanUp()
    {
        Destroy(this.gameObject);
    }
}
