using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractablePet : Interactable
{
    private PetController _petController;
    public override void Interact()
    {
        base.Interact();
        GetComponent<Animator>().SetTrigger("Celebration");
        PlayerSparkController.instance.PlayerAnimator.SetTrigger("m_celebrate");
        Debug.Log("playing");
    }

    public override void ArriveAtInteraction()
    {
        if (_petController == null) SetPetController();
        if(_petController.ExcitableWaitTimeRemaining <= 0) base.ArriveAtInteraction();
    }

    public override void LeaveInteraction()
    {
        if (_petController == null) SetPetController();
            base.LeaveInteraction();
        Debug.Log("Leave interaction");
    }

    private void SetPetController()
    {
        _petController = GetComponent<PetController>();        
    }
}
