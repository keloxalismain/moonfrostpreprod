using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(NavMeshAgent))]
public class PlayerSparkController : MonoBehaviour
{
    #region Singleton

    public static PlayerSparkController instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    public Transform VisualsTransform;
    public Interactable Focus;
    public Animator PlayerAnimator;

    public bool ShouldLockRotation = false;
    public bool CouldInteract = false;
    public float TurnPoint = 0.1f;
    public float TiltAngle = 30f;
    public float AngleCheck = 0.05f;
    public float VerticalMultiplier = 1.25f;
    public bool IsAirborne;
    public bool IsPerformingAction = false;

    [SerializeField] private Material[] _interactiveFoliageMats;

    private float _animationDampTime = 0.1f;
    private bool _tempIsShaking = false;

    private NavMeshAgent _navMeshAgent;
    private Transform _target;
    private Camera _mainCamera;

    private float _8waySpeed = 5f;
    private float _base8waySpeed;
    private float _dashPower = 2f;
    private float _dashTime = 0.5f;
    private bool _isDashing = false;

    private Vector3 _moveDirection = Vector3.zero;
    private float _speedForAnimator;

    private Vector3 _lastPos;
    private Vector3 _newPos;
    
    private Joystick _joystick;

    // Start is called before the first frame update
    void Start()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _mainCamera = Camera.main;
        PlayerAnimator = GetComponentInChildren<Animator>();
        _base8waySpeed = _8waySpeed;
        _lastPos = this.transform.position;
        _joystick = Joystick.instance;

        InvokeRepeating("DirectionEvaluate", AngleCheck, AngleCheck);
    }

    // Update is called once per frame
    private void Update()
    {
        //temp test const shake
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (_tempIsShaking)
            {
                CameraShake.instance.ShakeCont(1, 0);
                _tempIsShaking = false;
            }
            else
            {
                CameraShake.instance.ShakeCont(1, 1);
                _tempIsShaking = true;
            }
        }

        if (!IsPerformingAction)
        {

            float m_moveX = Input.GetAxis("Horizontal");
            float m_moveZ = Input.GetAxis("Vertical");

            // Mobile on screen keyboard
            if (_joystick.Horizontal != 0)
                m_moveX = _joystick.Horizontal;
            if (_joystick.Vertical != 0)
                m_moveZ = _joystick.Vertical;

            _moveDirection = new Vector3(m_moveX, 0f, m_moveZ);

            if (m_moveX != 0 || m_moveZ != 0)
            {
                if (!IsAirborne) RemoveFocus();
            }

            if (Input.GetMouseButtonDown(0) || Input.GetButtonDown("Fire2"))
            {
                Ray m_ray = _mainCamera.ScreenPointToRay(Input.mousePosition / UI_SceneManager.instance.UpscaleRatio);
                RaycastHit m_hit;

                if (Physics.Raycast(m_ray, out m_hit, 100))
                {
                    if (EventSystem.current.IsPointerOverGameObject())
                        return;

                    if (m_hit.transform.TryGetComponent(out Interactable m_interactable))
                    {
                        SetFocus(m_interactable);
                    }

                    else
                    {
                        if (!_isDashing && _target == null)
                        {
                            StartCoroutine(Dash());
                        }
                        RemoveFocus();
                    }
                }
            }
        }
        else
        {
            _moveDirection = Vector3.zero;
        }

        for (int i = 0; i < _interactiveFoliageMats.Length; i++)
        {
            _interactiveFoliageMats[i].SetVector("_PlayerPos", VisualsTransform.position + new Vector3(0,1.0f,0f));
        }
    }

    public void SetFocus(Interactable m_newFocus)
    {
        if (m_newFocus != Focus)
        {
            if (Focus != null) Focus.OnDefocused();
            Focus = m_newFocus;
            FollowTarget(m_newFocus);
        }
        m_newFocus.OnFocused(VisualsTransform);
        _navMeshAgent.SetDestination(m_newFocus.InteractionPoint.position);
    }

    public void RemoveFocus()
    {
        if (Focus != null) Focus.OnDefocused();
        Focus = null;
        StopFollowingTarget();
    }

    private void LateUpdate()
    {
        if (!IsAirborne)
        {
            //Vector3 m_movementVec = _moveDirection.normalized * VerticalMultiplier * Time.deltaTime * _8waySpeed;
            Vector3 m_movementVec = new Vector3(_moveDirection.normalized.x, _moveDirection.normalized.y, _moveDirection.normalized.z * VerticalMultiplier) * Time.deltaTime * _8waySpeed;
            _navMeshAgent.Move(m_movementVec);
            VisualsTransform.localPosition = Vector3.zero;
        }
        _newPos = this.transform.position;

        _speedForAnimator = Mathf.Lerp(_speedForAnimator, (_newPos - _lastPos).magnitude / Time.deltaTime, 0.5f);
        _speedForAnimator = Mathf.Round(_speedForAnimator);
        _speedForAnimator = Mathf.Ceil(_speedForAnimator);
        PlayerAnimator.SetFloat("m_agentSpeed", _speedForAnimator);//,_animationDampTime, Time.deltaTime);
    }

    private void DirectionEvaluate()
    {
        if (!ShouldLockRotation && _lastPos != _newPos)
        {
            if (CouldInteract)
            {
                Vector3 m_direction = (_target.position - this.transform.position).normalized;
                Pseudo2dRotation(m_direction);
            }
            else
            {
                Vector3 m_direction = (_newPos - _lastPos).normalized;
                Pseudo2dRotation(m_direction);
            }
            if (Focus != null) _navMeshAgent.stoppingDistance = Focus.InteractableRadius;
        }
        _lastPos = _newPos;

        if (ShouldLockRotation)
        {
            _navMeshAgent.stoppingDistance = 0;
            VisualsTransform.localRotation = Quaternion.Slerp(VisualsTransform.localRotation, _target.localRotation, 0.5f);
        }
    }

    private void Pseudo2dRotation(Vector3 m_direction)
    {
        if (m_direction.x > 0)
        {
            if (m_direction.z > TurnPoint)
            {
                VisualsTransform.localRotation = Quaternion.Euler(TiltAngle / 2, 45, TiltAngle / 2);
            }
            else if (m_direction.z < -TurnPoint)
            {
                VisualsTransform.localRotation = Quaternion.Euler(-TiltAngle / 2, 135, TiltAngle / 2);
            }
            else
            {
                VisualsTransform.localRotation = Quaternion.Euler(0, 90, TiltAngle);
            }
        }
        else if (m_direction.x < 0)
        {
            if (m_direction.z > TurnPoint)
            {
                VisualsTransform.localRotation = Quaternion.Euler(TiltAngle / 2, -45, -TiltAngle / 2);
            }
            else if (m_direction.z < -TurnPoint)
            {
                VisualsTransform.localRotation = Quaternion.Euler(-TiltAngle / 2, -135, -TiltAngle / 2);
            }
            else
            {
                VisualsTransform.localRotation = Quaternion.Euler(0, -90, -TiltAngle);
            }
        }
        else if (m_direction.z > 0)
        {
            VisualsTransform.localRotation = Quaternion.Euler(TiltAngle, 0, 0);
        }
        else
        {
            VisualsTransform.localRotation = Quaternion.Euler(-TiltAngle, 180, 0);
        }
    }

    public void FollowTarget(Interactable m_newTarget)
    {
        _target = m_newTarget.InteractionPoint;
        _navMeshAgent.isStopped = false;
    }

    public void StopFollowingTarget()
    {
        if (!IsAirborne)
        {
            _navMeshAgent.isStopped = true;
            _navMeshAgent.stoppingDistance = 0;
            _target = null;
            PlayerAnimator.SetBool("m_isPerformingAction", false);
        }
    }

    IEnumerator Dash()
    {
        _isDashing = true;
        _8waySpeed = _base8waySpeed * _dashPower;

        if (_speedForAnimator > 1)
        {
            PlayerAnimator.SetTrigger("m_shouldSlide");
        }

        yield return new WaitForSeconds(_dashTime);
        _8waySpeed = _base8waySpeed;

        yield return new WaitForSeconds(_dashTime);
        _isDashing = false;
    }
}
