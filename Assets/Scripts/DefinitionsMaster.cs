using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GEN_DefinitionsMaster_SO", menuName = "Preproduction/Definitions Master")]
public class DefinitionsMaster : ScriptableObject
{
    public List<GameObject> ChibiBaseMeshes;
    public List<GameObject> AvatarBaseMeshes;

    public List<GameObject> ChibiEyeMeshes;

    public List<GameObject> NpcChibiPbs;
    public List<GameObject> NpcAvatarPbs;

    public List<BodyBlendDefinition> BodyBlendDefinitions;
    public List<ColorDuoDefinition> EyeColors;
    public List<ColorSoloDefinition> SkinColors;
    public List<ColorSoloDefinition> HairColors;
    public List<ColorDuoDefinition> ColorConfigs;

    [Space]
    public List<WearableDefinition> HairHatWearables;
    public List<WearableDefinition> FaceAccWearables;
    public List<WearableDefinition> TorsoWearables;
    public List<WearableDefinition> LegsWearables;
    public List<WearableDefinition> BodyAccWearables;
    public List<WearableDefinition> WeaponWearables;

    public List<ItemDefinition> Items;    
}
