using UnityEngine;

[CreateAssetMenu(fileName = "CHA_New_Wearable_SO", menuName = "Preproduction/Inventory/Wearable Config")]
public class WearableDefinition : ItemDefinition
{
    public WearableSlots WearableSlot;
    public WearableSlots[] MaskRegions;

    [Space]
    public SkinnedMeshRenderer chibiAsset = null;
    public SkinnedMeshRenderer avatarAsset = null;

    [Space]
    public ColorDuoDefinition defaultColorConfig = null;

    [Header("Modifiers")]
    [Space]
    public int health = 0;
    public int attack = 0;
    public int defence = 0;
    public int range = 0;
    public int speed = 0;
    public int luck = 0;
    public int charm = 0;

    public override void Use()
    {
        base.Use();

        foreach (var m_currentWearable in WearablesManager.instance.CurrentWearables)
        {

            if (m_currentWearable != null)
            {
                if ((int)m_currentWearable.WearableSlot == (int)WearableSlot)
                {
                    WearablesManager.instance.UnequipWearable((int)m_currentWearable.WearableSlot);
                }

                foreach (var m_currentMaskRegion in m_currentWearable.MaskRegions)
                {
                    foreach (var m_newMaskRegion in MaskRegions)
                    {
                        

                        if ((int)m_currentMaskRegion == (int)m_newMaskRegion)
                        {
                            WearablesManager.instance.UnequipWearable((int)m_currentWearable.WearableSlot);
                        }

                        //else if((int)m_currentMaskRegion == (int)WearableSlot)
                       // {
                          //  WearablesManager.instance.UnequipWearable((int)m_currentWearable.WearableSlot);
                        //}
                    }
                }
            }
        }
        
        //equip
        WearablesManager.instance.EquipWearable(this);

        //remove from inventory
        RemoveFromInventory();
    }
}

public enum WearableSlots { HairHat, FaceAccesories, Torso, Legs, Feet, BodyAccesories, Tattoo1, Tattoo2, Tattoo3, Weapon}
