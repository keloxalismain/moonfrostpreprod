using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ColorEye_BTN : UI_Color_BTN
{
    public override void ConfigButton()
    {
        base.ConfigButton();
        GetComponent<Image>().color = PlayerProfileManager.instance.DefinitionsMaster.EyeColors[_colorListID].PrimaryColor;
        GetComponentInChildren<TMPro.TMP_Text>().text = PlayerProfileManager.instance.DefinitionsMaster.EyeColors[_colorListID].ColorName;
    }

    public override void SetColor()
    {
        base.SetColor();
        PlayerProfileManager.instance.PlayerProfileData.EyeColor = PlayerProfileManager.instance.DefinitionsMaster.EyeColors[_colorListID];
        ChaCreatorWearableManager.instance.InitCharacter();
    }
}
