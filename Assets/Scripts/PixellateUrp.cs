﻿using System.Collections.Generic;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine;

public class PixellateUrp : ScriptableRendererFeature
    {
    public enum RenderQueueType
    {
        Opaque,
        Transparent,
    }

    [System.Serializable]
        public class PixellateSettings
        {
            public RenderPassEvent renderPassEvent = RenderPassEvent.AfterRenderingTransparents;
        public FilterSettings filterSettings = new FilterSettings();
        public Material material;
            [Range(0, 1)]
            public float Dencity = 0.0f;
            public int max = 150;
            public int min = 140;
        }

    [System.Serializable]
    public class FilterSettings
    {
        // TODO: expose opaque, transparent, all ranges as drop down
        public RenderQueueType RenderQueueType;
        public LayerMask LayerMask;
        //public string[] PassNames;

        public FilterSettings()
        {
            RenderQueueType = RenderQueueType.Opaque;
            LayerMask = 0;
        }
    }

    public PixellateSettings settings = new PixellateSettings();
        PixellateUrpPass pixellateLwrpPass;


        public override void Create()
        {
        FilterSettings filter = settings.filterSettings;

        if (settings.renderPassEvent < RenderPassEvent.BeforeRenderingPrePasses)
            settings.renderPassEvent = RenderPassEvent.BeforeRenderingPrePasses;

        pixellateLwrpPass = new PixellateUrpPass(settings.renderPassEvent, settings.material, settings.Dencity, this.name, settings.min, settings.max, filter.LayerMask, filter.RenderQueueType);
        }

        public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
        {
            pixellateLwrpPass.Setup(renderer.cameraColorTarget);
            renderer.EnqueuePass(pixellateLwrpPass);
        }
    
}

