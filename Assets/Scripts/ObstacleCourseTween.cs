using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleCourseTween : MonoBehaviour
{
    [SerializeField] private bool _shouldTweenScale = false;
    [SerializeField] private Vector3 _endScale = Vector3.one;

    [SerializeField, Space] private bool _shouldTweenPosition = false;
    [SerializeField] private Vector3 _endPos = Vector3.zero;

    [SerializeField, Space] private bool _shouldTweenRotation= false;
    [SerializeField] private Vector3 _rotRate = Vector3.zero;


    // Start is called before the first frame update
    void Start()
    {
        if (_shouldTweenScale)
        {
            LeanTween.scale(this.gameObject, _endScale, Random.Range(3,8)).setEasePunch().setLoopPingPong();
        }

        if (_shouldTweenPosition)
        {
            LeanTween.moveLocal(this.gameObject, _endPos, Random.Range(3, 8)).setEaseInOutSine().setLoopPingPong();
        }
    }

    private void FixedUpdate()
    {
        if (_shouldTweenRotation)
        {
            this.transform.localRotation *= Quaternion.Euler(_rotRate);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<StatsCharacter>().TakeDamage(5);
        }
    }
}
