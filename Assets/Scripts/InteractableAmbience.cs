using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableAmbience : MonoBehaviour
{
    [SerializeField] private GameObject _particleEffect;
    
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameObject.Instantiate(_particleEffect, this.transform);
            _particleEffect.GetComponent<ParticleSystem>().Play();
            var test = GetComponent<Animator>();
            test.SetTrigger("PlayerTouched");
        }
    }
}
