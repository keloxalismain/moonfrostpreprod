using UnityEngine;
using UnityEngine.UI;

public class UI_Color_BTN : MonoBehaviour
{
    public int _colorListID;

    private void Start()
    {
        ConfigButton();
    }

    public virtual void ConfigButton()
    {

    }

    public virtual void SetColor()
    {
        ChaCreatorWearableManager.instance.PrevLookTarget = Vector3.zero;
        ChaCreatorWearableManager.instance.AvatarCharCtrl.SetTrigger("NextPose");
        ChaCreatorWearableManager.instance.ChibiCharCtrl.SetTrigger("NextPose");
    }
}
