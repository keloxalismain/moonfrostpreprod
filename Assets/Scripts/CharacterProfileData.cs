using UnityEngine;

public class CharacterProfileData : ScriptableObject
{
	public string FirstName = "Moon";
	public string Surname = "Frost";

	[Space]
	public ColorSoloDefinition SkinColor;
	public ColorDuoDefinition EyeColor;
	public ColorSoloDefinition HairColor;

	[Space]
	public ColorDuoDefinition OverrideHeadColor;
	public ColorDuoDefinition OverrideFaceAccColor;
	public ColorDuoDefinition OverrideTorsoColor;
	public ColorDuoDefinition OverrideLegsColor;
	public ColorDuoDefinition OverrideBodyAccColor;
	public ColorDuoDefinition OverrideWeaponColor;

	public enum eClass
	{
		Vagrant,
		Tank,
		Cleric,
		Archer,
		Mage
	}

	[Space]
	public eClass Class;

	public enum eGender
	{
		CurvyMale,
		Male,
		NonBinary,
		Female,
		CurvyFemale
	}

	[Space]
	public eGender Gender;

	public enum eProNoun
	{
		They,
		He,
		She,
		Ze
	}

	[Space]
	public eProNoun Pronoun;

	[Space]
	public int EyeTypeID;

	[Space]
	public BodyBlendDefinition BodyBlend;
	public int BodyBlendIntensity = 0;

	public WearableDefinition[] StarterWearables;
}

