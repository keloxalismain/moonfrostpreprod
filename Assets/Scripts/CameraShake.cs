using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraShake : MonoBehaviour
{
    #region Singleton
    public static CameraShake instance;
    private void Awake()
    {
        instance = this;
    }
    #endregion
    [SerializeField] private bool _isIso = true;

    private CinemachineVirtualCamera _virtualCamera;
    CinemachineBasicMultiChannelPerlin _camNoise;

    private float _shakeRemaining;
    private bool _isShaking = false;
    
    private Vector3 _isoAngle = new(30, -45, 0);
    private Vector3 _topDownAngle = new(30, 0, 0);
    private Vector3 _rotationLock;

    private void Start()
    {
        _virtualCamera = GetComponent<CinemachineVirtualCamera>();
        _camNoise = _virtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

        if (_isIso) _rotationLock = _isoAngle;
        else _rotationLock = _topDownAngle;
    }

    public void ShakeBurst(float m_intensity, float m_time)
    {
        _camNoise.m_AmplitudeGain = m_intensity;
        _camNoise.m_FrequencyGain = 1;
        _shakeRemaining = m_time;
        _isShaking = true;
    }

    public void ShakeCont(float m_intensity, float m_frequency)
    {
        _camNoise.m_AmplitudeGain = m_intensity;
        _camNoise.m_FrequencyGain = m_frequency;
        if (m_frequency > 0) _isShaking = true;
        else
        {
            _isShaking = false;
            _virtualCamera.transform.localRotation = Quaternion.Euler(_rotationLock);
        }
    }

    private void LateUpdate()
    {
        if (_isShaking)
        {
            _virtualCamera.transform.localRotation = Quaternion.Euler(_rotationLock);
        }
    }
    private void Update()
    {
        if (_shakeRemaining > 0)
        {
            _shakeRemaining -= Time.deltaTime;
            if (_shakeRemaining <= 0f)
            {
                _camNoise.m_AmplitudeGain = 0f;
                _isShaking = false;
                _virtualCamera.transform.localRotation = Quaternion.Euler(_rotationLock);
            }
        }
    }
}
