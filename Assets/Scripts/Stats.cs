using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stats
{
    [SerializeField] private int _baseValue;

    private List<int> _modifiers = new List<int>();

    public int GetValue()
    {
        int m_modifiedValue = _baseValue;
        _modifiers.ForEach(x => m_modifiedValue += x);

        return m_modifiedValue;
    }

    public void AddModifier (int m_modifier)
    {
        if (m_modifier != 0) _modifiers.Add(m_modifier);
    }

    public void RemoveModifier(int m_modifier)
    {
        if (m_modifier != 0) _modifiers.Remove(m_modifier);
    }
}
