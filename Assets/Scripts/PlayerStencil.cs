using System.Collections;
using UnityEngine;

public class PlayerStencil : MonoBehaviour
{
    [SerializeField] private GameObject _stencilSphere;
    [SerializeField] private LayerMask _stencilLayerMask;
    [SerializeField] private Vector3 _stencilOffset;

    private Camera _camera;
    private Vector3 _sphereScale = new(3, 3, 3);
    private float _tweenRemaining;
    private bool _isTweening = false;

    void Start()
    {
        _camera = Camera.main;
    }

    void Update()
    {
        _tweenRemaining -= Time.deltaTime;

        if (_tweenRemaining <= 0f && !_isTweening)
        {
            RaycastHit m_hit;

            if (Physics.Raycast(_camera.transform.position, ((_stencilSphere.transform.position + _stencilOffset) - _camera.transform.position + _stencilOffset).normalized, out m_hit, 100f, _stencilLayerMask))
            {
                if (m_hit.collider.gameObject.tag == "Occluder")
                {
                    _stencilSphere.LeanScale(_sphereScale, 1f).setEaseOutBounce();
                }

                else
                {
                    _stencilSphere.LeanScale(Vector3.zero, 1f).setEaseOutBounce();
                }
                StartCoroutine(Tween());
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere((_stencilSphere.transform.position + _stencilOffset), 1f);
    }

IEnumerator Tween()
    {
        _isTweening = true;
        _tweenRemaining = .5f;

        yield return new WaitForSeconds(_tweenRemaining);
        _isTweening = false;
    }

}
