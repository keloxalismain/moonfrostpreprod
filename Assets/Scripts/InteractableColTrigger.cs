using UnityEngine;

public class InteractableColTrigger : MonoBehaviour
{
    private Interactable _parentInteractable;
    private void Start()
    {
        _parentInteractable = GetComponentInParent<Interactable>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) _parentInteractable.ArriveAtInteraction();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player")) _parentInteractable.LeaveInteraction();
    }
}
