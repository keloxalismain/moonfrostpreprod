using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarDebug : MonoBehaviour
{
    [SerializeField] private Animator[] _animators;

    public void OnBlinkButton()
    {
        foreach (var m_animator in _animators)
        {
            m_animator.SetTrigger("m_shouldBlink");
        }
    }

    public void OnWinkButton()
    {
        foreach (var m_animator in _animators)
        {
            m_animator.SetTrigger("m_shouldWink");
        }
    }

    public void OnIsTalkingButton()
    {
        foreach (var m_animator in _animators)
        {
            m_animator.SetBool("m_isTalking", !m_animator.GetBool("m_isTalking"));
        }
    }
}
