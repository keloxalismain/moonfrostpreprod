using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionAttack : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var m_charCombat = animator.GetComponentInParent<CharacterCombat>();
        m_charCombat.IsAttacking = true;
        m_charCombat.AttackAction();
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponentInParent<CharacterCombat>().IsAttacking = false;
    }
}
