﻿using System.Collections.Generic;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering;
using UnityEngine.Scripting.APIUpdating;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class PixellateUrpPass : ScriptableRenderPass
{
    public Material material;
    FilteringSettings m_FilteringSettings;
    PixellateUrp.RenderQueueType renderQueueType;

    private RenderTargetIdentifier source;
    private RenderTargetIdentifier tempCopy = new RenderTargetIdentifier(tempCopyString);

    readonly string tag;
    readonly float dencity;
    readonly int max = 150;
    readonly int min = 140;

    static readonly int dencityString = Shader.PropertyToID("_Dencity");
    static readonly int tempCopyString = Shader.PropertyToID("_TempCopy");

    float r;

    public PixellateUrpPass(RenderPassEvent renderPassEvent, Material material, float dencity, string tag, int min, int max, int layerMask, PixellateUrp.RenderQueueType renderQueueType)
    {
        this.renderPassEvent = renderPassEvent;
        this.material = material;
        this.dencity = dencity;
        this.tag = tag;
        this.min = min;
        this.max = max;
        this.renderQueueType = renderQueueType;

        RenderQueueRange renderQueueRange = (renderQueueType == PixellateUrp.RenderQueueType.Transparent)
                ? RenderQueueRange.transparent
                : RenderQueueRange.opaque;


        m_FilteringSettings = new FilteringSettings(renderQueueRange, layerMask);
    }

    public void Setup(RenderTargetIdentifier source)
    {
        this.source = source;
    }

    public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
    {
        /*SortingCriteria sortingCriteria = (renderQueueType == PixellateUrp.RenderQueueType.Transparent)
                ? SortingCriteria.CommonTransparent
                : renderingData.cameraData.defaultOpaqueSortFlags;

        DrawingSettings drawingSettings = CreateDrawingSettings(null, ref renderingData, sortingCriteria);
        drawingSettings.overrideMaterial = material;
        drawingSettings.overrideMaterialPassIndex = 0;*/
        //at least 80% sure this isn't actually supported yet, reminder to revisit filtering


        if (dencity > 0)
        {

            CommandBuffer cmd = CommandBufferPool.Get(tag);
            RenderTextureDescriptor opaqueDesc = renderingData.cameraData.cameraTargetDescriptor;
            opaqueDesc.depthBufferBits = 0;

            cmd.GetTemporaryRT(tempCopyString, opaqueDesc, FilterMode.Point);
            // 

            cmd.CopyTexture(source, tempCopy);
            r = (float)Screen.width / Screen.height;
            Vector2 data = (r > 1 ? new Vector2(r, 1) : new Vector2(1, r));
            material.SetVector(dencityString, data * (max - dencity * min));


            //context.DrawRenderers(renderingData.cullResults, ref drawingSettings, ref m_FilteringSettings); at least 80% sure this isn't actually supported yet, reminder to revisit filtering
            cmd.Blit(tempCopy, source, material, 0);
            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);
        }
    }

    public override void FrameCleanup(CommandBuffer cmd)
    {
        cmd.ReleaseTemporaryRT(tempCopyString);
    }
}

