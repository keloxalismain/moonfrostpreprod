using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_GenericTween : MonoBehaviour
{
    public Vector3 TweenOpening;
    public Vector3 TweenClosing;
    public float TweenDuration = .5f;

    public void ToggleTween()
    {
        if (this.gameObject.activeSelf)
        {
            LeanTween.move(this.gameObject.GetComponent<RectTransform>(), TweenClosing, TweenDuration).setEaseSpring();
            Invoke("ToggleEnable", TweenDuration);
        }
        else
        {
            ToggleEnable();
            LeanTween.move(this.gameObject.GetComponent<RectTransform>(), TweenOpening, TweenDuration).setEaseSpring();
        }
    }

    private void ToggleEnable()
    {
        this.gameObject.SetActive(!this.gameObject.activeSelf);
    }
}
