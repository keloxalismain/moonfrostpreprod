using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractablePickup : Interactable
{
    public ItemDefinition Item;

    public override void Interact()
    {
        base.Interact();
    }

    public override void InteractAnimComplete()
    {
        base.InteractAnimComplete();
        Pickup();
    }

    private void Pickup()
    {
        //pop up box? found .... or show card?

        bool m_wasAddedToInventory = PlayerInventoryManager.instance.AddToPlayerInventory(Item);
        if(m_wasAddedToInventory) DestroyOnPickup();        
    }

    void DestroyOnPickup()
    {
        PlayerSparkController.instance.RemoveFocus();
        Destroy(this.gameObject);
    }
}
