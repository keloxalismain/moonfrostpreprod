using EasyBuildSystem.Features.Scripts.Core.Base.Builder;
using EasyBuildSystem.Features.Scripts.Core.Base.Builder.Enums;
using UnityEngine;
using UnityEngine.UI;

public class UI_BLD_Menu_Manager : MonoBehaviour
{

    private string ModeName = "Mode: {0}";
    private string SelectedName = "Selected: {0}";


    [SerializeField] private TMPro.TMP_Text _ModeTmproText;
    [SerializeField] private TMPro.TMP_Text _SelectTmproText;

    private void Update()
    {
        if (BuilderBehaviour.Instance == null)
            return;

        if (BuilderBehaviour.Instance.SelectedPiece == null)
            return;

        _ModeTmproText.text = string.Format(ModeName, BuilderBehaviour.Instance.CurrentMode.ToString());

        if (BuilderBehaviour.Instance.CurrentMode == EasyBuildSystem.Features.Scripts.Core.Base.Builder.Enums.BuildMode.None)
        {
            _SelectTmproText.text = "Selected: None";
        }
        else
        _SelectTmproText.text = string.Format(SelectedName, BuilderBehaviour.Instance.SelectedPiece.Name);
    }

    public void OnBuildButtonPressed()
    {
        BuilderBehaviour.Instance.ChangeMode(BuildMode.Placement);
    }

    public void OnEditButtonPressed()
    {
        BuilderBehaviour.Instance.ChangeMode(BuildMode.Edit);
    }

    public void OnPaintButtonPressed()
    {
        BuilderBehaviour.Instance.ChangeMode(BuildMode.None);
    }

    public void OnDeleteButtonPressed()
    {
        BuilderBehaviour.Instance.ChangeMode(BuildMode.Destruction);
    }

    public void OnCancelBuildMode()
    {
        BuilderBehaviour.Instance.ChangeMode(BuildMode.None);
    }
}
