using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBreakable : Interactable
{
    [Space]
    [SerializeField] private int _requiredHitsToBreak = 3;

    [Space]
    [SerializeField] private GameObject _spawnOnHit;
    [SerializeField] private int _percentChanceOnHit = 25;

    [Space]
    [SerializeField] private GameObject _spawnOnBreak;
    [SerializeField] private int _percentChanceOnBreak = 90;

    private float _hitCooldownTime = 0.25f;
    private bool _hasHit = false;

    private int _hitCount = 0;
    public override void Interact()
    {
        base.Interact();
    }

    public override void InteractAnimComplete()
    {
        base.InteractAnimComplete();
        InteractableHit();
    }

    private void InteractableHit()
    {
        if (!_hasHit)
        {
            if (_hitCount >= _requiredHitsToBreak - 1)
            {
                Invoke("DestroyOnBreak", _hitCooldownTime);
            }
            else StartCoroutine(Hit());
        }
    }

    void DestroyOnBreak()
    {
        //move to function JIC we need it from an anim event
        PlayerSparkController.instance.RemoveFocus();

        if(_spawnOnBreak != null)
        {
            int m_randNumber = Random.Range(0, 101);
            if (m_randNumber <= _percentChanceOnBreak)
            {
                var m_spawnedObj = GameObject.Instantiate(_spawnOnBreak, this.transform);

                LeanTween.moveY(m_spawnedObj, transform.localPosition.y + 0.5f, 0.25f).setEaseShake();
                m_spawnedObj.transform.parent = null;
            }
        }

        Destroy(this.gameObject);
    }

    IEnumerator Hit() //should probably tie to animation event instead of coroutine... or both
    {
        _hasHit = true;
        LeanTween.moveY(this.gameObject, transform.position.y + 0.25f, 0.25f).setEaseShake().setLoopOnce();

        if (_spawnOnHit != null)
        {
            int m_randNumber = Random.Range(0, 101);
            if (m_randNumber <= _percentChanceOnHit)
            {
                Vector3 m_spawnEndPos = new Vector3(transform.localPosition.x + Random.Range(-.5f, .5f), 1, transform.localPosition.z + Random.Range(-.5f, .5f));
                var m_spawnedObj = GameObject.Instantiate(_spawnOnHit, this.transform);
                
                LeanTween.move(m_spawnedObj, m_spawnEndPos, 0.5f).setEaseInOutBounce();
                m_spawnedObj.transform.parent = null;
                Debug.Log("Item dropped on hit");
            }
        }

        yield return new WaitForSeconds(_hitCooldownTime);

        _hitCount++;
        _hasHit = false;
    }
}
