using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RainOccluder : MonoBehaviour
{
    [System.Serializable]
    public struct RainOccluderTuple
    {
        public Collider RainCollider;
        public Texture RainMask;
        public float RainMaskAngle;
    }
    public List<RainOccluderTuple> RainOccluderTuples;
    private Rigidbody _rigidBody;
    public LayerMask RainLayerMask;

    private int _triggerOccluderLoop = 0;
    private int _updateOccluderLoop = 0;

    private void Update()
    {
        if (TimeOfDayManager.instance.MoistureLevel != TimeOfDayManager.eMoistureLevels.dry)
        {
            if (GetComponent<Rigidbody>() == null)
            {
                _rigidBody = this.gameObject.AddComponent<Rigidbody>();
                _rigidBody.useGravity = false;
            }

            if (_updateOccluderLoop <= _triggerOccluderLoop)
            {
                _updateOccluderLoop++;
                var m_Collider = RainOccluderTuples[_triggerOccluderLoop].RainCollider;
                if (m_Collider != null)
                    m_Collider.enabled = true;
            }
        }

        else
        {
            //_collider.enabled = false;
            if (_rigidBody != null)
                Destroy(_rigidBody);
        }
    }

    private void OnTriggerEnter(Collider m_other)
    {
        if (m_other.gameObject.layer == 6)
        {
            Material m_maskableMaterial = m_other.gameObject.GetComponentInChildren<MeshRenderer>().material;
            m_maskableMaterial.SetTexture("_RainMask", RainOccluderTuples[_triggerOccluderLoop].RainMask);
            m_maskableMaterial.SetFloat("_RainMaskAngle", RainOccluderTuples[_triggerOccluderLoop].RainMaskAngle);
            _triggerOccluderLoop++;
        }
    }
}
