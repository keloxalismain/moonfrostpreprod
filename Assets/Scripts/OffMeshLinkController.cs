using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class OffMeshLinkController : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private bool _isBiDirectional;
    [SerializeField] private float _duration = 1f;
    [SerializeField] private bool _scaleX = true;
    [SerializeField] private bool _scaleZ = true;
    public string ButtonText = "Jump";
    public Transform InteractableUiPoint;

    private UI_JumpPopup _jumpPopup;
    private float _landingDelay = 0.5f;

    private void Start()
    {
        _jumpPopup = UI_SceneManager.instance.Canvas.GetComponent<UI_JumpPopup>();

        if (InteractableUiPoint == null) InteractableUiPoint = _target;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) _jumpPopup.OpenInteractionPopup(this);

    }

    public void OnJumpButton()
    {
        PlayerSparkController.instance.gameObject.GetComponent<NavMeshAgent>().enabled = false;
        PlayerSparkController.instance.PlayerAnimator.SetBool("m_isAirborne", true);
        PlayerSparkController.instance.PlayerAnimator.SetBool("m_isBiDirectional", _isBiDirectional);
        PlayerSparkController.instance.IsAirborne = true;

        LeanTween.moveY(GameObject.Find("Player"), _target.position.y, _duration).setEaseInSine();

        //split these to make link colliders wider and easier to set up in scene
        Invoke("OnJumpComplete", _duration);

        if (_scaleX)
            LeanTween.moveX(GameObject.Find("Player"), _target.position.x , _duration).setEaseInSine();
        if(_scaleZ)
            LeanTween.moveZ(GameObject.Find("Player"), _target.position.z, _duration).setEaseInSine();

        LeanTween.moveLocalY(GameObject.Find("UI_CHA_Chibi_01_PB(Clone)"), 1, _duration/2).setLoopPingPong(1).setEaseOutCirc();

        
    }

    private void OnJumpComplete()
    {
        PlayerSparkController.instance.PlayerAnimator.SetBool("m_isAirborne", false);
        PlayerSparkController.instance.PlayerAnimator.SetBool("m_isBiDirectional", false);
        Invoke("LandingDelay", _landingDelay);
    }

    private void LandingDelay()
    {
        PlayerSparkController.instance.IsAirborne = false;
        PlayerSparkController.instance.gameObject.GetComponent<NavMeshAgent>().enabled = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player")) _jumpPopup.CloseInteractionPopup();
    }
}
