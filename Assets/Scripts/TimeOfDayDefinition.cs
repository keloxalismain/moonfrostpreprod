using UnityEngine;

[CreateAssetMenu(fileName = "ENV_ToD_NewRegion_SO", menuName = "Preproduction/Time of Day Settings")]
public class TimeOfDayDefinition : ScriptableObject
{
    public bool HasDayNightCycle = true;

    [Space]
    public float LightsOffTime24h = 5.5f;
    public float lightsOnTime24h = 20f;

    [Space]
    public float Light_X_angle = 130f;
    public float SunBrightness = 1f;
    public float MoonBrightness = 0.5f;    

    [Space]
    public Color SunColour;
    public Color MoonColour;

    [Space, Header("Weather Settings")]
    public float FogDensity = 50f;
    public float CloudIntensity = 0.5f;
    public float LightningIntensity = 3f;
    public float LighningFrequency = 10f;
    public float SnowDensity = 15f;
    public float RainDensity = 150f;

    [Space]
    public AnimationCurve LightCurve;
    public Gradient AmbienceGradient;
}
