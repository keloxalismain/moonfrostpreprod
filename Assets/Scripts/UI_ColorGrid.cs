using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GridLayoutGroup))]
public class UI_ColorGrid : MonoBehaviour
{
    private GridLayoutGroup _gridLayoutGroup;
    // Start is called before the first frame update
    void Awake()
    {
        _gridLayoutGroup = GetComponent<GridLayoutGroup>();
        UI_Color_BTN[] m_ColorButtons = GetComponentsInChildren<UI_Color_BTN>();

        for (int i = 0; i <= _gridLayoutGroup.constraintCount; i++)
        {
            m_ColorButtons[i]._colorListID = i;
        }
    }
}
