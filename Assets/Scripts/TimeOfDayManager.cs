using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;

public class TimeOfDayManager : MonoBehaviour
{
    #region Singleton
    public static TimeOfDayManager instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    public TimeOfDayDefinition RegionLightingSettings;
    public float TimeNormalised;
    public eWeather WeatherState;
    public bool IsNightime = false;
    private eWeather _prevWeatherState;
    public eMoistureLevels MoistureLevel;

    private float _prevFogDensity, _targetFogDensity;
    private float _prevCloudIntensity, _targetCloudIntensity;
    private float _prevSnowDensity, _targetSnowDensity;
    private float _prevRainDensity, _targetRainDensity;
    private float _prevWetness, _targetWetness;


    [Space]
    [SerializeField] private TMPro.TMP_Text _clockUI;
    [SerializeField] private TMPro.TMP_Text _dateUI;
    [SerializeField] private Light _sunDirectionalLight;
    [SerializeField] private Light _stormDirectionalLight;

    [Space]
    [SerializeField] private Volume _dayClearVolume;
    [SerializeField] private Volume _nightClearVolume;
    [SerializeField] private Volume _fogVolume;

    private DateTime _currentTime;
    private float _sunDirectionalLightAngle;
    private Quaternion _uiRotation;
    private float _sunriseTime;
    private float _sunsetTime;

    [Space]
    [SerializeField] private float _timeRateMultiplier = 1f;
    [SerializeField] private float _weatherTransition = 5f;
    [SerializeField] private float _dryingDuration = 10f;

    [SerializeField] private ParticleSystem _snowParticleSystem;
    [SerializeField] private ParticleSystem _rainParticleSystem;

    private float _weatherDuration = 0f;
    private bool _weatherChanged = false;
    private bool _isDrying = false;
    private bool _isStorm, _lightningHasStruck = false;
    private float _lightningInterval = 5f;
    private float _lightningIntensity = 0f;

    public enum eWeather { isSunny, isCloudy, isFoggy, isRaining, isStorm, isSnowing }
    public enum eMoistureLevels { dry, moist, wet }

    // Start is called before the first frame update
    void Start()
    {
        _currentTime = DateTime.Now.Date + DateTime.Now.TimeOfDay;
        _uiRotation = _clockUI.rectTransform.localRotation;

        _sunriseTime = RegionLightingSettings.LightsOffTime24h / 24;
        _sunsetTime = RegionLightingSettings.lightsOnTime24h / 24;

        _prevWeatherState = WeatherState;
        ChangeWeatherSettings();
        HandleWeather();
    }

    private void ChangeWeatherSettings()
    {
        _stormDirectionalLight.enabled = false;
        _isStorm = false;

        switch (WeatherState)
        {
            case eWeather.isSunny:
                _targetFogDensity = 0f;
                _targetCloudIntensity = 0.4f;
                _targetSnowDensity = 0f;
                _targetRainDensity = 0f;
                _targetWetness = 0f;
                break;
            case eWeather.isCloudy:
                _targetFogDensity = 0.35f;
                _targetCloudIntensity = 0.7f;
                _targetSnowDensity = 0f;
                _targetRainDensity = 0f;
                _targetWetness = 0f;
                break;
            case eWeather.isFoggy:
                _targetFogDensity = 1f;
                _targetCloudIntensity = .6f;
                _targetSnowDensity = 0f;
                _targetRainDensity = 0f;
                _targetWetness = 0f;
                break;
            case eWeather.isRaining:
                _targetFogDensity = 0.25f;
                _targetCloudIntensity = 0.8f;
                _targetSnowDensity = 0f;
                _targetRainDensity = RegionLightingSettings.RainDensity;
                _targetWetness = 0.75f;
                MoistureLevel = eMoistureLevels.wet;
                break;
            case eWeather.isStorm:
                _targetFogDensity = 1f;
                _targetCloudIntensity = 1f;
                _targetSnowDensity = 0f;
                _targetRainDensity = RegionLightingSettings.RainDensity * 2;
                _targetWetness = 1f;
                _isStorm = true;
                _stormDirectionalLight.enabled = true;
                MoistureLevel = eMoistureLevels.wet;
                break;
            case eWeather.isSnowing:
                _targetFogDensity = .5f;
                _targetCloudIntensity = .6f;
                _targetSnowDensity = RegionLightingSettings.SnowDensity;
                _targetRainDensity = 0f;
                _targetWetness = 0.5f;
                MoistureLevel = eMoistureLevels.moist;
                break;
            default:
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (RegionLightingSettings.HasDayNightCycle)
        {
            UpdateTimeOfDay();
            HandleSunAngle();
            UpdateLightSettings();
        }

        //if is day time
        if (TimeNormalised > _sunriseTime && TimeNormalised < _sunsetTime)
        {
            Shader.SetGlobalInt("_ShouldLightsOn", 0);
            IsNightime = false;
        }
        //is nighttime 
        else
        {
            Shader.SetGlobalInt("_ShouldLightsOn", 1);
            IsNightime = true;
        }

        if (_prevWeatherState != WeatherState)
        {
            _weatherChanged = true;
            _weatherDuration = 0;
            _prevWeatherState = WeatherState;
            ChangeWeatherSettings();
        }

        if (_weatherChanged)
        {
            _weatherDuration += Time.deltaTime / _weatherTransition;

            if (_weatherDuration > 1)
            {
                _weatherChanged = false;
                _prevFogDensity = _targetFogDensity;
                _prevCloudIntensity = _targetCloudIntensity;
                _prevSnowDensity = _targetSnowDensity;
                _prevRainDensity = _targetRainDensity;
                _prevWetness = _targetWetness;
            }
            else HandleWeather();
        }

        if (_isStorm)
        {
            _lightningInterval -= Time.deltaTime;
            if (_lightningInterval < 0)
            {
                if (_lightningHasStruck == false)
                {
                    if (_lightningIntensity <= RegionLightingSettings.LightningIntensity)
                    {
                        _lightningIntensity += (Time.deltaTime * RegionLightingSettings.LightningIntensity) * 2;
                        _stormDirectionalLight.intensity = _lightningIntensity;
                    }
                    else
                        _lightningHasStruck = true;

                }
                else
                {
                    _lightningIntensity -= (Time.deltaTime * (RegionLightingSettings.LightningIntensity * 1));
                    _stormDirectionalLight.intensity = _lightningIntensity;
                    if (_lightningIntensity <= 0)
                    {
                        _lightningInterval = RegionLightingSettings.LighningFrequency + UnityEngine.Random.Range(-RegionLightingSettings.LighningFrequency / 2, RegionLightingSettings.LighningFrequency / 2);
                        _lightningHasStruck = false;
                    }
                }
            }
        }

        if (MoistureLevel == eMoistureLevels.wet)
        {
            PetController.instance.ShouldRest = true;
            if (WeatherState == eWeather.isRaining || WeatherState == eWeather.isStorm)
            {                
                return;
            }
            else Invoke("MoistSteps", _weatherDuration);            
        }

        else if (MoistureLevel == eMoistureLevels.moist && _isDrying == false)
        {
            if (WeatherState != eWeather.isSnowing)
            {
                Invoke("DrySteps", _dryingDuration);
                _isDrying = true;
            }
            PetController.instance.ShouldRest = true;
        }

        else
        {
            if (PetController.instance != null)
            {
                if (IsNightime) PetController.instance.ShouldRest = true;
                else PetController.instance.ShouldRest = false;
            }
        }
    }

    private void DrySteps()
    {
        _isDrying = false;
        MoistureLevel = eMoistureLevels.dry;
    }

    private void MoistSteps()
    {
        MoistureLevel = eMoistureLevels.moist;
    }

    private void UpdateTimeOfDay()
    {
        _currentTime = _currentTime.AddSeconds(Time.deltaTime * _timeRateMultiplier);

        if (_clockUI != null)
            _clockUI.text = _currentTime.ToString("HH:mm");

        if (_dateUI != null)
            _dateUI.text = _currentTime.ToString("ddd d/MMM");

        var m_lerpTime = ((float)_currentTime.Hour) + (((float)_currentTime.Minute) / 60);
        m_lerpTime /= 24;
        TimeNormalised = m_lerpTime % 1f;

        _uiRotation = Quaternion.Euler(0, 0, -(TimeNormalised * 720f) - 90f);
        _clockUI.rectTransform.rotation = _uiRotation;
    }

    private void HandleSunAngle()
    {
        _sunDirectionalLightAngle = TimeNormalised * 360f;
        _sunDirectionalLight.transform.rotation = Quaternion.Euler(RegionLightingSettings.Light_X_angle, _sunDirectionalLightAngle - 45f, 0f);
    }

    private void UpdateLightSettings()
    {
        _sunDirectionalLight.intensity = Mathf.Lerp(RegionLightingSettings.MoonBrightness, RegionLightingSettings.SunBrightness, RegionLightingSettings.LightCurve.Evaluate(TimeNormalised));
        _sunDirectionalLight.color = Color.Lerp(RegionLightingSettings.MoonColour, RegionLightingSettings.SunColour, RegionLightingSettings.LightCurve.Evaluate(TimeNormalised));
        _dayClearVolume.weight = Mathf.Lerp(0, 1, RegionLightingSettings.LightCurve.Evaluate(TimeNormalised));
        _nightClearVolume.weight = Mathf.Lerp(1, 0, RegionLightingSettings.LightCurve.Evaluate(TimeNormalised));

        RenderSettings.ambientLight = RegionLightingSettings.AmbienceGradient.Evaluate(TimeNormalised);
    }

    private void HandleWeather()
    {
        Shader.SetGlobalFloat("_CloudDensity", Mathf.Lerp(_prevCloudIntensity, _targetCloudIntensity, _weatherDuration));
        _snowParticleSystem.emissionRate = Mathf.Lerp(_prevSnowDensity, _targetSnowDensity, _weatherDuration);
        _rainParticleSystem.emissionRate = Mathf.Lerp(_prevRainDensity, _targetRainDensity, _weatherDuration);
        _fogVolume.weight = Mathf.Lerp(_prevFogDensity, _targetFogDensity, _weatherDuration);
        Shader.SetGlobalFloat("_SpecIntensity", Mathf.Lerp(_prevWetness, _targetWetness, _weatherDuration));
    }
}