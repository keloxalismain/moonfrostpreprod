using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI_SceneManager : MonoBehaviour
{
    #region Singleton
    public static UI_SceneManager instance;

    private void Awake()
    {
        instance = this;
        UpscaleRatio = 1;// Screen.height / _resoloution;
    }

    #endregion

    public GameObject Canvas;
    public int UpscaleRatio;

    private readonly int _resoloution = 360;

    private void Start()
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("tileMapConversionTest"));
    }
   
}
