using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightCookieManager : MonoBehaviour
{
    [SerializeField] private List<Texture> _lightCookies;
    [SerializeField] private int _intervalTime = 2;
   private Light _light;
    // Start is called before the first frame update
    void Start()
    {
        _light = GetComponent<Light>();
        InvokeRepeating("RandomLightCookies",0f, _intervalTime);
    }

    private void RandomLightCookies()
    {
        _light.cookie = _lightCookies[Random.Range(0, _lightCookies.Count)];
    }
}
