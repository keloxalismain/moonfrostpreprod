//--------------------------------------------------------------------
// From UnityPackage 2D Pixel Perfect - refactored
// Created by Alexis Bacot - 2021 - www.alexisbacot.com - All rights reserved
//--------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.U2D;

//--------------------------------------------------------------------
namespace SnakeDungeon
{
    //--------------------------------------------------------------------
    [DisallowMultipleComponent] 
    public class Pixelizer : MonoBehaviour
    {
        [Header("Settings")]
        public bool isEnabled = true;
        public bool isUpdate = false;
        public int resolutionY = 180;
        private int resolutionX = -1;
        public int assetsPPU = 100;
        public bool cropFrameX = true;
        public bool cropFrameY = true;
        public bool stretchFill = false;

        // Internal
        private Camera m_Camera;
        private bool cropFrameXAndY = false;
        private bool cropFrameXOrY = false;
        private bool useStretchFill = false;
        private int zoom = 1;
        private bool useOffscreenRT = false;
        private int offscreenRTWidth = 0;
        private int offscreenRTHeight = 0;
        private Rect pixelRectPreCull = Rect.zero;
        //private float orthoSize = 1.0f;
        private float unitsPerPixel = 0.0f;
        //private int cinemachineVCamZoom = 1;
        private Rect pixelRectPostRender;

        //--------------------------------------------------------------------
        /*public void Init(GameManager game_)
        {
            m_Camera = game_.cameraController.cam;

            if (isEnabled)
            {
                m_Camera.forceIntoRenderTexture = true;

                CalculateCameraProperties(Screen.width, Screen.height);

                CalculatePostRenderPixelRect(m_Camera.aspect, Screen.width, Screen.height);
            }
            else
            {
                this.enabled = false;
            }
        }*/

        private void Awake()
        {
            m_Camera = Camera.main;

            if (isEnabled)
            {
                m_Camera.forceIntoRenderTexture = true;

                CalculateCameraProperties(Screen.width, Screen.height);

                CalculatePostRenderPixelRect(m_Camera.aspect, Screen.width, Screen.height);
            }
            else
            {
                this.enabled = false;
            }
        }

        //--------------------------------------------------------------------
        void OnPreCull()
        {
            if (isUpdate) CalculateCameraProperties(Screen.width, Screen.height);

            m_Camera.pixelRect = pixelRectPreCull;
        }

        //--------------------------------------------------------------------
        void OnPreRender()
        {
            // Required for sprites, but we're not using them
            //PixelPerfectRendering.pixelSnapSpacing = unitsPerPixel;
        }

        //--------------------------------------------------------------------
        void OnPostRender()
        {
            // Required for sprites, but we're not using them
            //PixelPerfectRendering.pixelSnapSpacing = 0.0f;

            if (m_Camera.activeTexture != null)
            {
                Graphics.SetRenderTarget(null as RenderTexture);
                GL.Viewport(new Rect(0.0f, 0.0f, Screen.width, Screen.height));
                GL.Clear(false, true, Color.black);
            }

            if (m_Camera.activeTexture != null)
                m_Camera.activeTexture.filterMode = useStretchFill ? FilterMode.Bilinear : FilterMode.Point;

            if (isUpdate) CalculatePostRenderPixelRect(m_Camera.aspect, Screen.width, Screen.height);

            m_Camera.pixelRect = pixelRectPostRender;
        }

        //--------------------------------------------------------------------
        private void CalculateCameraProperties(int screenWidth, int screenHeight)
        {
            bool upscaleRT = true; 
            bool pixelSnapping = false;

            resolutionX = resolutionY * 16 / 9;

            cropFrameXAndY = cropFrameY && cropFrameX;
            cropFrameXOrY = cropFrameY || cropFrameX;
            useStretchFill = cropFrameXAndY && stretchFill;

            // zoom level (PPU scale)
            int verticalZoom = screenHeight / resolutionY;
            int horizontalZoom = screenWidth / resolutionX;
            zoom = Mathf.Max(1, Mathf.Min(verticalZoom, horizontalZoom));

            // off-screen RT
            useOffscreenRT = false;
            offscreenRTWidth = 0;
            offscreenRTHeight = 0;

            if (cropFrameXOrY)
            {
                if (!upscaleRT)
                {
                    if (useStretchFill)
                    {
                        useOffscreenRT = true;
                        offscreenRTWidth = zoom * resolutionX;
                        offscreenRTHeight = zoom * resolutionY;
                    }
                }
                else
                {
                    useOffscreenRT = true;
                    if (cropFrameXAndY)
                    {
                        offscreenRTWidth = resolutionX;
                        offscreenRTHeight = resolutionY;
                    }
                    else if (cropFrameY)
                    {
                        offscreenRTWidth = screenWidth / zoom / 2 * 2;   // Make sure it's an even number by / 2 * 2.
                        offscreenRTHeight = resolutionY;
                    }
                    else    // crop frame X
                    {
                        offscreenRTWidth = resolutionX;
                        offscreenRTHeight = screenHeight / zoom / 2 * 2;   // Make sure it's an even number by / 2 * 2.
                    }
                }
            }
            else if (upscaleRT && zoom > 1)
            {
                useOffscreenRT = true;
                offscreenRTWidth = screenWidth / zoom / 2 * 2;        // Make sure it's an even number by / 2 * 2.
                offscreenRTHeight = screenHeight / zoom / 2 * 2;
            }

            // viewport
            pixelRectPreCull = Rect.zero;

            if (cropFrameXOrY && !upscaleRT && !useStretchFill)
            {
                if (cropFrameXAndY)
                {
                    pixelRectPreCull.width = zoom * resolutionX;
                    pixelRectPreCull.height = zoom * resolutionY;
                }
                else if (cropFrameY)
                {
                    pixelRectPreCull.width = screenWidth;
                    pixelRectPreCull.height = zoom * resolutionY;
                }
                else // crop frame X
                {
                    pixelRectPreCull.width = zoom * resolutionX;
                    pixelRectPreCull.height = screenHeight;
                }

                pixelRectPreCull.x = (screenWidth - (int)pixelRectPreCull.width) / 2;
                pixelRectPreCull.y = (screenHeight - (int)pixelRectPreCull.height) / 2;
            }
            else if (useOffscreenRT)
            {
                // When Camera.forceIntoRenderTexture is true, the size of the internal RT is determined by VP size.
                // That's why we set the VP size to be (m_OffscreenRTWidth, m_OffscreenRTHeight) here.
                pixelRectPreCull = new Rect(0.0f, 0.0f, offscreenRTWidth, offscreenRTHeight);
            }

            // orthographic size
            //if (cropFrameY)
            //    orthoSize = (resolutionY * 0.5f) / assetsPPU;
            //else if (cropFrameX)
            //{
            //    float aspect = (pixelRectPreCull == Rect.zero) ? (float)screenWidth / screenHeight : pixelRectPreCull.width / pixelRectPreCull.height;
            //    orthoSize = ((resolutionX / aspect) * 0.5f) / assetsPPU;
            //}
            //else if (upscaleRT && zoom > 1)
            //    orthoSize = (offscreenRTHeight * 0.5f) / assetsPPU;
            //else
            //{
            //    float pixelHeight = (pixelRectPreCull == Rect.zero) ? screenHeight : pixelRectPreCull.height;
            //    orthoSize = (pixelHeight * 0.5f) / (zoom * assetsPPU);
            //}

            // Camera pixel grid spacing
            if (upscaleRT || (!upscaleRT && pixelSnapping))
                unitsPerPixel = 1.0f / assetsPPU;
            else
                unitsPerPixel = 1.0f / (zoom * assetsPPU);
        }

        //--------------------------------------------------------------------
        private void CalculatePostRenderPixelRect(float cameraAspect, int screenWidth, int screenHeight)
        {
            // This VP is used when the internal temp RT is blitted back to screen.
            //Rect pixelRect = new Rect();

            if (useStretchFill)
            {
                // stretch (fit either width or height)
                float screenAspect = (float)screenWidth / screenHeight;
                if (screenAspect > cameraAspect)
                {
                    pixelRectPostRender.height = screenHeight;
                    pixelRectPostRender.width = screenHeight * cameraAspect;
                    pixelRectPostRender.x = (screenWidth - (int)pixelRectPostRender.width) / 2;
                    pixelRectPostRender.y = 0;
                }
                else
                {
                    pixelRectPostRender.width = screenWidth;
                    pixelRectPostRender.height = screenWidth / cameraAspect;
                    pixelRectPostRender.y = (screenHeight - (int)pixelRectPostRender.height) / 2;
                    pixelRectPostRender.x = 0;
                }
            }
            else
            {
                // center
                pixelRectPostRender.height = zoom * offscreenRTHeight;
                pixelRectPostRender.width = zoom * offscreenRTWidth;
                pixelRectPostRender.x = (screenWidth - (int)pixelRectPostRender.width) / 2;
                pixelRectPostRender.y = (screenHeight - (int)pixelRectPostRender.height) / 2;
            }
        }

        //--------------------------------------------------------------------
    }

    //--------------------------------------------------------------------
}

//--------------------------------------------------------------------
