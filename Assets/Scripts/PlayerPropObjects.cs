using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPropObjects : MonoBehaviour
{
    #region Singleton
    public static PlayerPropObjects instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    [SerializeField] private List<GameObject> _propObjects;

    public void ActivatePropObject(int m_ID)
    {
        if(_propObjects[m_ID] != null) _propObjects[m_ID].SetActive(true);
    }

    public void DeactivateAllPropObjects()
    {
        foreach (var m_propObject in _propObjects)
        {
            if(m_propObject != null) m_propObject.SetActive(false);
        }
    }
}
