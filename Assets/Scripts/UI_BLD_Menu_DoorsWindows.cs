using EasyBuildSystem.Features.Scripts.Core.Base.Builder;
using EasyBuildSystem.Features.Scripts.Core.Base.Manager;
using EasyBuildSystem.Features.Scripts.Core.Base.Piece;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UI_BLD_Menu_DoorsWindows : MonoBehaviour
{
    public Button ButtonTemplate;
    public Transform Container;
    public List<PieceBehaviour> DoorsWindows_Pieces;

    private void Start()
    {
        

        for (int i = 0; i < DoorsWindows_Pieces.Count; i++)
        {
            if (DoorsWindows_Pieces[i] == null) continue;

            GameObject Button = (GameObject)Instantiate(ButtonTemplate.gameObject, Container);
            Button.SetActive(true);

           

            int Index = i;
            Button.GetComponent<Button>().onClick.AddListener(() =>
            {
                BuildManager.Instance.Pieces = DoorsWindows_Pieces;
                BuilderBehaviour.Instance.ChangeMode(EasyBuildSystem.Features.Scripts.Core.Base.Builder.Enums.BuildMode.None);
                BuilderBehaviour.Instance.SelectPrefab(BuildManager.Instance.Pieces[Index]);
                BuilderBehaviour.Instance.ChangeMode(EasyBuildSystem.Features.Scripts.Core.Base.Builder.Enums.BuildMode.Placement);
            });

            Button.transform.GetChild(0).GetComponent<Image>().sprite = DoorsWindows_Pieces[i].Icon;
            Button.transform.GetChild(0).GetComponent<Image>().preserveAspect = true;

            Button.transform.GetChild(1).GetComponent<Text>().text = DoorsWindows_Pieces[i].Name;
        }
    }
}
