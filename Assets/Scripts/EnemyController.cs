using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    public float LookRadius = 10f;
    public bool IsPerformingAction = false;
    const float k_jumpOrientSpeed = 1000f;

    [SerializeField] private float _wanderRadius = 3f;

    private float _jumpDuration = 1f;
    private float _agentSpeed;

    private Transform _target;
    private NavMeshAgent _agent;
    private CharacterCombat _characterCombat;
    private Animator _animator;

    private bool _isPatrolling = false;
    private float _patrolTimeRemaining = 5f;

    private bool _moveAcrossNavMeshesStarted = false;

    private bool m_isAirborne = false;
    private bool m_isBiDirectional = false;

    // Start is called before the first frame update
    private void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        _target = PlayerManager.instance.Player.transform;
        _characterCombat = GetComponent<CharacterCombat>();
        _animator = GetComponentInChildren<Animator>();
        _agentSpeed = _agent.speed;
    }

    private void LateUpdate()
    {
        float m_distance = Vector3.Distance(_target.position, transform.position);
        float m_agentSpeed;
        _patrolTimeRemaining -= Time.deltaTime;

        if (!IsPerformingAction)
        {
            _agent.speed = _agentSpeed;
            m_agentSpeed = _agent.velocity.magnitude / _agent.speed;

            if (_agent.isOnOffMeshLink && !_moveAcrossNavMeshesStarted)
            {
                StartCoroutine(MoveAcrossNavMeshLink());
                _moveAcrossNavMeshesStarted = true;
            }

            if (_agent.isOnOffMeshLink && m_isAirborne == false)
            {
                m_isBiDirectional = _agent.currentOffMeshLinkData.offMeshLink.biDirectional;
                m_isAirborne = true;

                _animator.SetBool("m_isBiDirectional", m_isBiDirectional);
                _animator.SetBool("m_isAirborne", m_isAirborne);
            }
            else if (_agent.isOnNavMesh && m_isAirborne == true)
            {
                m_isAirborne = false;
                m_isBiDirectional = false;
                _animator.SetBool("m_isAirborne", m_isAirborne);
                _animator.SetBool("m_isBiDirectional", m_isBiDirectional);
            }

            else if (m_distance <= LookRadius)
            {
                _agent.SetDestination(_target.position);

                if (m_distance <= _agent.stoppingDistance)
                {
                    StatsCharacter m_targetStats = _target.GetComponent<StatsCharacter>();
                    if (m_targetStats != null)
                    {
                        _characterCombat.Attack(m_targetStats);
                    }
                    FaceTarget();
                }
            }
            else
            {
                if (_patrolTimeRemaining <= 0f)
                {
                    StartCoroutine(Patrolling());
                }

            }
        }

        else
        {
            _agent.speed = 0;
            m_agentSpeed = 0f;
        }
        _animator.SetFloat("m_agentSpeed", m_agentSpeed, .1f, Time.deltaTime);
    }

    IEnumerator MoveAcrossNavMeshLink()
    {
        OffMeshLinkData data = _agent.currentOffMeshLinkData;
        _agent.updateRotation = false;

        Vector3 startPos = _agent.transform.position;
        Vector3 endPos = data.endPos + Vector3.up * _agent.baseOffset;

        Vector3 m_lookAt;
        if (Vector3.Distance(transform.position, data.startPos) > Vector3.Distance(transform.position, data.endPos)) m_lookAt = data.startPos;
        else m_lookAt = data.endPos;

        FaceTarget();
        float t = 0.0f;
        float tStep = 1.0f / _jumpDuration;

        //_isAirborne = true;
        Vector3 m_destinationHolder = _agent.destination;
        while (t < 1.0f)
        {
            this.transform.position = Vector3.Lerp(startPos, endPos, t);
            _agent.destination = this.transform.position;
            t += tStep * Time.deltaTime;
            yield return null;
        }

        this.transform.position = endPos;

        _agent.updateRotation = true;

        _agent.CompleteOffMeshLink();
        _moveAcrossNavMeshesStarted = false;
        _agent.destination = m_destinationHolder;

    }

    IEnumerator Patrolling()
    {
        _isPatrolling = true;
        _agent.SetDestination(new Vector3(_agent.transform.position.x + Random.Range(-_wanderRadius, _wanderRadius), _agent.transform.position.y, _agent.transform.position.z + Random.Range(-_wanderRadius, _wanderRadius)));
        _patrolTimeRemaining = 5f;
        yield return new WaitForSeconds(5f);
    }

    private void FaceTarget()
    {
        Vector3 m_direction = (_target.position - this.transform.position).normalized;
        Quaternion m_lookRotation = Quaternion.LookRotation(new Vector3(m_direction.x, 0, m_direction.z));
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, m_lookRotation, Time.deltaTime * 2f);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, LookRadius);
    }
}
