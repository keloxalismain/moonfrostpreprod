using UnityEngine;
using UnityEngine.UI;

public class UI_PlayerInventorySlot : MonoBehaviour
{
    private ItemDefinition _item;
    public Image ItemIcon;
    public Button DiscardButton;
    public Button infoButton;

    public void AddItemToSlot(ItemDefinition m_newItem)
    {
        _item = m_newItem;
        ItemIcon.sprite = _item.itemIcon;
        ItemIcon.enabled = true;
        DiscardButton.interactable = true;
        infoButton.interactable = true;
    }

    public void ClearItemSlot()
    {
        _item = null;
        ItemIcon.sprite = null;
        ItemIcon.enabled = false;
        DiscardButton.interactable = false;
        infoButton.interactable = false;
    }

    public void OnDiscardInventoryButton()
    {
        PlayerInventoryManager.instance.RemoveFromPlayerInventory(_item);
    }

    public void OnUseInventoryItem()
    {
        if (_item!= null)
        {
            _item.Use();
        }
    }

    public void OnInfoInventoryButton()
    {
        UI_SceneManager.instance.Canvas.GetComponent<UI_InfoDialogBox>().PopulateInfoDialog(_item);
    }
}
