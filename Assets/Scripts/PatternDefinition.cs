using UnityEngine;

[CreateAssetMenu(fileName = "CHA_Pattern_00_SO", menuName = "Preproduction/Customisation Configs/Pattern Config")]
public class PatternDefinition : ScriptableObject
{
    public string PatternName = "New Name";
    public Texture2D PatternMask = null;
}
