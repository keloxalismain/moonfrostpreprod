using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomNpcSpawner : MonoBehaviour
{
    [SerializeField] private GameObject _randomNpcBase;
    [SerializeField] private int _crowdSize = 10;
    void Start()
    {
        for (int i = 0; i < _crowdSize; i++)
        {
            Instantiate(_randomNpcBase, this.transform);
        }
    }
}
