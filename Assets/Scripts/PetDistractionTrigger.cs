using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetDistractionTrigger : MonoBehaviour
{
    private PetController _petController;
    private void Start()
    {
        _petController = GetComponentInParent<PetController>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            _petController.IsDistracted = true;
            _petController._agent.SetDestination(other.transform.position);
            Debug.Log("Bark at enemy");
        }

        if (other.CompareTag("DistractStatic"))
        {
            _petController.IsDistracted = true;
            _petController._agent.SetDestination(other.transform.position);
            Debug.Log("Bark at distraction static");
        }

        if (other.CompareTag("DistractMobile"))
        {
            _petController.IsDistracted = true;
            _petController._agent.SetDestination(other.transform.position);
            Debug.Log("Bark at distraction mobile");
        }

    }
}
