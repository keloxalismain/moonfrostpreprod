using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_PlayerHud : MonoBehaviour
{
    [SerializeField] private GameObject _healthUI;
    [SerializeField] private Image _healthBar;

    private void Start()
    {
        PlayerManager.instance.Player.GetComponent<StatsCharacter>().OnHealthChanged += OnHealthChanged;
    }

    private void OnHealthChanged(int m_maxHealth, int m_currentHealth)
    {
        float m_healthPercent = (float)m_currentHealth / m_maxHealth;
        _healthBar.fillAmount = m_healthPercent;
    }
}