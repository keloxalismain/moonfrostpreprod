Shader "Unlit/GEN_Stencil_SHD"
{
	Properties
	{
		[IntRange] _StencilID("Stencil ID", Range(0,255)) = 0
	}
		SubShader{
		  Tags { "RenderType" = "Opaque" "Queue" = "Geometry" "RenderPipeline" = "UniversalPipeline"}
		  Pass {
				ZWrite Off
				Blend Zero One
			  Stencil {
				  Ref[_StencilID]
				  Comp Always
				  Pass Replace
				Fail Keep
			  }

			  CGPROGRAM
			  #pragma vertex vert
			  #pragma fragment frag
			  struct appdata {
				  float4 vertex : POSITION;
			  };
			  struct v2f {
				  float4 pos : SV_POSITION;
			  };
			  v2f vert(appdata v) {
				  v2f o;
				  o.pos = UnityObjectToClipPos(v.vertex);
				  return o;
			  }
			  half4 frag(v2f i) : SV_Target {
				  return half4(1,0,0,0);
			  }
			  ENDCG
		  }
	}
}