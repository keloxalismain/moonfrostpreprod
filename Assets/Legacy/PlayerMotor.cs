using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class PlayerMotor : MonoBehaviour
{
    private NavMeshAgent _navMeshAgent;
    private Transform _target;

    const float k_jumpOrientSpeed = 1000f;
    const float k_genericSmoothingTime = 5f;
    private bool _moveAcrossNavMeshesStarted = false;
    private bool _isAirborne = false;

    public float duration = 1f; //Should be timed with the link crossing animation, may be different and need to be moved to a node on the link?

    // Start is called before the first frame update
    private void Start()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
    }

    public void MoveToPoint(Vector3 point)
    {
        _navMeshAgent.SetDestination(point);
    }

    public void FollowTarget(Interactable m_newTarget)
    {
        _navMeshAgent.stoppingDistance = m_newTarget.InteractableRadius * 0.5f;
        _navMeshAgent.updateRotation = false;
        _target = m_newTarget.InteractionPoint;        
    }

    public void StopFollowingTarget()
    {
        _navMeshAgent.stoppingDistance = 0;
        _navMeshAgent.updateRotation = true;
        _target = null;
    }

    private void LateUpdate()
    {
        if (_target != null && _isAirborne == false)
        {
            _navMeshAgent.SetDestination(_target.position);
            FaceTarget(_target.position, k_genericSmoothingTime);
        }

        if (_navMeshAgent.isOnOffMeshLink && !_moveAcrossNavMeshesStarted)
        {
            StartCoroutine(MoveAcrossNavMeshLink());
            _moveAcrossNavMeshesStarted = true;
            _isAirborne = true;
        }
    }

    private void FaceTarget(Vector3 m_target, float m_time)
    {
        if (!_isAirborne)
        {
            Vector3 m_direction = (m_target - transform.position).normalized;
            Quaternion m_lookRotation = Quaternion.LookRotation(new Vector3(m_direction.x, 0f, m_direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, m_lookRotation, Time.deltaTime * m_time);
        }
    }

    IEnumerator MoveAcrossNavMeshLink()
    {
        OffMeshLinkData data = _navMeshAgent.currentOffMeshLinkData;
        _navMeshAgent.updateRotation = false;

        Vector3 startPos = _navMeshAgent.transform.position;
        Vector3 endPos = data.endPos + Vector3.up * _navMeshAgent.baseOffset;

        Vector3 m_lookAt;
        if (Vector3.Distance(transform.position, data.startPos) > Vector3.Distance(transform.position, data.endPos)) m_lookAt = data.startPos;
        else m_lookAt = data.endPos;
        
        FaceTarget(m_lookAt, k_jumpOrientSpeed);
        float t = 0.0f;
        float tStep = 1.0f / duration;

        //_isAirborne = true;
        Vector3 m_destinationHolder = _navMeshAgent.destination;
        while (t < 1.0f)
        {
            this.transform.position = Vector3.Lerp(startPos, endPos, t);
            _navMeshAgent.destination = this.transform.position;
            t += tStep * Time.deltaTime;
            yield return null;
        }

        this.transform.position = endPos;
        
        _navMeshAgent.updateRotation = true;
        _isAirborne = false;
        _navMeshAgent.CompleteOffMeshLink();
        _moveAcrossNavMeshesStarted = false;
        _navMeshAgent.destination = m_destinationHolder;        
    }
}
