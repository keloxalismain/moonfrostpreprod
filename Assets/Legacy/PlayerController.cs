using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour
{
    private Camera _mainCamera;
    private PlayerMotor _playerMotor;
    private Vector3 _destination = Vector3.zero;
    private float _snapValue = 0.5f;
    public Interactable focus;

    // Start is called before the first frame update
    void Start()
    {
        _mainCamera = Camera.main;
        _playerMotor = GetComponent<PlayerMotor>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray m_ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit m_hit;

            // is navigatable?
            if (Physics.Raycast(m_ray, out m_hit, 100))
            {
                if (EventSystem.current.IsPointerOverGameObject())
                    return;
                if(m_hit.transform.gameObject.layer == 6)
                    //(Physics.Raycast(m_ray, out m_hit, 100, _walkableMask))
                {
                    
                    //move to location
                    var tempTransform = m_hit.point;
                    tempTransform.x = Round(tempTransform.x);
                    tempTransform.y = Round(tempTransform.y);
                    tempTransform.z = Round(tempTransform.z);
                    _destination = tempTransform;
                    
                   // _destination = m_hit.point;
                    _playerMotor.MoveToPoint(_destination);

                    //end item focus
                    RemoveFocus();
                }

                else if (m_hit.transform.TryGetComponent(out Interactable m_interactable))
                {
                    //is interactable?
                    SetFocus(m_interactable);
                }
            }
        }
    }

    private void SetFocus(Interactable m_newFocus)
    {
        if(m_newFocus!= focus)
        {
            if(focus != null) focus.OnDefocused();
            focus = m_newFocus;
            _playerMotor.FollowTarget(m_newFocus);
        }
        m_newFocus.OnFocused(this.transform);       
    }

    private void RemoveFocus()
    {
        if (focus != null) focus.OnDefocused();
        focus = null;
        _playerMotor.StopFollowingTarget();
    }


    private float Round(float m_input)
    {
        m_input *= 1 / _snapValue;
        return (float)System.Math.Round(m_input, System.MidpointRounding.AwayFromZero) / (1 / _snapValue);
    }
}
