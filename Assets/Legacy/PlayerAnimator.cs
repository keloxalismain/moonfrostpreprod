using UnityEngine;
using UnityEngine.AI;

public class PlayerAnimator : MonoBehaviour
{
    private NavMeshAgent _navMeshAgent;
    private Animator _playerAnimator;
    const float k_animationDampTime = 0.1f;
    private bool m_isAirborne = false;
    private bool m_isBiDirectional = false;

    private void Start()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _playerAnimator = GetComponentInChildren<Animator>();
        m_isAirborne = false;
        m_isBiDirectional = false;
    }

    private void FixedUpdate()
    {
        float m_agentSpeed = _navMeshAgent.velocity.magnitude / _navMeshAgent.speed;
        Debug.Log(_navMeshAgent.velocity);
        _playerAnimator.SetFloat("m_agentSpeed", m_agentSpeed, k_animationDampTime, Time.deltaTime);

        if (_navMeshAgent.isOnOffMeshLink && m_isAirborne == false)
        {
            m_isBiDirectional = _navMeshAgent.currentOffMeshLinkData.offMeshLink.biDirectional;
            m_isAirborne = true;

            _playerAnimator.SetBool("m_isBiDirectional", m_isBiDirectional);
            _playerAnimator.SetBool("m_isAirborne", m_isAirborne);
        }
        else if (_navMeshAgent.isOnNavMesh && m_isAirborne == true)
        {
            m_isAirborne = false;
            m_isBiDirectional = false;
            _playerAnimator.SetBool("m_isAirborne", m_isAirborne);
            _playerAnimator.SetBool("m_isBiDirectional", m_isBiDirectional);
        }
    }
}
