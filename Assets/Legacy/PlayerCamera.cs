using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))] 
public class PlayerCamera : MonoBehaviour
{
    [SerializeField] private Transform _playerTransform;
    [SerializeField] private Vector3 _cameraOffset;
    [SerializeField] private float _playerHeight = 2f;
    [SerializeField] private float _zoomSmoothing = 4f;
    [SerializeField] private float _currentZoom = 5f;
    [SerializeField] private float _minZoom = 2f;
    [SerializeField] private float _maxZoom = 5f;
    private Camera _camera;

    private void Start()
    {
        _camera = GetComponent<Camera>();
    }

    private void Update()
    {
        _currentZoom -= Input.GetAxis("Mouse ScrollWheel") * _zoomSmoothing;
        _currentZoom = Mathf.Clamp(_currentZoom, _minZoom, _maxZoom);
    }

    private void LateUpdate()
    {
        transform.position = _playerTransform.position + _cameraOffset;
        _camera.orthographicSize = _currentZoom;
    }
}
